#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

int escapeMaze(vector <int> offsets){
  int i = 0;
  int steps = 0;

  while (i < offsets.size()){
    int jump =  offsets[i];
    steps++;
    if (jump >= 3) { offsets[i]--; } else { offsets[i]++; }
    i += jump;
  }
  return steps;
}

int main() {

  vector<int> offsets;

  ifstream infile;
  infile.open("../data/firstInput.txt");

  while(!infile.eof()) {
    string newline;
    getline(infile,newline);
    if (newline != ""){
      int offset = stoi(newline);
      offsets.push_back(offset);
    }
  }

  int totalSteps = escapeMaze(offsets);
  cout << "Total steps: "<<totalSteps<<endl;

  return 0;
}
