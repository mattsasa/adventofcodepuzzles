const fs = require('fs')

function escapeMaze(offsets){
  let i = 0
  let steps = 0

  while (i < offsets.length){
    let jump = parseInt(offsets[i]);
    steps++;
    jump >= 3 ? offsets[i]-- : offsets[i]++
    i += jump
  }
  return steps
}

let array = fs.readFileSync('../data/firstInput.txt').toString().split("\n")
array.splice(array.length - 1, 1)

const totalSteps = escapeMaze(array)
console.log(`Total Steps: ${totalSteps}`)
