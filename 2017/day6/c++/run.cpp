#include <iostream>
#include <vector>
#include <array>


using namespace std;

array<int, 16> banksCurrentState;
vector<array<int, 16>> states;

array<int, 16> firstInput = {4,	1, 15,	12,	0,	9,	9,	5,	5,	8,	7,	3,	14,	5,	12,	3};
array<int, 4> testInput = {0, 2, 7, 0};



int findLargestBank(){
  int largestBank = 0;
  int largestBankIndex = 0;
  for(int i = 0; i < 16; i++) {
    if (banksCurrentState[i] > largestBank) { largestBank = banksCurrentState[i]; largestBankIndex = i; }
  }
  return largestBankIndex;
}

bool newState() {

  for (int i = 0; i < states.size(); i++){
    if (banksCurrentState == states[i]) {
      cout<<"Duplicate state!  "<<i<<endl;
      return true;
    }
  }
  return false;
}

void redistribute(){
  int start = findLargestBank();
  int size = banksCurrentState[start];
  banksCurrentState[start] = 0;
  int index = start + 1;
  for (int i = 0; i < size; i++) {
    if (index > 15) index = 0;
    banksCurrentState[index]++;
    index++;
  }
}

int main(){

  banksCurrentState = firstInput;

  int redistributions = 0;
  while (!newState()) {
    states.push_back(banksCurrentState);
    redistribute();
    redistributions++;
  }

  cout<<"Total Redistributions: "<<redistributions<<endl;
  return 0;
}
