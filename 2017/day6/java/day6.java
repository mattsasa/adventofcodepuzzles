import java.util.ArrayList;
import java.util.Arrays;


public class day6
{
	static int[] banksCurrentState = new int[16];
	static ArrayList<int[]> states = new ArrayList<int[]>();
	static int[] firstInput = {4,	1, 15,	12,	0,	9,	9,	5,	5,	8,	7,	3,	14,	5,	12,	3};

	static int findLargestBank(){
	  int largestBank = 0;
	  int largestBankIndex = 0;
	  for(int i = 0; i < 16; i++) {
	    if (banksCurrentState[i] > largestBank) { largestBank = banksCurrentState[i]; largestBankIndex = i; }
	  }
	  return largestBankIndex;
	}

	static boolean newState() {
	  for (int i = 0; i < states.size(); i++){
	    if (Arrays.equals(banksCurrentState, states.get(i))) {
	      System.out.println("Duplicate state!  " + i);
	      return true;
	    }
	  }
	  return false;
	}

	static void redistribute(){
	  int start = findLargestBank();
	  int size = banksCurrentState[start];
	  banksCurrentState[start] = 0;
	  int index = start + 1;
	  for (int i = 0; i < size; i++) {
	    if (index > 15) index = 0;
	    banksCurrentState[index]++;
	    index++;
	  }
	}

	public static void main(String[] args) {

		banksCurrentState = firstInput;
		int redistributions = 0;
		while (!newState()) {
			states.add(banksCurrentState.clone());
			redistribute();
			redistributions++;
		}

		System.out.println("Total Redistributions: " + redistributions);
	}
}
