import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
//import javax.swing.tree.TreeNode;


class Disc
{
  String name;
  int weight;
  int totalWeight;
  ArrayList<String> children;
}


public class day7
{
  static ArrayList<Disc> allDiscs = new ArrayList<Disc>();
  static ArrayList<String> potentialBottomDiscs = new ArrayList<String>();
  static void parseInputFile() throws Exception {
    File file = new File("../data/firstInput.txt");
    BufferedReader br = new BufferedReader(new FileReader(file));

    String line;
    while ((line = br.readLine()) != null) {
      Disc newDisc = new Disc();
      int firstParenIndex = line.indexOf('(');
      int secondParenIndex = line.indexOf(')');
      newDisc.name = line.substring(0, firstParenIndex - 1);
      potentialBottomDiscs.add(newDisc.name);
      newDisc.weight = Integer.parseInt(line.substring(firstParenIndex + 1,secondParenIndex));
      newDisc.totalWeight = newDisc.weight;
      newDisc.children = new ArrayList<String>();
      String remainingCharacters = line.substring(secondParenIndex + 1);
      if (remainingCharacters.length() != 0){
        String childrenString = line.substring(secondParenIndex + 5);
        String newChild = "";
        for (int i = 0; i < childrenString.length(); i++){
          if (childrenString.charAt(i) == ' '){
            newDisc.children.add(newChild);
            newChild = "";
          } else if (childrenString.charAt(i) != ',') {
            newChild += childrenString.charAt(i);
          }
        }
        newDisc.children.add(newChild);
      }
      allDiscs.add(newDisc);
    }
  }
  static Disc findDiscByName(String name) {
    Disc disc = new Disc();
    for (int j = 0; j < allDiscs.size(); j++){
      if (allDiscs.get(j).name.equals(name)) { disc = allDiscs.get(j); }
    }
    return disc;
  }
  static Disc findBottomDisc() {
    for (int i = 0; i < allDiscs.size(); i++){
      for (int j = 0; j < allDiscs.get(i).children.size(); j++){
        potentialBottomDiscs.remove(allDiscs.get(i).children.get(j));
      }
    }
    for (int i = 0; i < allDiscs.size(); i++){
      if (allDiscs.get(i).name == potentialBottomDiscs.get(0)) {
        return allDiscs.get(i);
      }
    }
    return null;
  }
  static DefaultMutableTreeNode createTree(Disc bottomDisc) {
    DefaultMutableTreeNode root = new DefaultMutableTreeNode(bottomDisc);
    for (int i = 0; i < bottomDisc.children.size(); i++){
      Disc childDisc = findDiscByName(bottomDisc.children.get(i));
      DefaultMutableTreeNode newChild = createTree(childDisc);
      root.add(newChild);
      //childDisc = (Disc) newChild.getUserObject();
      bottomDisc.totalWeight += childDisc.totalWeight;
    }
    //root.setUserObject(bottomDisc);
    return root;
  }
  static Boolean checkNodeBalance(DefaultMutableTreeNode node) {
    int firstWeight = 0;
    for(int i = 0; i < node.getChildCount(); i++){
      DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(i);
      Disc theDisc = (Disc) child.getUserObject();
      if (i == 0) { firstWeight = theDisc.totalWeight; }
      else if (theDisc.totalWeight != firstWeight) { return false; }
    }
    return true;
  }
  static DefaultMutableTreeNode parseTreeForIncorrectNode(DefaultMutableTreeNode node) {
    if (!checkNodeBalance(node)) {
      for(int i = 0; i < node.getChildCount(); i++){
        DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(i);
        if (!checkNodeBalance(child)) {
          return parseTreeForIncorrectNode(child);
        }
      }
      ///completed for loop, each child was balanced
      return node;
    }
    return null;
  }

  public static void main(String[] args) throws Exception {

    parseInputFile();
    Disc bottomDisc = findBottomDisc();
    DefaultMutableTreeNode treeRoot = createTree(bottomDisc);
    DefaultMutableTreeNode culpritNode = parseTreeForIncorrectNode(treeRoot);
    //Disc theDisc = (Disc) culpritNode.getUserObject();
    //System.out.println(theDisc.name);
    //DefaultMutableTreeNode culpritParentNode = (DefaultMutableTreeNode) culpritNode.getParent();
    for(int i = 0; i < culpritNode.getChildCount(); i++){
      DefaultMutableTreeNode child = (DefaultMutableTreeNode) culpritNode.getChildAt(i);
      Disc theDisc = (Disc) child.getUserObject();
      System.out.println(theDisc.weight);
    }
  }
}
