#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<int> changes;
vector<int> states;


bool checkExists(int sum) {
  for (int j = 0; j < states.size(); j++) {
    if (states[j] == sum) {
      cout << "this state already exists!! : " << sum << endl;
      return true;
    }
  }
  return false;
}

int main() {

  ifstream infile;
  infile.open("../data/firstInput.txt");

  while(!infile.eof()) {
    string newline;
    getline(infile,newline);
    if (newline != ""){
      int change = stoi(newline);
      changes.push_back(change);
    }
  }

  int sum = 0;
  states.push_back(sum);
  while (true) {
    for (int i = 0; i < changes.size(); i++){
      sum += changes[i];
      if(checkExists(sum)) {
        return 0;
      }
      states.push_back(sum);
    }
  }
  return 0;
}
