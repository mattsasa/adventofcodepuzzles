const fs = require('fs')
let listOfNumbers = fs.readFileSync('../data/firstInput.txt').toString().split("\n")

//listOfNumbers.forEach(number => {console.log(number);})

history = []

// function numberExistsInHistory(number) {
//   //console.log(history);
//   //console.log(number);
//   for (j = 0; j < history.length; j++){
//     if (number == history[j]) {
//       return true
//     }
//   }
//   return false
// }


sum = 0
weFoundADuplicate = false

history.push(sum)
while (!weFoundADuplicate) {
  for (i = 0; i < listOfNumbers.length; i++) {
    //console.log("history: ");
    //console.log(history);
    //console.log("i : " + i);
    //console.log("old sum: :" + sum  + "   newAddtion:  "+ listOfNumbers[i]);
    sum += parseInt(listOfNumbers[i])
    //console.log("new sum: "+sum);
    if(history.includes(sum)) {
      //console.log(history);
      console.log("we found the first duplicate: " + sum);
      weFoundADuplicate = true
      break;
    }
    history.push(sum)
  }
}



console.log(sum);
