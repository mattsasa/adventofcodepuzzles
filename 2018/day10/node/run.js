const fs = require('fs')
let strings = fs.readFileSync('../data/input.txt').toString().split("\n")
let data = []

strings.forEach(str => {
  let newObj = {}
  let lessIndex = str.indexOf('<'), commaIndex = str.indexOf(','), greatIndex = str.indexOf('>')
  newObj.pos = { x: parseInt(str.substring(lessIndex + 1, commaIndex)), y: parseInt(str.substring(commaIndex + 1, greatIndex)) }
  str = str.substring(greatIndex + 1, str.length)
  lessIndex = str.indexOf('<'), commaIndex = str.indexOf(','), greatIndex = str.indexOf('>')
  newObj.vel = { x: parseInt(str.substring(lessIndex + 1, commaIndex)), y: parseInt(str.substring(commaIndex + 1, greatIndex)) }
  data.push(newObj)
})

function printLights() {
  for (let h = 100; h < 150; h++) {
    for (let w = 130; w < 330; w++) {
      let occupied = false
      for (let i = 0; i < data.length; i++) {
        if (parseInt(data[i].pos.x / 1) == w && parseInt(data[i].pos.y / 1) == h) occupied = true
      }
      if (occupied) {
        process.stdout.write("#")
      } else { process.stdout.write(".") }
    }
    console.log();
  }
  console.log();
}

function moveLights(multiplier){
  data.forEach(light => {
    light.pos.x += light.vel.x * multiplier
    light.pos.y += light.vel.y * multiplier
  })
}

for (let t = 0; t < 10813; t++) { moveLights(1) }

const interval = setInterval(() => {
  printLights()
  //moveLights(1)
}, 1000)

