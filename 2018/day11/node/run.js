let grid = []
for (let h = 1; h <= 300; h++) {
  grid[h] = []
  for (let w = 1; w <= 300; w++) {
    let rackId = w + 10
    let powerLevel = rackId * h
    powerLevel += 7803
    powerLevel = powerLevel * rackId
    let powerLevelStr = powerLevel.toString()
    let digit = parseInt(powerLevelStr[powerLevelStr.length - 3])
    if (digit == null) digit = 0
    let final = digit - 5
    grid[h][w] = final
  }
}

let largest = 0, xId = 0, yId = 0, totalSquares = 0, correctSS = 0
for (let h = 1; h <= 300; h++) {
  console.log("Current H: " + h);
  for (let w = 1; w <= 300; w++) {
    //console.log("Current H: " + h + "  W: " + w);
    for (let i = Math.max(h,w); i <= 300; i++) {
      let sSize = 301 - i
      let total = 0
      totalSquares++
      for (let h2 = h; h2 < h + sSize; h2++) {
        for (let w2 = w; w2 < w + sSize; w2++) {
          total += grid[h2][w2]
        }
      }
      if (total > largest) { largest = total; xId = w; yId = h; correctSS = sSize }
    }
  }
}

console.log(xId);
console.log(yId);
console.log(correctSS);
