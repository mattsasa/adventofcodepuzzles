const fs = require('fs')
const notes = fs.readFileSync('../data/input.txt').toString().split("\n")
const init_state = '#..######..#....#####..###.##..#######.####...####.##..#....#.##.....########.#...#.####........#.#.'
let current_state = '..........' + init_state + '....................'


let conditions = []
let resolutions = []
const totalGenerations = 50000000000

notes.forEach(note => {
  conditions.push(note.substring(0, 5))
  note[note.length-1] == '#' ? resolutions.push(true) : resolutions.push(false)
})

String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

function doGeneration() {
  let next_state = current_state
  for (i = 0; i < current_state.length; i++) {
    for (j = 0; j < conditions.length; j++) {
      let target = current_state[i]
      current_state[i-1] ? target = current_state[i-1] + target : target = '.' + target
      current_state[i-2] ? target = current_state[i-2] + target : target = '.' + target
      current_state[i+1] ? target += current_state[i+1] : target += '.'
      current_state[i+2] ? target += current_state[i+2] : target += '.'
      if (target == conditions[j]) {
        resolutions[j] ? next_state = next_state.replaceAt(i, '#') : next_state = next_state.replaceAt(i, '.')
      }
    }
  }
  if (next_state[next_state.length-1] != '.' || next_state[next_state.length-1] != '.') next_state += '.'
  return next_state
}

let answer = (63*50000000000) + 905
console.log("Answer for 50 billion: " + answer);

let numGenerations = 0
let oldTotal = 0
while (numGenerations < totalGenerations) {
  current_state = doGeneration()
  numGenerations++
  if (numGenerations % 100 == 0) {
    let total = 0
    for (i = 0; i < current_state.length; i++) {
      if (current_state[i] == '#') total += i - 10
    }
    let diff = total - oldTotal
    let estimated = (63*numGenerations) + 905
    console.log("generation:  "  + numGenerations + " total: " + total + "  diff: " + diff + " estimate total: " + estimated);
    oldTotal = total
  }
}