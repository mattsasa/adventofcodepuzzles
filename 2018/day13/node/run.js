const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")

let width = 0, height = lines.length, cartMap = [], carts = []
lines.forEach((line, y) => {
  let newRow = line.split('')
  cartMap.push(newRow)
  newRow.forEach((square, x) => {
    if (square == '^' || square == 'v' || square == '<' || square == '>') {
      carts.push({ loc: { x, y }, direction: square, nextTurn: "left" })
    }
  })
  if (line.length > width) width = line.length
})

function rotateLeft(cart) {
  switch (cart.direction) {
    case "<":
      cart.direction = "v"
      break
    case "v":
      cart.direction = ">"
      break
    case ">":
      cart.direction = "^"
      break
    case "^":
      cart.direction = "<"
      break
  }
}

function rotateRight(cart) {
  switch (cart.direction) {
    case "<":
      cart.direction = "^"
      break
    case "v":
      cart.direction = "<"
      break
    case ">":
      cart.direction = "v"
      break
    case "^":
      cart.direction = ">"
      break
  }
}

function makeTurn(cart) {
  switch (cart.nextTurn) {
    case "left":
      rotateLeft(cart)
      cart.nextTurn = "straight"
      break
    case "straight":
      cart.nextTurn = "right"
      break
    case "right":
      rotateRight(cart)
      cart.nextTurn = "left"
      break
  }
}

function backslash(cart) {
  switch (cart.direction) {
    case '<':
      cart.direction = '^'
      break
    case '>':
      cart.direction = 'v'
      break
    case '^':
      cart.direction = '<'
      break
    case 'v':
      cart.direction = '>'
      break
  }
}

function forwardslash(cart) {
  switch (cart.direction) {
    case '<':
      cart.direction = 'v'
      break
    case '>':
      cart.direction = '^'
      break
    case '^':
      cart.direction = '>'
      break
    case 'v':
      cart.direction = '<'
      break
  }
}

function whoGoesFirst(cartA, cartB) {
  let a = (cartA.loc.y * width) + cartA.loc.x
  let b = (cartB.loc.y * width) + cartB.loc.x
  return a > b
}

function moveAllCarts(){
  carts.sort(whoGoesFirst)
  for (cartI = 0; cartI < carts.length; cartI++) {
    switch (carts[cartI].direction) {
      case '<':
        carts[cartI].loc.x--
        break
      case '>':
        carts[cartI].loc.x++
        break
      case '^':
        carts[cartI].loc.y--
        break
      case 'v':
        carts[cartI].loc.y++
        break
    }
    switch (cartMap[carts[cartI].loc.y][carts[cartI].loc.x]) {
      case '+':
        makeTurn(carts[cartI])
        break
      case "\\":
        backslash(carts[cartI])
        break
      case '/':
        forwardslash(carts[cartI])
        break
    }
    checkForCrash()
  }
}

function sameLocation(locA, locB) { return locA.x == locB.x && locA.y == locB.y }

function checkForCrash() {
  for(let i = 0; i < carts.length; i++) {
    for(let j = 0; j < carts.length; j++) {
      if ( sameLocation(carts[i].loc, carts[j].loc) && i != j) {
        //console.log("crash at: " + carts[i].loc.x + "," + carts[i].loc.y);
        if (i > j) { carts.splice(i, 1); carts.splice(j, 1) }
        else { carts.splice(j, 1); carts.splice(i, 1) }
        if (i < cartI || j < cartI) cartI--
        cartI--
      }
    }
  }
}

for (let i = 0; carts.length > 1; i++){
  //console.log();
  moveAllCarts()
  //console.log(carts);
}

console.log("Last Remaining Cart: " + carts[0].loc.x + "," + carts[0].loc.y);
