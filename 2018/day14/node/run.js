let scoreboard = [3, 7, 1, 0], elf1i = 0, elf2i = 1, myoffset = 6, input = 327901, keepSearching = true

function checkLastRecipes() {
  let targetStr = input.toString()
  let lastRecipes = ""
  for (let i = scoreboard.length - targetStr.length; i < scoreboard.length; i++) {
    lastRecipes += scoreboard[i]
  }
  if (lastRecipes == targetStr) { console.log("Answer: " + (scoreboard.length - targetStr.length)); keepSearching = false }
}

function doNextCombination() {
  let sum = scoreboard[elf1i] + scoreboard[elf2i]
  scoreboard.push(parseInt(sum.toString()[0]))
  checkLastRecipes()
  //if (!(newRecipes < myoffset + recipesToMake)) return
  if (sum > 9) { scoreboard.push(parseInt(sum.toString()[1])); newRecipes++; checkLastRecipes() }
  let loop1 = scoreboard[elf1i] + 1
  for (let i = 0; i < loop1; i++) {
    elf1i++
    if (elf1i >= scoreboard.length) elf1i = 0
  }
  let loop2 = scoreboard[elf2i] + 1
  for (let i = 0; i < loop2; i++) {
    elf2i++
    if (elf2i >= scoreboard.length) elf2i = 0
  }
}
for(newRecipes = 0; keepSearching; newRecipes++) doNextCombination()
// let answer = ""
// for (let i = scoreboard.length - 10; i < scoreboard.length; i++) {
//   answer += scoreboard[i]
// }
// console.log("Answer: " + answer);
