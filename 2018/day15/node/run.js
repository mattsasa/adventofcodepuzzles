const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")

let width = 0, height = lines.length, battleMap = [], units = [], combat = true, rounds = 0
lines.forEach((line, y) => {
  width = line.length
  let newRow = line.split('')
  battleMap.push(newRow)
  newRow.forEach((square, x) => {
    if (square == 'G') {
      units.push({ type: square, x, y, hp: 200, ap: 3, turns: 0 })
    }
    if (square == 'E') {
      units.push({ type: square, x, y, hp: 200, ap: 25, turns: 0 })
    }
  })
})

function readingOrder(squareA, squareB) {
  let a = (squareA.y * width) + squareA.x
  let b = (squareB.y * width) + squareB.x
  if (a > b) return 1
  if (a < b) return -1
  return 0
}

function printMap(map) {
  for (let h = 0; h < height; h++) {
    for (let w = 0; w < width; w++) {
      process.stdout.write(map[h][w].toString())
    }
    console.log();
  }
  console.log();
}

function findInRangeSquares(targets, unit) {
  let inRangeSquares = [], alreadyInRange = false
  targets.forEach(target => {
    if (battleMap[target.y][target.x + 1] == '.') inRangeSquares.push({ x: target.x + 1, y: target.y })
    if (battleMap[target.y][target.x - 1] == '.') inRangeSquares.push({ x: target.x - 1, y: target.y })
    if (battleMap[target.y + 1][target.x] == '.') inRangeSquares.push({ x: target.x, y: target.y + 1 })
    if (battleMap[target.y - 1][target.x] == '.') inRangeSquares.push({ x: target.x, y: target.y - 1 })
    if (Math.abs(target.x - unit.x) + Math.abs(target.y - unit.y) == 1) alreadyInRange = true
  })
  return alreadyInRange ? -1 : inRangeSquares
}

function findMinDistance(src, dst) {
  let source = { row: src.y, col: src.x, dist: 0 }
  let visitedMap = new Array(height)
  for(let i = 0; i < visitedMap.length; i++) {
    visitedMap[i] = new Array(width)
    for (let j = 0; j < width; j++) {
      battleMap[i][j] == '.' ? visitedMap[i][j] = '.' : visitedMap[i][j] = 'X'
    }
  }
  visitedMap[dst.y][dst.x] = '.'
  visitedMap[src.y][src.x] = 0


  let q = []
  q.push(source)
  while (q.length != 0) {
    let p = q[0]
    q.splice(0,1)

    // Destination found;
    if (p.row == dst.y && p.col == dst.x) return { dist: p.dist, map: visitedMap }

    // moving up
    if (p.row - 1 >= 0 && visitedMap[p.row - 1][p.col] == '.') {
      q.push({ row: p.row - 1, col: p.col, dist: p.dist + 1 })
      visitedMap[p.row - 1][p.col] = p.dist + 1
    }
    // moving down
    if (p.row + 1 < height && visitedMap[p.row + 1][p.col] == '.') {
      q.push({ row: p.row + 1, col: p.col, dist: p.dist + 1 })
      visitedMap[p.row + 1][p.col] = p.dist + 1
    }
    // moving left
    if (p.col - 1 >= 0 && visitedMap[p.row][p.col - 1] == '.') {
      q.push({ row: p.row, col: p.col - 1, dist: p.dist + 1 })
      visitedMap[p.row][p.col - 1] = p.dist + 1
    }
    // moving right
    if (p.col + 1 < width && visitedMap[p.row][p.col + 1] == '.') {
      q.push({ row: p.row, col: p.col + 1, dist: p.dist + 1 })
      visitedMap[p.row][p.col + 1] = p.dist + 1
    }
  }
  return -1
}

function findNearestSquares(inRangeSquares, startPosition) {
  let nearestSquares = [], closestDist = 100000
  inRangeSquares.forEach(targetLocation => {
    const potentialLocation = Object.assign(targetLocation, findMinDistance(startPosition, targetLocation))
    nearestSquares.push(potentialLocation)
    if (potentialLocation.dist < closestDist && potentialLocation.dist != -1) closestDist = potentialLocation.dist
  })
  return nearestSquares.filter(x => x.dist == closestDist)
}

function pickNewLocation(dst, src) {
  const { map } = findMinDistance(dst, src) //Backwards
  let newDist = map[src.y][src.x] - 1
  if (map[src.y - 1][src.x] == newDist) return { y: src.y - 1, x: src.x } //Up
  if (map[src.y][src.x - 1] == newDist) return { y: src.y, x: src.x - 1 } //Left
  if (map[src.y][src.x + 1] == newDist) return { y: src.y, x: src.x + 1 } //Right
  if (map[src.y + 1][src.x] == newDist) return { y: src.y + 1, x: src.x } //Down

}

function doUnitMovement(unit) {
  const targets = units.filter(target => target.type != unit.type)
  if (targets.length == 0) { combat = false; return }
  const inRangeSquares = findInRangeSquares(targets, unit)
  if (inRangeSquares == -1) return
  if (inRangeSquares.length == 0) return
  const nearestSquares = findNearestSquares(inRangeSquares, unit)
  if (nearestSquares.length == 0) return
  nearestSquares.sort(readingOrder)
  const chosenSquare = nearestSquares[0]
  const newLocation = pickNewLocation(chosenSquare, unit)
  battleMap[unit.y][unit.x] = '.'
  unit = Object.assign(unit, newLocation)
  battleMap[unit.y][unit.x] = unit.type
}

function getUnitAt(location) { for (unit of units) { if (unit.x == location.x && unit.y == location.y) return unit } }

function pickAttackTarget(unit) {
  let inRangeTargets = [], weakestHP = 201
  let enemyType = unit.type == 'E' ? 'G' : 'E'
  if (battleMap[unit.y - 1][unit.x] == enemyType) inRangeTargets.push(getUnitAt({ x: unit.x, y: unit.y - 1 }))
  if (battleMap[unit.y][unit.x - 1] == enemyType) inRangeTargets.push(getUnitAt({ x: unit.x - 1, y: unit.y }))
  if (battleMap[unit.y][unit.x + 1] == enemyType) inRangeTargets.push(getUnitAt({ x: unit.x + 1, y: unit.y }))
  if (battleMap[unit.y + 1][unit.x] == enemyType) inRangeTargets.push(getUnitAt({ x: unit.x, y: unit.y + 1 }))
  for (target of inRangeTargets) { if(target.hp < weakestHP) weakestHP = target.hp }
  for (target of inRangeTargets) { if(target.hp == weakestHP) return target }
}

function removeUnit(corpse, killer) {
  battleMap[corpse.y][corpse.x] = '.'
  units = units.filter(unit => (unit.hp > 0))
  if (readingOrder({x:corpse.x, y:corpse.y},{x:killer.x, y:killer.y}) == -1) gUnitIndex--
}

function doUnitAttack(unit) {
  const attackTarget = pickAttackTarget(unit)
  if (attackTarget) {
    attackTarget.hp -= unit.ap
    if (attackTarget.hp <= 0) removeUnit(attackTarget, unit)
  }

}

function doRound(){
  units.sort(readingOrder)
  for (gUnitIndex = 0; gUnitIndex < units.length; gUnitIndex++) {
    units[gUnitIndex].turns++
    doUnitMovement(units[gUnitIndex])
    doUnitAttack(units[gUnitIndex])
  }
}

printMap(battleMap)
while (combat) {
  doRound()
  if (combat) rounds++
  printMap(battleMap)
}

let sum = 0
units.forEach(unit => sum += unit.hp)
console.log("Rounds: " + rounds);
console.log("Sum HP: " + sum);
console.log("Outcome: " + rounds * sum);