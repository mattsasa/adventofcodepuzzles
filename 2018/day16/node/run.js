const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
let samples = [], newSample = {}

lines.forEach(line => {
  switch (line[0]) {
    case 'B':
      items = line.substring(9,19).split(",")
      items.forEach((item, i) => items[i] = parseInt(item))
      newSample.beforeRegisters = items
      break
    case undefined:
      newSample = {}
      break
    case 'A':
      items = line.substring(9,19).split(",")
      items.forEach((item, i) => items[i] = parseInt(item))
      newSample.afterRegisters = items
      samples.push(newSample)
      break
    default:
      items = line.split(" ")
      items.forEach((item, i) => items[i] = parseInt(item))
      newSample.instruction = items
  }
})


const lines2 = fs.readFileSync('../data/input2.txt').toString().split("\n")
let instructions = []
lines2.forEach(line => {
  let array = line.split(" ")
  array.forEach((item, i) => array[i] = parseInt(item))
  instructions.push(array)
})

function addi(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] + instruction[2]
  return registers
}
function addr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] + registers[instruction[2]]
  return registers
}
function mulr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] * registers[instruction[2]]
  return registers
}
function muli(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] * instruction[2]
  return registers
}
function banr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] & registers[instruction[2]]
  return registers
}
function bani(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] & instruction[2]
  return registers
}
function borr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] | registers[instruction[2]]
  return registers
}
function bori(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] | instruction[2]
  return registers
}
function setr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]]
  return registers
}
function seti(registers, instruction){
  registers[instruction[3]] = instruction[1]
  return registers
}
function gtir(registers, instruction){
  instruction[1] > registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
  return registers
}
function gtri(registers, instruction){
  registers[instruction[1]] > instruction[2] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
  return registers
}
function gtrr(registers, instruction){
  registers[instruction[1]] > registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
  return registers
}
function eqir(registers, instruction){
  instruction[1] == registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
  return registers
}
function eqri(registers, instruction){
  registers[instruction[1]] == instruction[2] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
  return registers
}
function eqrr(registers, instruction){
  registers[instruction[1]] == registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
  return registers
}
let opcodes = [addi, addr, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr]



function checkBehavior(sample, opcodeFunc) {
  let beforeRegistersClone = JSON.parse(JSON.stringify(sample.beforeRegisters))
  let outputData = opcodeFunc(beforeRegistersClone, sample.instruction)
  return sample.afterRegisters.toString() == outputData.toString()
}

let samplesForAnswer = 0
samples.forEach(sample => {
  let matchingBehaviorCount = 0
  opcodes.forEach(opcode => {
    checkBehavior(sample, opcode) ? matchingBehaviorCount++ : null
  })
  if (matchingBehaviorCount >= 3) samplesForAnswer++
})
console.log("Answer: " + samplesForAnswer + " samples behave like three or more opcodes!");

let currentRegisters = [0, 0, 0, 0]

let opcodeIds = {
  0: muli,
  1: seti,
  2: bani,
  3: gtri,
  4: gtrr,
  5: eqrr,
  6: addi,
  7: gtir,
  8: eqir,
  9: mulr,
  10: addr,
  11: borr,
  12: bori,
  13: eqri,
  14: banr,
  15: setr
}

instructions.forEach(instruction => {
  opcodeIds[instruction[0]](currentRegisters, instruction)
})

console.log(currentRegisters);

