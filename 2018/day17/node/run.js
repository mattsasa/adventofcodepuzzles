const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
let walls = [], steps = 0
lines.forEach(line => {
  let newWall = {}
  let commaIndex = line.indexOf(','), firstDot = line.indexOf('.');
  (line[0] == 'x') ? newWall.type = 'vertical' : newWall.type = 'horizontal'
  newWall.long = parseInt(line.substring(2, commaIndex))
  newWall.start = parseInt(line.substring(commaIndex + 4, firstDot))
  newWall.end = parseInt(line.substring(firstDot + 2, line.length))
  walls.push(newWall)
})

function findDimensions() {
  let height = 0, leftmax = 500, rightmax = 500
  walls.forEach(wall => {
    if (wall.type == 'horizontal') {
      if (wall.long > height) height = wall.long
    } else {  // Is vertical
      if (wall.long < leftmax) leftmax = wall.long
      if (wall.long > rightmax) rightmax = wall.long
    }
  })
  leftmax--; rightmax += 2; height++; // padding
  const width = rightmax - leftmax
  return { height, leftmax, rightmax, width }
}

function printGrid(grid) {
  for (h = 0; h < height; h++) {
    //process.stdout.write(h.toString())
    for (w = 0; w < width; w++) {
      process.stdout.write(grid[h][w])
    }
    console.log();
  }
  console.log();
}

function initGrid(width, height) {
  let grid = []
  for (h = 0; h < height; h++) {
    let row = []
    for (w = 0; w < width; w++) {
      row.push('.')
    }
    grid.push(row)
  }
  return grid
}

function addWalls(grid, leftmax) {
  walls.forEach(wall => {
    if (wall.type == 'horizontal') {
      for (i = wall.start - leftmax; i < wall.end - leftmax; i++) grid[wall.long][i] = '#'
    } else {  // Is vertical
      for (i = wall.start; i < wall.end + 1; i++) grid[i][wall.long - leftmax] = '#'
    }
  })
  grid[0][500 - leftmax] = '+'
}

function calcTotalSand() {
  sand = 0
  for (h = 0; h < height; h++) {
    for (w = 0; w < width; w++) {
      if (grid[h][w] == '.') sand++
    }
  }
  return sand
}

function calcAllWaterTiles() {
  x = 0, minHeight = height
  for (h = 0; h < height; h++) {
    for (w = 0; w < width; w++) {
      if (grid[h][w] == '#' && h < minHeight) minHeight = h
    }
  }
  for (h = minHeight; h < height; h++) {
    for (w = 0; w < width; w++) {
      if (grid[h][w] == '|' || grid[h][w] == '~' || grid[h][w] == 'x') x++
    }
  }
  return x
}

function calcTotalStillWater() {
  x = 0
  for (h = 0; h < height; h++) {
    for (w = 0; w < width; w++) {
      if (grid[h][w] == '~' ||  grid[h][w] == 'x') x++
    }
  }
  return x
}

function filloutUnstableWater(leftSide, rightSide) {
//   grid[source.h][source.w] = '|'
//   while (grid[leftSide.h][leftSide.w] == '.'
  for (i = leftSide.w + 1; i < rightSide.w; i++) {
    grid[leftSide.h][i] = '|'
  }
}

function dripWaterVertically(source) {
  let location = { h: source.h + 1, w: source.w }
  while (grid[location.h][location.w] == '.') {
    grid[location.h][location.w] = '|'; steps++
    location.h++; if (location.h > height - 1 || grid[location.h][location.w] == '|') return null
  } location.h--
  return location
}

function spreadHorizontally(source) {
  let leftResult = "ok", rightResult = "ok",  leftstop = "ok", rightstop = "ok", stable = true
  grid[source.h][source.w] = '~'
  let leftSide = { h: source.h, w: source.w - 1 }
  if (grid[leftSide.h + 1][leftSide.w] == '.') { //an exit
    grid[leftSide.h][leftSide.w] = 'x'; steps++;
    leftResult = recursiveWaterfall(leftSide)
    if (grid[leftSide.h + 1][leftSide.w] == '|') {
      leftstop = "untested"; stable = false
    }
    leftSide.w--
  }
  let rightSide = { h: source.h, w: source.w + 1 }
  if (grid[rightSide.h + 1][rightSide.w] == '.') { //an exit
    grid[rightSide.h][rightSide.w] = 'x'; steps++
    rightResult = recursiveWaterfall(rightSide)
    if (grid[rightSide.h + 1][rightSide.w] == '|') {
      rightstop = "untested"; stable = false
    }
    rightSide.w++
  }


  while (grid[leftSide.h][leftSide.w] == '.' && leftResult == "ok" && leftstop == "ok") { // && grid[leftSide.h + 1][leftSide.w] != '.') {
    grid[leftSide.h][leftSide.w] = '~'; steps++
    leftSide.w--; leftResult = "ok"
    if (grid[leftSide.h + 1][leftSide.w] == '.') { //an exit
      grid[leftSide.h][leftSide.w] = 'x'; steps++
      leftResult = recursiveWaterfall(leftSide)
      ///Fillout unstable water
      if (grid[leftSide.h + 1][leftSide.w] == '|') {
        leftstop = "untested"; stable = false
      }
      leftSide.w--
    }
  }
  while (grid[rightSide.h][rightSide.w] == '.' && rightResult == "ok" && rightstop == "ok") { // && grid[rightSide.h + 1][rightSide.w] != '.') {
    grid[rightSide.h][rightSide.w] = '~'; steps++
    rightSide.w++; rightResult = "ok"
    if (grid[rightSide.h + 1][rightSide.w] == '.') { //an exit
      grid[rightSide.h][rightSide.w] = 'x'; steps++
      rightResult = recursiveWaterfall(rightSide)
      if (grid[rightSide.h + 1][rightSide.w] == '|') {
        rightstop = "untested"; stable = false
      }
      rightSide.w++
    }
  }
  if (!stable) filloutUnstableWater(leftSide, rightSide)
  if (rightResult == "ok" && leftResult == "ok" && stable) return "stable"
  if (rightResult == "stop" && leftResult == "stop") return "stop"
  return "other"
}

function recursiveWaterfall(source) {
  const bottomDrip = dripWaterVertically(source)
  if (bottomDrip == null) return "stop"
  let exits = [], result = "stable"
  while (bottomDrip.h > source.h && result == "stable") {
    result = spreadHorizontally(bottomDrip)
    if (result == "stop") return "stop"
    bottomDrip.h--
  }
  return "ok"
}


let { height, leftmax, rightmax, width } = findDimensions()
const grid = initGrid(width, height)
addWalls(grid, leftmax)

recursiveWaterfall({ h: 0, w: 500 - leftmax })

printGrid(grid)
console.log("Final Total Steps: " + steps);
console.log("Still water: " + calcTotalStillWater());
console.log("All Water tiles: " + calcAllWaterTiles());


