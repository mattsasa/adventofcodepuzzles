const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
const height = lines.length, width = lines[0].length
let grid = []
lines.forEach(line => {   grid.push(line.split(""))  })

function printGrid() {
  console.log();
  let trees = 0, lumberyards = 0
  for (h = 0; h < height; h++) {
    //process.stdout.write(h.toString())
    for (w = 0; w < width; w++) {
      process.stdout.write(grid[h][w])
      if (grid[h][w] == '|') trees++
      if (grid[h][w] == '#') lumberyards++
    }
    console.log();
  }
  console.log("Total trees: " + trees + " yards: " + lumberyards + "  product: " + lumberyards*trees);
}

function checkOpenLand(center_h, center_w) {
  let counter = 0
  for (h = center_h - 1; h < center_h + 2; h++) {
    for (w = center_w - 1; w < center_w + 2; w++) {
      if (h == center_h && w == center_w) continue
      if (h < 0 || h > height - 1 || w < 0 || w > width - 1) continue
      if (grid[h][w] == '|') counter++
    }
  }
  return counter >= 3 ? '|' : '.'
}

function checkTrees(center_h, center_w) {
  let counter = 0
  for (h = center_h - 1; h < center_h + 2; h++) {
    for (w = center_w - 1; w < center_w + 2; w++) {
      if (h == center_h && w == center_w) continue
      if (h < 0 || h > height - 1 || w < 0 || w > width - 1) continue
      if (grid[h][w] == '#') counter++
    }
  }
  return counter >= 3 ? '#' : '|'
}

function checkLumberYard(center_h, center_w) {
  let treeNearby = false, lumberYardNearby = false
  for (h = center_h - 1; h < center_h + 2; h++) {
    for (w = center_w - 1; w < center_w + 2; w++) {
      if (h == center_h && w == center_w) continue
      if (h < 0 || h > height - 1 || w < 0 || w > width - 1) continue
      if (grid[h][w] == '|') treeNearby = true
      if (grid[h][w] == '#') lumberYardNearby = true
    }
  }
  return ( treeNearby && lumberYardNearby ) ? '#' : '.'
}

function doIteration() {
  let newGrid = []
  for (gh = 0; gh < height; gh++) {
    let newline = []
    for (gw = 0; gw < width; gw++) {
      let newTile = '.'
      switch (grid[gh][gw]) {
        case '.':
          newTile = checkOpenLand(gh,gw)
          break
        case '|':
          newTile = checkTrees(gh,gw)
          break
        case '#':
          newTile = checkLumberYard(gh,gw)
          break
      }
      newline.push(newTile)
    }
    newGrid.push(newline)
  }
  grid = newGrid
}

let grids = []

/// y = 29x + b

///23 offset

//34482758 .... + 18

// 20 * 29 = 580

/// 1 billion  - (35714189 * 28) = 2708

/// 2708 + (28 * 35714189) = 1bil

let reference = ""

for (i = 0; i < 2708; i++) {
  doIteration()
  printGrid()

  // if (i + 1 == 2918) reference = grid.toString()
  //
  // if ( ((i+1) - 2918)  % 29 == 0 ) {
  //   console.log(  grid.toString() == reference  );
  // }

  // if (grids.includes(grid.toString())) {
  //   console.log("found duplicate at " + i);
  //   let matchIndex = grids.indexOf(grid.toString())
  //   console.log("old match at: " + matchIndex);
  //   //process.exit()
  // }
  // grids.push(grid.toString())
}

printGrid()