// #ip 2
// 0, 1, 2 , 3, 4, 5
// a, b, ip, d, e, f
//
// 0   addi 2 16 2 # ip += 16
// 1   seti 1 1 3 # d = 1
// 2   seti 1 7 5 # f = 1
// 3   mulr 3 5 4 # e = d * f
// 4   eqrr 4 1 4 # e =  (e == b) ? 1 : 0
// 5   addr 4 2 2 # ip = ip + e
// 6   addi 2 1 2 # ip += 1
// 7   addr 3 0 0 # a = a + d  # will be here if b == 1
// 8   addi 5 1 5 # f = f + 1  # will be here if b != 1, b == 0
// 9   gtrr 5 1 4 # e = (f > b) ? 1 : 0
// 10  addr 2 4 2 # ip = ip + e
// 11  seti 2 3 2 # ip = 2 # will be here if  !(f > b)
// 12  addi 3 1 3 # d = d + 1 # will be here if (f > b)
// 13  gtrr 3 1 4 # e = (d > b) ? 1 : 0
// 14  addr 4 2 2 # ip += e
// 15  seti 1 9 2 # ip = 1 # will be here if !(d > b)
// 16  mulr 2 2 2 # ip = 32 # will be here if (d > b)

// 17  addi 1 2 1 # b = b + 2
// 18  mulr 1 1 1 # b = b * b
// 19  mulr 2 1 1 # b = ip(19) * b
// 20  muli 1 11 1 # b = b * 11 #  836
// 21  addi 4 3 4 # e = e + 3
// 22  mulr 4 2 4 # e = e * ip(22)
// 23  addi 4 13 4  # e = e + 13
// 24  addr 1 4 1 # b = b + e
// 25  addr 2 0 2 # ip = ip + a
// 26  seti 0 1 2 # ip = 0 # a starts at 0
// 27  setr 2 0 4 # e = 27 # a starts at 1
// 28  mulr 4 2 4 # e = e * 28
// 29  addr 2 4 4 # e = e + 29
// 30  mulr 2 4 4 # e = e * 30
// 31  muli 4 14 4 # e = e * 14
// 32  mulr 4 2 4 # e = e * 32
// 33  addr 1 4 1 # b += e
// 34  seti 0 4 0 # a = 0
// 35  seti 0 5 2 # ip = 0

let a = 0, b = 0, ip = 0, d = 0, e = 0, f = 0

// function topLoop() {
//   e = d * f
//   if (b == e) a += d
//   f++
// }

function smartTopLoop() { if (b % d == 0) a += d }

a = 0, b = 10551315 //10551315  //915
d = 1
for (d = 1; d <= b; d++) smartTopLoop()


console.log(a + " " + b + " " + ip + " " + d + " " + e + " " + f);