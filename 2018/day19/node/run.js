const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
const instructions = []
let registers = [0, 0, 0, 0, 0, 0]

for (i = 1; i < lines.length; i++) {
  let words = lines[i].split(" ")
  instructions.push( { opcode: words[0], instruction: [ null, parseInt(words[1]), parseInt(words[2]), parseInt(words[3]) ] } )
}


function addi(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] + instruction[2]
}
function addr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] + registers[instruction[2]]
}
function mulr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] * registers[instruction[2]]
}
function muli(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] * instruction[2]
}
function banr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] & registers[instruction[2]]
}
function bani(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] & instruction[2]
}
function borr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] | registers[instruction[2]]
}
function bori(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] | instruction[2]
}
function setr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]]
}
function seti(registers, instruction){
  registers[instruction[3]] = instruction[1]
}
function gtir(registers, instruction){
  instruction[1] > registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function gtri(registers, instruction){
  registers[instruction[1]] > instruction[2] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function gtrr(registers, instruction){
  registers[instruction[1]] > registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function eqir(registers, instruction){
  instruction[1] == registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function eqri(registers, instruction){
  registers[instruction[1]] == instruction[2] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function eqrr(registers, instruction){
  registers[instruction[1]] == registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
let opcodes = {
  "muli": muli,
  "seti": seti,
  "bani": bani,
  "gtri": gtri,
  "gtrr": gtrr,
  "eqrr": eqrr,
  "addi": addi,
  "gtir": gtir,
  "eqir": eqir,
  "mulr": mulr,
  "addr": addr,
  "borr": borr,
  "bori": bori,
  "eqri": eqri,
  "banr": banr,
  "setr": setr
}

let ip = 2

while (true) {
  let pc = registers[ip]
  opcodes[instructions[pc].opcode](registers, instructions[pc].instruction)
  registers[ip]++
  if (registers[ip] > instructions.length - 1) {
    console.log("end"); console.log(registers); break;
  }
}