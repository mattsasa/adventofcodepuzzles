#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<string> boxIds;

bool containsTwo(string str) {

  for (int i = 0; i < str.length(); i++) {
    for (int j = 0; j < str.length(); j++) {
      if (str[j] != ' ' && str[j] == str[i] && i != j) {
        bool exactlyTwo = true;
        for (int k = 0; k < str.length(); k++) {
          if (str[k] == str[j] && k != i && k != j) { exactlyTwo = false; }
        }
        if (exactlyTwo) return true;
      }
    }
  }

  return false;
}

bool containsThree(string str) {

  for (int i = 0; i < str.length(); i++) {
    int count = 0;
    for (int j = 0; j < str.length(); j++) {
      if (str[j] != ' ' && str[j] == str[i]) { count++; }
    }
    if (count == 3) return true;
  }

  return false;
}

void compareBoxIds(string str1, string str2) {
  int differCount = 0;
  for (int i = 0; i < str1.size(); i++) {
    if (str1[i] != str2[i]) { differCount++; }
  }
  if (differCount == 1){
    cout << str1 <<" " <<str2<<endl;
  }
}

int main() {

  ifstream infile;
  infile.open("../data/firstInput.txt");


  int totalTwo = 0;
  int totalThree = 0;

  while(!infile.eof()) {
    string newline;
    getline(infile,newline);
    if (newline != ""){
      boxIds.push_back(newline);
      if (containsTwo(newline)) totalTwo++;
      if (containsThree(newline)) totalThree++;
    }
  }

  cout << "TotalTwo: "<<totalTwo<<endl;
  cout << "totalThree: "<<totalThree<<endl;
  cout << "checksum: "<<totalTwo*totalThree<<endl;

  for (int i = 0; i < boxIds.size(); i++) {
    for (int j = 0; j < boxIds.size(); j++) {
      if (i == j) break;
      compareBoxIds(boxIds[i], boxIds[j]);
    }
  }

  return 0;
}
