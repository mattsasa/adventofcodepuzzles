const fs = require('fs'); require("collections/shim-array"); require("collections/listen/array-changes")
let input = fs.readFileSync('../data/input.txt').toString().split("\n")[1-1]; input = input.substring(1, input.length - 1)
let grid = [], height = 0, width = 0, index = 0, location = { x: 0, y: 0 }, keepGoing = true,
locationStack = [], locationAStack = [], oldGrid = [], printing = false,
currentEnds = []

Array.prototype.prepend = function(item) { this.splice( 0, 0, item ) }

function copyObj(obj) { return JSON.parse(JSON.stringify(obj)) }

function initGrid() { grid = ['#?#', '?X?', '#?#']; height = grid.length; width = grid[0].length; location.x = 1; location.y = 1 }

function printGrid() {
  for (h = 0; h < height; h++) {
    for (w = 0; w < width; w++) {
      if (location.x == w && location.y == h) process.stdout.write('\x1b[41m' + grid[h][w] + '\x1b[0m')
      else if (oldGrid[h] && oldGrid[h][w] && oldGrid[h][w] == grid[h][w]) process.stdout.write(grid[h][w])
      else process.stdout.write(grid[h][w]) //process.stdout.write('\x1b[43m' + grid[h][w] + '\x1b[0m')
    }
    console.log();
  }
  console.log();
  oldGrid = copyObj(grid)
}

function fillInFinalWalls() {
  for (h = 0; h < height; h++) {
    grid[h] = grid[h].split("")
    for (w = 0; w < width; w++) if (grid[h][w] == '?') grid[h][w] = '#'
  }
}

function updateAllLocations(shift) { locationStack.forEach(loc => { loc.x += shift.x; loc.y += shift.y }) }

function extendWest(loc) {
  grid[loc] = '?.|' + grid[loc].substring(1, width)
  for (h = 0; h < height; h++) {
    if (h == loc + 1 || h  == loc - 1) grid[h] = '#?' + grid[h]
    else if (h != loc) grid[h] = '##' + grid[h]
  }
  updateAllLocations({ x: 2, y: 0})
}
function extendEast(loc) {
  grid[loc] = grid[loc].substring(0, width - 1) + '|.?'
  for (h = 0; h < height; h++) {
    if (h == loc - 1 || h == loc + 1) grid[h] = grid[h] + '?#'
    else if (h != loc) grid[h] = grid[h] + '##'
  }
}
function extendNorth(loc) {
  let line = ''; for (w = 0; w < width; w++) line += '#'
  grid.prepend(line.substring(0, loc - 1)  + '?.?' + line.substring(loc + 2, width))
  grid.prepend(line.substring(0, loc - 1)  + '#?#' + line.substring(loc + 2, width))
  grid[2] = grid[2].substring(0, loc) + '-' + grid[2].substring(loc + 1, width)
  updateAllLocations({ x: 0, y: 2})
}
function extendSouth(loc) {
  let line = ''; for (w = 0; w < width; w++) line += '#'
  grid.push(line.substring(0, loc - 1)  + '?.?' + line.substring(loc + 2, width))
  grid.push(line.substring(0, loc - 1)  + '#?#' + line.substring(loc + 2, width))
  grid[height - 3] = grid[height - 3].substring(0, loc) + '-' + grid[height - 3].substring(loc + 1, width)
}

function addWest(loc) {
  grid[loc.y] = grid[loc.y].substring(0, loc.x - 1) + ((grid[loc.y][loc.x-1]=='|') ? '|' : '?') + '.' + '|' + grid[loc.y].substring(loc.x + 2, width)
  grid[loc.y - 1] = grid[loc.y - 1].substring(0, loc.x - 1) + '#' + ((grid[loc.y - 1][loc.x]=='-') ? '-' : '?') + '#' + grid[loc.y - 1].substring(loc.x + 2, width)
  grid[loc.y + 1] = grid[loc.y + 1].substring(0, loc.x - 1) + '#' + ((grid[loc.y + 1][loc.x]=='-') ? '-' : '?') + '#' + grid[loc.y + 1].substring(loc.x + 2, width)
}
function addEast(loc) {
  grid[loc.y] = grid[loc.y].substring(0, loc.x - 1) + '|' + '.' + ((grid[loc.y][loc.x+1]=='|') ? '|' : '?') + grid[loc.y].substring(loc.x + 2, width)
  grid[loc.y - 1] = grid[loc.y - 1].substring(0, loc.x - 1) + '#' + ((grid[loc.y - 1][loc.x]=='-') ? '-' : '?') + '#' + grid[loc.y - 1].substring(loc.x + 2, width)
  grid[loc.y + 1] = grid[loc.y + 1].substring(0, loc.x - 1) + '#' + ((grid[loc.y + 1][loc.x]=='-') ? '-' : '?') + '#' + grid[loc.y + 1].substring(loc.x + 2, width)
}
function addNorth(loc) {
  grid[loc.y] = grid[loc.y].substring(0, loc.x - 1) + ((grid[loc.y][loc.x-1]=='|') ? '|' : '?') + '.' + ((grid[loc.y][loc.x+1]=='|') ? '|' : '?') + grid[loc.y].substring(loc.x + 2, width)
  grid[loc.y - 1] = grid[loc.y - 1].substring(0, loc.x - 1) + '#' + ((grid[loc.y-1][loc.x]=='-') ? '-' : '?') + '#' + grid[loc.y - 1].substring(loc.x + 2, width)
  grid[loc.y + 1] = grid[loc.y + 1].substring(0, loc.x - 1) + '#-#' + grid[loc.y + 1].substring(loc.x + 2, width)
}
function addSouth(loc) {
  grid[loc.y] = grid[loc.y].substring(0, loc.x - 1) + ((grid[loc.y][loc.x-1]=='|') ? '|' : '?') + '.' + ((grid[loc.y][loc.x+1]=='|') ? '|' : '?') + grid[loc.y].substring(loc.x + 2, width)
  grid[loc.y - 1] = grid[loc.y - 1].substring(0, loc.x - 1) + '#-#' + grid[loc.y - 1].substring(loc.x + 2, width)
  grid[loc.y + 1] = grid[loc.y + 1].substring(0, loc.x - 1) + '#' + ((grid[loc.y+1][loc.x] == '-') ? '-' : '?') + '#' + grid[loc.y + 1].substring(loc.x + 2, width)
}

function finishThread(locationA) {
  let saveIndex = index; location = copyObj(locationA)
  while (index < input.length) { keepGoing = step()
    if (keepGoing == '|') {
      let parenthesisCount = 0
      while (input[index] != ')' || parenthesisCount != 0) {
        if (input[index] == '(') parenthesisCount++; if (input[index] == ')') parenthesisCount--; index++
      }
    }
  }
  index = saveIndex
}

function diverge() {
  let paths = 0; index++; locationStack.push(copyObj(location))
  while (keepGoing != ')') { keepGoing = step()
    if (keepGoing == '|') {
      locationAStack.push(copyObj(location)); location = copyObj(locationStack.peekBack()); paths++
    }
  }
  for (let i = 0; i < paths; i++) {
    let locationA = locationAStack.pop();
    if (location.x != locationA.x || location.y != locationA.y) { finishThread(locationA) }
  }
  locationStack.pop()
}

function step() {
  if (printing) {
    console.log(input);
    for (i = 0; i < input.length; i++) (i == index) ? process.stdout.write(input[i]) : process.stdout.write(' ')
    console.log();
    console.log("locationStack " + JSON.stringify(locationStack));
    console.log("locationAStack " + JSON.stringify(locationAStack));
  }
  let result = true
  switch (input[index]) {
    case 'W':
      if (location.x != 1) { location.x -= 2; addWest(location); break }
      extendWest(location.y); width += 2; break
    case 'E':
      if (location.x != width - 2) { location.x += 2; addEast(location); break }
      extendEast(location.y); width += 2; location.x += 2; break
    case 'N':
      if (location.y != 1) { location.y -= 2; addNorth(location); break }
      height += 2; extendNorth(location.x); break
    case 'S':
      if (location.y != height - 2) { location.y += 2; addSouth(location); break }
      height += 2; extendSouth(location.x); location.y += 2; break
    case '(':
      diverge(); return true
    case '|':
      result = '|'; break
    case ')':
      result = ')'; break
  }
  index++
  if (printing) printGrid()
  return result
}

initGrid()
locationStack.push(copyObj(location))
oldGrid = copyObj(grid)
printGrid()
while (index < input.length) step()
fillInFinalWalls()
printGrid()
printGrid()

///////

function newGrid() {
  for (h = 0; h < height; h++) {
    for (w = 0; w < width; w++) {
      process.stdout.write(grid[h][w].toString())
    }
    console.log();
  }
  console.log();
}

function initEnds() {
  if (grid[startPosition.y + 1][startPosition.x] == '-') {
    grid[startPosition.y + 2][startPosition.x] = 1; currentEnds.push({ x: startPosition.x, y: startPosition.y + 2})
  }
  if (grid[startPosition.y - 1][startPosition.x] == '-') {
    grid[startPosition.y - 2][startPosition.x] = 1; currentEnds.push({ x: startPosition.x, y: startPosition.y - 2})
  }
  if (grid[startPosition.y][startPosition.x + 1] == '|') {
    grid[startPosition.y][startPosition.x + 2] = 1; currentEnds.push({ x: startPosition.x + 2, y: startPosition.y})
  }
  if (grid[startPosition.y][startPosition.x - 1] == '|') {
    grid[startPosition.y][startPosition.x - 2] = 1; currentEnds.push({ x: startPosition.x - 2, y: startPosition.y})
  }
}

function traverseEnds() {
  let newEnds = []
  currentEnds.forEach(end => {
    if (grid[end.y + 1][end.x] == '-' && grid[end.y + 2][end.x] == '.') {
      grid[end.y + 2][end.x] = grid[end.y][end.x] + 1;
      newEnds.push({ x: end.x, y: end.y + 2})
    }
    if (grid[end.y - 1][end.x] == '-' && grid[end.y - 2][end.x] == '.') {
      grid[end.y - 2][end.x] = grid[end.y][end.x] + 1;
      newEnds.push({ x: end.x, y: end.y - 2})
    }
    if (grid[end.y][end.x - 1] == '|' && grid[end.y][end.x - 2] == '.') {
      grid[end.y][end.x - 2] = grid[end.y][end.x] + 1;
      newEnds.push({ x: end.x - 2, y: end.y})
    }
    if (grid[end.y][end.x + 1] == '|' && grid[end.y][end.x + 2] == '.') {
      grid[end.y][end.x + 2] = grid[end.y][end.x] + 1;
      newEnds.push({ x: end.x + 2, y: end.y})
    }
  })
  currentEnds = newEnds
}

let startPosition = locationStack.pop()
grid[startPosition.y][startPosition.x] = 0
initEnds()


counter = 0
while (currentEnds.length > 0) {
  traverseEnds()
  //newGrid()
  counter++
}

console.log("Longest: " + counter);


let total = 0

for (h = 0; h < height; h++) {
  for (w = 0; w < width; w++) {
    if (!isNaN(grid[h][w]) && grid[h][w] >= 1000) total++
  }
}

console.log("Total atleast 1000: " + total);



