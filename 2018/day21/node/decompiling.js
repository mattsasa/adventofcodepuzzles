// #ip 4

// 0, 1, 2, 3, 4,  5
// a, b, c, d, ip, f

//0 // seti 123 0 3     # d = 123
//1 // bani 3 456 3     # d = d & 456
//2 // eqri 3 72 3      # d = (d == 72)
//3 // addr 3 4 4       # ip = d + ip
//4 // seti 0 0 4       # ip = 0
//5 // seti 0 5 3       # d = 0   /// Done infinite loop check
//6 // bori 3 65536 2   # c = d & 65536
//7 // seti 7637914 8 3 # d = 7637914
//8 // bani 2 255 1     # b = c & 255
//9 // addr 3 1 3       # d += b
//10// bani 3 16777215 3# d = d & 16777215
//11// muli 3 65899 3   # d *= 65899
//12// bani 3 16777215 3# d = d & 16777215
//13// gtir 256 2 1     # b = (256 > c)
//14// addr 1 4 4       # ip += b
//15// addi 4 1 4       # ip += 1
//16// seti 27 1 4      # ip = 27  ///jumps to 27(28) if (256 > c)
//17// seti 0 7 1       # b = 0    ///  if not (256 > c)
//18// addi 1 1 5       # f = b + 1
//19// muli 5 256 5     # f *= 256
//20// gtrr 5 2 5       # f = (f > c)
//21// addr 5 4 4       # ip += f
//22// addi 4 1 4       # ip += 1
//23// seti 25 3 4      # ip = 25  /// jumps to 25(26) if (f > c)
//24// addi 1 1 1       # b++      /// if not (f > c)
//25// seti 17 0 4      # ip = 17  /// jumps back to 17(18) if not (f > c)
//26// setr 1 8 2       # c = b
//27// seti 7 7 4       # ip = 7  // jumpts to 7(8)
//28// eqrr 3 0 1       # b = (a == d)  // causes halt
//29// addr 1 4 4       # ip += b  // causes halt if (a == d)
//30// seti 5 5 4       # ip = 5  /// jump back to 5(6) if not (a == d)

let a = 100, b = 0, c = 0, d = 0, f = 0

function newSection() {
  b = 0
  while (true) {
    f = b + 1
    f *= 256  /// appends 8 zero bits to the end of number
    /// c = 65536
    if (f > c) {  // c is 65536, f will be (b + 1) * 256
      /// jump to 26, top of main loop
      c = b   /// can calulate what b would be... (b+1)*256 > 65536
      /// jump to checkpoint
      break
    } else { b++ }
  }
}

function checkForHalt() {
  //console.log("D: " + d);
  if (a == d) {  /// a we can pick,  d changes over time
    console.log("Halted!");
    process.exit()
  }
}

d = 0
while (true) {
  breakCheckPoint = true
  c = d & 65536 // the result c will either be 0 or 65536
  d = 7637914
  /// jump target
  while (breakCheckPoint) {
    b = c & 255 // (bottom 8 bits stay the same, top bits set to 0)  ( subtracts 256 ) (or multiple til its below 256)
    d += b
    d &= 16777215  /// max 3 byte int, (bottom 24 bits stay the same, top bits set to 0)
    d *= 65899
    d &= 16777215  /// 3 byte overflow
    console.log("D: " + d);
    if(256 > c)  {  // if condition on line 42, if d was 65535 or less, then set b to true, otherwise b false
      checkForHalt()
      breakCheckPoint = false
    } else {
      newSection()
      /// sets c to something we can calulate, f and b would also be modifed
    }
  }

}
