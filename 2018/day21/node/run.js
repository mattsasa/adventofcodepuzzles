function addi(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] + instruction[2]
}
function addr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] + registers[instruction[2]]
}
function mulr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] * registers[instruction[2]]
}
function muli(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] * instruction[2]
}
function banr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] & registers[instruction[2]]
}
function bani(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] & instruction[2]
}
function borr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] | registers[instruction[2]]
}
function bori(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]] | instruction[2]
}
function setr(registers, instruction){
  registers[instruction[3]] = registers[instruction[1]]
}
function seti(registers, instruction){
  registers[instruction[3]] = instruction[1]
}
function gtir(registers, instruction){
  instruction[1] > registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function gtri(registers, instruction){
  registers[instruction[1]] > instruction[2] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function gtrr(registers, instruction){
  registers[instruction[1]] > registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function eqir(registers, instruction){
  instruction[1] == registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function eqri(registers, instruction){
  registers[instruction[1]] == instruction[2] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
function eqrr(registers, instruction){
  registers[instruction[1]] == registers[instruction[2]] ? registers[instruction[3]] = 1 : registers[instruction[3]] = 0
}
let opcodes = {
  "muli": muli,
  "seti": seti,
  "bani": bani,
  "gtri": gtri,
  "gtrr": gtrr,
  "eqrr": eqrr,
  "addi": addi,
  "gtir": gtir,
  "eqir": eqir,
  "mulr": mulr,
  "addr": addr,
  "borr": borr,
  "bori": bori,
  "eqri": eqri,
  "banr": banr,
  "setr": setr
}

const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
const instructions = []

for (i = 1; i < lines.length; i++) {
  let words = lines[i].split(" ")
  instructions.push( { opcode: words[0], instruction: [ null, parseInt(words[1]), parseInt(words[2]), parseInt(words[3]) ] } )
}

let registers = [0, 0, 0, 0, 0, 0], ip = 4

let stack = []

while (true) {
  //console.log(registers);
  if (registers[ip] > instructions.length - 1) {
    console.log("end"); console.log(registers); break;
  }
  if (registers[ip] == 28) {
    if (stack.includes(registers[3])) {
      /// do nothing
      console.log("duplicate");
      console.log(stack[stack.length - 1]);
      process.exit()
    } else {
      //if (stack.length > 100000) console.log("new value: " + registers[3])
      //else console.log("stack length: " + stack.length);
      stack.push(registers[3])
    }
  }
  let pc = registers[ip]
  opcodes[instructions[pc].opcode](registers, instructions[pc].instruction)
  registers[ip]++
}