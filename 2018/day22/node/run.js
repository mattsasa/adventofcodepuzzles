let cave_depth = 3558
let target = { w: 15, h: 740 }
let buffer_length = 200

let map = []

for (h = 0; h <= target.h + buffer_length; h++) {
  let row = []
  for (w = 0; w <= target.w + buffer_length; w++) {
    row.push(0)
  }
  map.push(row)
}

function geoIdxToErosLvl(geoIndex) { return (geoIndex + cave_depth) % 20183 }

function doHorizontalRow(index) {
  let height = index
  for (w = index; w <= target.w + buffer_length; w++) {
    if (height == target.h && w == target.w) continue
    map[height][w] = geoIdxToErosLvl(map[height - 1][w] * map[height][w - 1])
  }
}

function doVerticalColumn(index) {
  let width = index
  for (h = index + 1; h <= target.h + buffer_length; h++) {
    if (h == target.h && width == target.w) continue
    map[h][width] = geoIdxToErosLvl(map[h - 1][width] * map[h][width - 1])
  }
}

///  First Rule
map[0][0] = geoIdxToErosLvl(0)

/// Second Rule
map[target.h][target.w] = geoIdxToErosLvl(0)

/// Third Rule
for (w = 1; w <= target.w + buffer_length; w++) map[0][w] = geoIdxToErosLvl(w * 16807)

/// Fourth Rule
for (h = 1; h <= target.h + buffer_length; h++) map[h][0] = geoIdxToErosLvl(h * 48271)

/// Fifth Rule
for (let x = 1; x <= target.w + buffer_length; x++) {
  doHorizontalRow(x)
  doVerticalColumn(x)
}

let riskLevel = 0
/// Convert all to Characters
//map[0][0] = 'M'; map[target.h][target.w] = 'T'
for (h = 0; h <= target.h + buffer_length; h++) {
  for (w = 0; w <= target.w + buffer_length; w++) {
    riskLevel += (map[h][w] % 3)
    if (map[h][w] % 3 == 0) map[h][w] = '.'
    else if (map[h][w] % 3 == 1) map[h][w] = '='
    else if (map[h][w] % 3 == 2) map[h][w] = '|'
  }
}

function printMap() {
  for (h = 0; h <= target.h + buffer_length; h++) {
    for (w = 0; w <= target.w + buffer_length; w++) {
      process.stdout.write(map[h][w])
    }
    console.log();
  }
  console.log();
}

printMap()
console.log();
console.log("Risk Level: " + riskLevel);

//// Phase 2

function copyObj(obj) { return JSON.parse(JSON.stringify(obj)) }

let ends = [], stepsMap = [], completeTrips = []
for (h = 0; h <= target.h + buffer_length; h++) {
  let row = []
  for (w = 0; w <= target.w + buffer_length; w++) {
    row.push({ niether: 99999999999999, torch: 99999999999999, climbing: 99999999999999 })
  }
  stepsMap.push(row)
}
/// start as Torch

function initEnds() {
  if (map[0][1] == '|') {
    stepsMap[0][1] = 1; ends.push({ h: 0, w: 1, state: 'torch', steps: 1, switches: 0})
  }
  if (map[1][0] == '.') {
    stepsMap[1][0] = 1; ends.push({ h: 1, w: 0, state: 'torch', steps: 1, switches: 0})
  }
  stepsMap[0][0] = 0;
}

function lowestStepsFirst(endA, endB) { return endA.steps < endB.steps }

function checkCompatibility(state, area) {
  if (state == 'torch' && area == '=') return false
  if (state == 'climbing' && area == '|') return false
  if (state == 'niether' && area == '.') return false
  return true
}

function findCorrectEquipment(state, area, orig) {
  let choice
  if (state == 'torch')  {
    (checkCompatibility('niether', area) && checkCompatibility('niether', orig)) ? choice = 'niether' : choice = 'climbing'
  }
  if (state == 'niether')  {
    (checkCompatibility('torch', area) && checkCompatibility('torch', orig)) ? choice = 'torch' : choice = 'climbing'
  }
  if (state == 'climbing')  {
    (checkCompatibility('torch', area) && checkCompatibility('torch', orig)) ? choice = 'torch' : choice = 'niether'
  }
  return choice
}

function checkNewArea(end, orig)  {
  //if (stepsMap[end.h][end.w] <= end.steps ) return null

  if (checkCompatibility(end.state, map[end.h][end.w])) {
    /////Move there in 1 minute
    end.steps++
    if (stepsMap[end.h][end.w][end.state] > end.steps) {
      stepsMap[end.h][end.w][end.state] = end.steps
      //end.breadcrumbs.push({ h: end.h, w: end.w })
      return end
    }
  }
  else { // changing gear
    end.steps += 8
    end.switches++
    end.state = findCorrectEquipment(end.state, map[end.h][end.w], map[orig.h][orig.w])
    if (stepsMap[end.h][end.w][end.state] > end.steps) {
      stepsMap[end.h][end.w][end.state] = end.steps
      //end.breadcrumbs.push({ h: end.h, w: end.w })
      return end
    }
  }
  return null
}

function traverseEnds() {
  let newEnds = []
  ends.sort(lowestStepsFirst)
  ends.forEach(end => {
    //Check South
    let orig = copyObj(end)
    if (map[end.h + 1] != null && map[end.h + 1][end.w] != null) {
      end.h++
      let newEnd = checkNewArea(copyObj(end), orig)
      if (newEnd != null && newEnd.h == target.h && newEnd.w == target.w) completeTrips.push(copyObj(newEnd))
      else if (newEnd != null) { newEnds.push(copyObj(newEnd)) }
      end = copyObj(orig)
    }
    //Check North
    if (map[end.h - 1] != null && map[end.h - 1][end.w] != null) {
      end.h--
      let newEnd = checkNewArea(copyObj(end), orig)
      if (newEnd != null && newEnd.h == target.h && newEnd.w == target.w) completeTrips.push(copyObj(newEnd))
      if (newEnd != null) { newEnds.push(copyObj(newEnd)) }
      end = copyObj(orig)
    }
    //Check West
    if (map[end.h][end.w - 1] != null) {
      end.w--
      let newEnd = checkNewArea(copyObj(end), orig)
      if (newEnd != null && newEnd.h == target.h && newEnd.w == target.w) completeTrips.push(copyObj(newEnd))
      if (newEnd != null) { newEnds.push(copyObj(newEnd)) }
      end = copyObj(orig)
    }
    //Check East
    if (map[end.h][end.w + 1] != null) {
      end.w++
      let newEnd = checkNewArea(copyObj(end), orig)
      if (newEnd != null && newEnd.h == target.h && newEnd.w == target.w) completeTrips.push(copyObj(newEnd))
      if (newEnd != null) { newEnds.push(copyObj(newEnd)) }
      end = copyObj(orig)
    }
  })
  //console.log(newEnds);
  ends = newEnds
}

//initEnds()
stepsMap[0][0] = 0; ends.push({ h: 0, w: 0, state: 'torch', steps: 0, switches: 0 })


while (ends.length > 0) traverseEnds()

completeTrips.forEach(trip => {
  if (trip.state == "climbing") { trip.switches++; trip.steps += 7; trip.state = "torch" }
})

console.log("Complete Trips");
console.log(completeTrips);

let shortestTrip = completeTrips[0]
completeTrips.forEach(trip => { if (trip.steps < shortestTrip.steps) shortestTrip = trip })

console.log("Shortest Trip: ");
console.log(shortestTrip);

//completeTrips[3].breadcrumbs.forEach(loc => map[loc.h][loc.w] = '*')
//map[0][0] = 'M'; map[target.h][target.w] = 'T'
//printMap()

