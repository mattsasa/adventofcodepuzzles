const fs = require('fs')
let lines = fs.readFileSync('../data/input.txt').toString().split("\n")
let nanoBots = []
lines.forEach(line => {
  let bot = {}
  const firstCommaIndex = line.indexOf(',')
  bot.x = parseInt(line.substring(5, firstCommaIndex))
  const leftOver = line.substring(firstCommaIndex + 1, line.length)
  const secondCommaIndex = leftOver.indexOf(',')
  const greaterThanIndex = leftOver.indexOf('>')
  const equalsIndex = leftOver.indexOf('=')
  bot.y = parseInt(leftOver.substring(0, secondCommaIndex))
  bot.z = parseInt(leftOver.substring(secondCommaIndex + 1, greaterThanIndex))
  bot.r = parseInt(leftOver.substring(equalsIndex + 1, leftOver.length))
  nanoBots.push(bot)
})

let xMin = 999999999, yMin = 999999999, zMin = 999999999
let xMax = 0, yMax = 0, zMax = 0
nanoBots.forEach(bot => {
  if (bot.x < xMin) xMin = bot.x
  if (bot.y < yMin) yMin = bot.y
  if (bot.z < zMin) zMin = bot.z
  if (bot.x > xMax) xMax = bot.x
  if (bot.y > yMax) yMax = bot.y
  if (bot.z > zMax) zMax = bot.z
})

let voxelArray = [], bestNumBots = nanoBots.length, voxelSize = 0
const slice_factor = 2
const startVoxelSize = Math.ceil((xMax - xMin) / 100000000) * 100000000

function getDimensions(x, y, z) {
  return {
    minX: x,
    maxX: x + voxelSize,
    minY: y,
    maxY: y + voxelSize,
    minZ: z,
    maxZ: z + voxelSize
  }
}

function botInRangeOfVoxel(x, y, z, bot) {
  const dimensions = getDimensions(x, y, z)

  let distance = 0
  if (bot.x < dimensions.minX) distance += Math.abs(dimensions.minX - bot.x)
  else if (bot.x > dimensions.maxX) distance += Math.abs(bot.x - dimensions.maxX)
  if (bot.y < dimensions.minY) distance += Math.abs(dimensions.minY - bot.y)
  else if (bot.y > dimensions.maxY) distance += Math.abs(bot.y - dimensions.maxY)
  if (bot.z < dimensions.minZ) distance += Math.abs(dimensions.minZ - bot.z)
  else if (bot.z > dimensions.maxZ) distance += Math.abs(bot.z - dimensions.maxZ)

  return (distance <= bot.r)
}

function sliceUpVoxel(x, y, z) {
  let arrayOfVoxels = []
  for (let x2 = x; x2 < x + voxelSize; x2 += Math.ceil(voxelSize / slice_factor)) {
    for (let y2 = y; y2 < y + voxelSize; y2 += Math.ceil(voxelSize / slice_factor)) {
      for (let z2 = z; z2 < z + voxelSize; z2 += Math.ceil(voxelSize / slice_factor)) {
        arrayOfVoxels.push({ x: x2, y: y2, z: z2, bots: 0 })
      }
    }
  }
  return arrayOfVoxels
}

function sliceBestIntoSmallerVoxels() {
  let newVoxels = []
  voxelArray.forEach(voxel => {
    if (voxel.bots >= bestNumBots) {
      newVoxels = newVoxels.concat(sliceUpVoxel(voxel.x, voxel.y, voxel.z))
    }
  })
  voxelArray = newVoxels
  voxelSize = Math.ceil(voxelSize / slice_factor)
}

function populateSlicedVoxels() {
  nanoBots.forEach(bot => {
    voxelArray.forEach(voxel => {
      if (botInRangeOfVoxel(voxel.x, voxel.y, voxel.z, bot)) voxel.bots++
    })
  })
}

while (voxelArray.length == 0) {

  //Init a single voxel that covers entire range of bots
  voxelArray = [{ x: xMin, y: yMin, z: zMin, bots: 1000 }]
  voxelSize = startVoxelSize

  while (voxelSize > slice_factor) {
    //Slice the best voxels into smaller voxels
    sliceBestIntoSmallerVoxels()

    /// Populate sliced voxels with bot count
    populateSlicedVoxels()
  }
  bestNumBots--
}

/// Final Steps
let closestDist = 9999999999999
voxelArray.forEach(voxel => {
  if (voxel.bots >= bestNumBots) {
    for (let x = voxel.x; x < voxel.x + voxelSize; x++) {
      for (let y = voxel.y; y < voxel.y + voxelSize; y++) {
        for (let z = voxel.z; z < voxel.z + voxelSize; z++) {
          counter = 0
          nanoBots.forEach(bot => {
            const distance = Math.abs(bot.x - x) + Math.abs(bot.y - y) + Math.abs(bot.z - z)
            if (distance <= bot.r) counter++
          })
          if (counter >= bestNumBots) {
            let distance = Math.abs(x) + Math.abs(y) + Math.abs(z)
            if (distance < closestDist) closestDist = distance
          }
        }
      }
    }
  }
})
console.log("Shortest Distance: " + closestDist);







