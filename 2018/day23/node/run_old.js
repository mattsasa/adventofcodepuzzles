const fs = require('fs')
const JSONStream = require( "JSONStream" )
let lines = fs.readFileSync('../data/input.txt').toString().split("\n")
let nanoBots = []
lines.forEach(line => {
  let bot = {}
  const firstCommaIndex = line.indexOf(',')
  bot.x = parseInt(line.substring(5, firstCommaIndex))
  const leftOver = line.substring(firstCommaIndex + 1, line.length)
  const secondCommaIndex = leftOver.indexOf(',')
  const greaterThanIndex = leftOver.indexOf('>')
  const equalsIndex = leftOver.indexOf('=')
  bot.y = parseInt(leftOver.substring(0, secondCommaIndex))
  bot.z = parseInt(leftOver.substring(secondCommaIndex + 1, greaterThanIndex))
  bot.r = parseInt(leftOver.substring(equalsIndex + 1, leftOver.length))
  nanoBots.push(bot)
})

// Part 1
// let bestBot = nanoBots[0], counter = 0
// nanoBots.forEach(bot => { if (bot.r > bestBot.r) bestBot = bot })
// nanoBots.forEach(bot => {
//   const distance = Math.abs(bot.x - bestBot.x) + Math.abs(bot.y - bestBot.y) + Math.abs(bot.z - bestBot.z)
//   if (distance < bestBot.r) counter++
// })
// console.log(counter);


let xMin = 999999999, yMin = 999999999, zMin = 999999999
let xMax = 0, yMax = 0, zMax = 0

nanoBots.forEach(bot => {
  if (bot.x < xMin) xMin = bot.x
  if (bot.y < yMin) yMin = bot.y
  if (bot.z < zMin) zMin = bot.z
  if (bot.x > xMax) xMax = bot.x
  if (bot.y > yMax) yMax = bot.y
  if (bot.z > zMax) zMax = bot.z
})

let voxel_size = 1000000, sub_voxel_size = 25000, sub_sub_size = 1000
let voxelGrid = []
// for (let i = xMin; i < xMax; i += voxel_size) {
//   let planeGrid = []
//   for (let j = yMin; j < yMax; j += voxel_size) {
//     let bucketArray = []
//     for (let k = zMin; k < zMax; k += voxel_size) {
//       bucketArray.push(0)
//     }
//     planeGrid.push(bucketArray)
//   }
//   voxelGrid.push(planeGrid)
// }

function getDimensions(i, j, k) {
  //let minX = xMin + (i * voxel_size)
  //let minY = yMin + (j * voxel_size)
  //let minZ = zMin + (k * voxel_size)
  return {
    minX: i,
    maxX: i +  sub_sub_size,
    minY: j,
    maxY: j + sub_sub_size,
    minZ: k,
    maxZ: k + sub_sub_size
  }
}

function botInRangeOfVoxel(bot, i, j, k) {
  const dimensions = getDimensions(i, j, k)
  //Find closest point in the Voxel
  const coord = { x: bot.x, y: bot.y, z: bot.z }
  // modify point
  if (coord.x < dimensions.minX) coord.x = dimensions.minX
  else if (coord.x > dimensions.maxX) coord.x = dimensions.maxX
  if (coord.y < dimensions.minY) coord.y = dimensions.minY
  else if (coord.y > dimensions.maxY) coord.y = dimensions.maxY
  if (coord.z < dimensions.minZ) coord.z = dimensions.minZ
  else if (coord.z > dimensions.maxZ) coord.z = dimensions.maxZ

  //Measure the distance from the bot that point
  const distance = Math.abs(bot.x - coord.x) + Math.abs(bot.y - coord.y) + Math.abs(bot.z - coord.z)
  //Check if that distance is greater than bot range/radius
  return (distance < bot.r)
}

// nanoBots.forEach(bot => {
//   let radius = Math.ceil(bot.r / voxel_size)
//   let xi = Math.floor((bot.x - xMin) / voxel_size)
//   let yi = Math.floor((bot.y - yMin) / voxel_size)
//   let zi = Math.floor((bot.z - zMin) / voxel_size)
//   for (let i = Math.max(xi - radius, 0); i < Math.min(xi + radius, voxelGrid.length - 1); i++) {
//     for (let j = Math.max(yi - radius, 0); j < Math.min(yi + radius, voxelGrid[i].length - 1); j++) {
//       for (let k = Math.max(zi - radius, 0); k < Math.min(zi + radius, voxelGrid[i][j].length - 1); k++) {
//         if (botInRangeOfVoxel(bot, i, j, k)) voxelGrid[i][j][k]++
//       }
//     }
//   }
// })

let best_i = 0, best_j = 0, best_k = 0, filterCount = 0, totalVoxels = 0, bestCount = 0, bestVoxel = 0
let subVoxels = [], subSubVoxels = []

//fs.writeFileSync('../data/voxelGrid1000000atleastPartInRange.json', JSON.stringify(voxelGrid))
//console.log("written");

//voxelGrid = JSON.parse(fs.readFileSync('../data/voxelGrid1000000atleastPartInRange.json').toString())
//subVoxels = JSON.parse(fs.readFileSync('../data/subsubnotpopulated.json').toString())
//subSubVoxels = JSON.parse(fs.readFileSync('../data/subsubnotpopulated.json').toString())


const readInJSON = () => new Promise((resolve, reject) => {
  const parser = JSONStream.parse('*')
  parser.on('data', function(data) { subSubVoxels.push(data) })
  parser.on("end", resolve)
  fs.createReadStream('../data/subsubfilled.json', {flags: 'r', encoding: 'utf-8'}).pipe(parser)
})

run()

async function run() {

  //await readInJSON()
  //console.log("finished reading")

  /// Populate subVoxels
  // nanoBots.forEach(bot => {
  //   for (let i = 0; i < subSubVoxels.length; i++) {
  //     console.log(subSubVoxels.length);
  //     console.log(i);
  //     for (let j = 0; j < subSubVoxels[i].length; j++) {
  //       for (let k = 0; k < subSubVoxels[i][j].length; k++) {
  //         let subVoxel = subSubVoxels[i][j][k]
  //         if (botInRangeOfVoxel(bot, subVoxel.i, subVoxel.j, subVoxel.k)) subSubVoxels[i][j][k].bots++
  //       }
  //     }
  //   }
  // })
  // console.log("finished populating")

let finalCoords = JSON.parse(fs.readFileSync('../data/finalCoords.json').toString())

let closestObj = finalCoords[0]
let closestDist = Math.abs(closestObj.x) + Math.abs(closestObj.y) + Math.abs(closestObj.z)
finalCoords.forEach(coord => {
  let distance = Math.abs(coord.x) + Math.abs(coord.y) + Math.abs(coord.z)
  if (distance < closestDist) { closestObj = coord; closestDist = distance }

  counter = 0
  nanoBots.forEach(bot => {
    const distance = Math.abs(bot.x - coord.x) + Math.abs(bot.y - coord.y) + Math.abs(bot.z - coord.z)
    if (distance < bot.r) counter++
  })
  console.log(counter);
})

//console.log(closestDist);
//console.log(closestObj);

//////Diagnostics Filtering tools and initalizing subvoxels and more
// for (let i = 0; i < subSubVoxels.length; i++) {
//   for (let j = 0; j < subSubVoxels[i].length; j++) {
//     for (let k = 0; k < subSubVoxels[i][j].length; k++) {
//       totalVoxels++
//       if (subSubVoxels[i][j][k].bots > bestVoxel) {
//         bestVoxel = subSubVoxels[i][j][k].bots
//         // i2 = xMin + (i * voxel_size)
//         // j2 = yMin + (j * voxel_size)
//         // k2 = zMin + (k * voxel_size)
//       }
//       if (subSubVoxels[i][j][k].bots >= 972) {
//         filterCount++
//         // for (let x = subSubVoxels[i][j][k].i; x < subSubVoxels[i][j][k].i + sub_sub_size; x++) {
//         //   for (let y = subSubVoxels[i][j][k].j; y < subSubVoxels[i][j][k].j + sub_sub_size; y++) {
//         //     for (let z = subSubVoxels[i][j][k].k; z < subSubVoxels[i][j][k].k + sub_sub_size; z++) {
//         //       counter = 0
//         //       nanoBots.forEach(bot => {
//         //         const distance = Math.abs(bot.x - x) + Math.abs(bot.y - y) + Math.abs(bot.z - z)
//         //         if (distance < bot.r) counter++
//         //       })
//         //       //console.log(counter);
//         //       if (counter == 972) finalCoords.push({ x, y, z })
//         //       if (counter > bestCount) bestCount = counter
//         //     }
//         //   }
//         // }
//         // console.log(filterCount);
//         // i2 = subVoxels[i][j][k].i
//         // j2 = subVoxels[i][j][k].j
//         // k2 = subVoxels[i][j][k].k
//         // for (let l = i2; l < i2 + sub_voxel_size; l += sub_sub_size) {
//         //   let planeGrid = []
//         //   for (let m = j2; m < j2 + sub_voxel_size; m += sub_sub_size) {
//         //     let bucketArray = []
//         //     for (let n = k2; n < k2 + sub_voxel_size; n += sub_sub_size) {
//         //       bucketArray.push({ i: l, j: m, k: n, bots: 0 })
//         //       // counter = 0
//         //       // nanoBots.forEach(bot => {
//         //       //   const distance = Math.abs(bot.x - l) + Math.abs(bot.y - m) + Math.abs(bot.z - n)
//         //       //   if (distance < bot.r) counter++
//         //       // })
//         //       // console.log(counter);
//         //       // if (counter > bestCount) bestCount = counter
//         //     }
//         //     planeGrid.push(bucketArray)
//         //   }
//         //   subSubVoxels.push(planeGrid)
//         // }
//       }
//     }
//   }
// }

console.log("filterCount: " + filterCount);
console.log("totalVoxels: " + totalVoxels);
console.log("bestCount: " + bestCount);
console.log("bestVoxel: " + bestVoxel);


// let writeStream = fs.createWriteStream('../data/subsubfilled.json')
//
// writeStream.write('[')
// // write some data with a base64 encoding
// for (let i = 0; i < subSubVoxels.length; i++) {
//   console.log(i);
//   console.log(subSubVoxels.length);
//   let part = JSON.stringify(subSubVoxels[i])
//   if (i != subSubVoxels.length - 1) part += ','
//   writeStream.write(part)
// }
// writeStream.write(']')
//
//
// // the finish event is emitted when all data has been flushed from the stream
// writeStream.on('finish', () => {
//     console.log('wrote all data to file')
// })
//
// // close the stream
// writeStream.end()
//
// //fs.writeFileSync('../data/subsubnotpopulated.json', transformStream)
// console.log("done writing")

// for (let l = i2; l < i2 + voxel_size; l+=100) {
//   for (let m = j2; m < j2 + voxel_size; m+=100) {
//     for (let n = k2; n < k2 + voxel_size; n+=100) {
//       counter = 0
//       nanoBots.forEach(bot => {
//         const distance = Math.abs(bot.x - l) + Math.abs(bot.y - m) + Math.abs(bot.z - n)
//         if (distance < bot.r) counter++
//       })
//       if (counter > bestCount) bestCount = counter
//     }
//   }
// }
// console.log("bestCount: " + bestCount);


//244...884...926...959...972: likely the largest number.
// Total Voxels: 20020992   175 of 20020992
// SubVoxels down to 11.2 million   1434 of 11200000
// Sub Sub  5 of 24468750

console.log("finish");

}
