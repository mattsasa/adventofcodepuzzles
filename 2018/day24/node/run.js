/////Mine
const immuneArmy = [
  { numUnits: 7079, hp: 12296, weakTo: ["fire"], immuneTo: [], attackDmg: 13, attackType: "bludgeoning", initiative: 14 },
  { numUnits: 385, hp: 9749, weakTo: ["cold"], immuneTo: [], attackDmg: 196, attackType: "bludgeoning", initiative: 16 },
  { numUnits: 2232, hp: 1178, weakTo: ["cold", "slashing"], immuneTo: [], attackDmg: 4, attackType: "fire", initiative: 20 },
  { numUnits: 917, hp: 2449, weakTo: ["bludgeoning"], immuneTo: ["fire","cold"], attackDmg: 25, attackType: "cold", initiative: 15 },
  { numUnits: 2657, hp: 2606, weakTo: ["slashing"], immuneTo: [], attackDmg: 9, attackType: "cold", initiative: 13 },
  { numUnits: 2460, hp: 7566, weakTo: [], immuneTo: [], attackDmg: 29, attackType: "cold", initiative: 8 },
  { numUnits: 2106, hp: 6223, weakTo: [], immuneTo: [], attackDmg: 29, attackType: "bludgeoning", initiative: 2 },
  { numUnits: 110, hp: 7687, weakTo: ["slashing"], immuneTo: ["radiation", "fire"], attackDmg: 506, attackType: "slashing", initiative: 19 },
  { numUnits: 7451, hp: 9193, weakTo: [], immuneTo: ["cold"], attackDmg: 12, attackType: "radiation", initiative: 6 },
  { numUnits: 1167, hp: 3162, weakTo: ["fire"], immuneTo: ["bludgeoning"], attackDmg: 23, attackType: "fire", initiative: 9 }
]


const infectionArmy = [
  { numUnits: 2907, hp: 11244, weakTo: [], immuneTo: ["slashing"], attackDmg: 7, attackType: "fire", initiative: 7 },
  { numUnits: 7338, hp: 12201, weakTo: [], immuneTo: ["bludgeoning","slashing","cold"], attackDmg: 3, attackType: "radiation", initiative: 4 },
  { numUnits: 7905, hp: 59276, weakTo: [], immuneTo: ["fire"], attackDmg: 12, attackType: "cold", initiative: 17 },
  { numUnits: 1899, hp: 50061, weakTo: ["fire"], immuneTo: [], attackDmg: 51, attackType: "radiation", initiative: 10 },
  { numUnits: 2711, hp: 27602, weakTo: [], immuneTo: [], attackDmg: 17, attackType: "cold", initiative: 12 },
  { numUnits: 935, hp: 38240, weakTo: [], immuneTo: ["slashing"], attackDmg: 78, attackType: "bludgeoning", initiative: 1 },
  { numUnits: 2783, hp: 17937, weakTo: [], immuneTo: ["cold","bludgeoning"], attackDmg: 12, attackType: "fire", initiative: 11 },
  { numUnits: 8046, hp: 13608, weakTo: ["fire","bludgeoning"], immuneTo: [], attackDmg: 2, attackType: "slashing", initiative: 5 },
  { numUnits: 2112, hp: 37597, weakTo: [], immuneTo: ["cold","slashing"], attackDmg: 31, attackType: "slashing", initiative: 18 },
  { numUnits: 109, hp: 50867, weakTo: ["radiation"], immuneTo: ["slashing"], attackDmg: 886, attackType: "cold", initiative: 3 }
]

immuneArmy.forEach(army => army.team = "immune")
infectionArmy.forEach(army => army.team = "infection")

let boost = 55

immuneArmy.forEach(army => army.attackDmg += boost)

let allArmies = infectionArmy.concat(immuneArmy)

allArmies.forEach(army => { army.chosen = false; army.target = null })

function strongestPicksFirst(armyA, armyB) {
  if (armyA.numUnits * armyA.attackDmg == armyB.numUnits * armyB.attackDmg) return armyA.initiative < armyB.initiative ? 1 : -1
  else return armyA.numUnits * armyA.attackDmg < armyB.numUnits * armyB.attackDmg ? 1 : -1
}

function initiativeAttacksFirst(armyA, armyB) { return armyA.initiative < armyB.initiative ? 1 : -1 }

function pickBetterTarget(targetA, targetB, attacker) {

  let damageB = attacker.attackDmg * attacker.numUnits
  if (targetB.immuneTo.includes(attacker.attackType)) damageB = 0
  if (targetB.weakTo.includes(attacker.attackType)) damageB *= 2

  if (damageB == 0) return targetA
  if (targetA == null) return targetB

  let damageA = attacker.attackDmg * attacker.numUnits
  if (targetA.immuneTo.includes(attacker.attackType)) damageA = 0
  if (targetA.weakTo.includes(attacker.attackType)) damageA *= 2

  let strengthA = targetA.attackDmg * targetA.numUnits
  let strengthB = targetB.attackDmg * targetB.numUnits

  if (damageA > damageB) return targetA
  if (damageA < damageB) return targetB

  if (strengthA > strengthB) return targetA
  if (strengthA < strengthB) return targetB

  if (targetA.initiative > targetB.initiative) return targetA
  if (targetA.initiative < targetB.initiative) return targetB
}

function teamStillHasUnits(team) {
  let bool = false
  allArmies.forEach(army => {
    if (army.team == team) bool = true
  })
  return bool
}

let round = 1
while (teamStillHasUnits("infection") && teamStillHasUnits("immune")) {
  console.log("Round " + round + ":  ");
  //// Target Selection
  allArmies.sort(strongestPicksFirst)
  allArmies.forEach(army => {
    let target = null
    ///// Pick the right target
    allArmies.forEach(enemy => {
      if (enemy.team != army.team && !enemy.chosen) {
        target = pickBetterTarget(target, enemy, army)
      }
    })
    if (target != null) { army.target = target; target.chosen = true }
  })
  /// Attack Phase
  allArmies.sort(initiativeAttacksFirst)
  allArmies.forEach(army => {
    if (army.numUnits > 0 && army.target != null) {
      let damage = army.attackDmg * army.numUnits
      if (army.target.weakTo.includes(army.attackType)) damage *= 2
      console.log(`   ${army.team} ${army.id} attacks ${army.target.team} ${army.target.id} for ${damage} damage      ${Math.floor(damage/army.target.hp)} of ${army.target.numUnits} killed.`);
      army.target.numUnits -= Math.floor(damage/army.target.hp)
    }
  })

  ///Housekeeping
  allArmies = allArmies.filter(army => army.numUnits > 0)
  allArmies.forEach(army => { army.chosen = false; army.target = null })
  round++
  console.log();
}

let remainingUnits = 0
allArmies.forEach(army => remainingUnits += army.numUnits)
console.log(remainingUnits);
console.log("Winning Team: " + allArmies[0].team);


