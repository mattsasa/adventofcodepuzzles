const fs = require('fs')
let lines = fs.readFileSync('../data/input.txt').toString().split("\n")
let points = []
lines.forEach(line => { points.push(line.split(",")) })

function distance4D(pointA, pointB) {
  return Math.abs(pointA[0] - pointB[0]) + Math.abs(pointA[1] - pointB[1]) + Math.abs(pointA[2] - pointB[2]) + Math.abs(pointA[3] - pointB[3])
}

let constellations = []

function tryToMatchOnePoint() {
  for (let i = 0; i < points.length; i++) {
    ///Search every all constellations
    for (let c = 0; c < constellations.length; c++) {
      for (let p = 0; p < constellations[c].length; p++) {
        if (distance4D(constellations[c][p], points[i]) <= 3) {
          constellations[c].push(points[i])
          points.splice(i, 1);
          return true
        }
      }
    }

    ///Then search every unmatched point
    for (let j = 0; j < points.length; j++) {
      if( i != j && distance4D(points[i], points[j]) <= 3) {
        constellations.push([points[i], points[j]])
        if (i > j) { points.splice(i, 1); points.splice(j, 1) }
        else { points.splice(j, 1); points.splice(i, 1) }
        return true
      }
    }
  }
  return false
}

console.log("Step 1");

while (tryToMatchOnePoint()) {}

///Match constellations together

function doConstellationsOverlap(constelA, constelB) {
  for (let a = 0; a < constelA.length; a++) {
    let pointA = constelA[a]
    for (let b = 0; b < constelB.length; b++) {
      let pointB = constelB[b]
      if (distance4D(pointA, pointB) <= 3) return true
    }
  }
  return false
}

function makeOneConstel()  {
  for (let c = 0; c < constellations.length; c++) {
    let constelA = constellations[c]
    for (let c2 = 0; c2 < constellations.length; c2++) {
      let constelB = constellations[c2]
      if ( c != c2 && doConstellationsOverlap(constelA, constelB)) {
        constellations[c] = constelA.concat(constelB)
        constellations.splice(c2, 1)
        return true
      }
    }
  }
}

console.log("Step 2");

while (makeOneConstel()) {}

console.log("constellations greater than 2: " + constellations.length);
console.log("left over points " + points.length);

console.log("Total Constellations: " + (points.length + constellations.length));




