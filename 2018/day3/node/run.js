const fs = require('fs')

let claimStrings = fs.readFileSync('../data/input.txt').toString().split("\n")
claimStrings.splice(claimStrings.length - 1, 1)

let claims = []
claimStrings.forEach(str => {
  let newClaim = {}
  const atIndex = str.indexOf('@')
  const commaIndex = str.indexOf(',')
  const colonIndex = str.indexOf(':')
  const xIndex = str.indexOf('x')
  newClaim.id = parseInt(str.substring(1, atIndex-1))
  newClaim.x = parseInt(str.substring(atIndex+2, commaIndex))
  newClaim.y = parseInt(str.substring(commaIndex+1, colonIndex))
  newClaim.width = parseInt(str.substring(colonIndex+2, xIndex))
  newClaim.height = parseInt(str.substring(xIndex+1, str.length))
  claims.push(newClaim)
})

let fabricMap = []
for(i = 0; i < 1000; i++) {
    fabricMap[i] = []
    for(j = 0; j < 1000; j++) {
        fabricMap[i][j] = 0
    }
}

claims.forEach(claim => {
  for (let h = claim.y; h < claim.y+claim.height; h++) {
    for (let w = claim.x; w < claim.x+claim.width; w++) {
      fabricMap[w][h]++
    }
  }
})

let overlappingSquares = 0
fabricMap.forEach(row => {
  row.forEach(square => {
    if (square >= 2) overlappingSquares++
  })
})

console.log("Overlapping Squares: " + overlappingSquares)

claims.forEach(claim => {
  let noOverlappingSquares = true
  for (let h = claim.y; h < claim.y+claim.height; h++) {
    for (let w = claim.x; w < claim.x+claim.width; w++) {
      if (fabricMap[w][h] > 1) noOverlappingSquares = false
    }
  }
  if (noOverlappingSquares) console.log("We Found it! Yay! Claim ID: " + claim.id)
})
