const fs = require('fs')
let strings = fs.readFileSync('../data/input.txt').toString().split("\n")
const maxDays = {1: 31, 2:28, 3: 31, 4: 30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31 }
let days = [], guardInfo = {}
for(j = 0; j < 403; j++) { days[j] = [] }
strings.forEach(str => {
  let month = parseInt(str.substring(6, 8)), day = parseInt(str.substring(9, 11)), hour = parseInt(str[13])
  if (hour == 3){ if (maxDays[month] == day) { month++; day = 1 } else { day++ } }
  let newRecord = { dayId: (month * 31) + day, minute: parseInt(str.substring(15, 17)), type: str[19] }
  if (newRecord.type == 'G') {
    const hashIndex = str.indexOf('#'), spaceIdx = str.substring(hashIndex).indexOf(' ')
    newRecord.guardId = parseInt(str.substring(hashIndex + 1, hashIndex + spaceIdx))
  }
  if (hour == 3) days[newRecord.dayId][newRecord.minute] = newRecord
  else days[newRecord.dayId][newRecord.minute + 60] = newRecord
})
days.forEach(day => {
  day.forEach(minute => {
    if (minute.type == 'w' ) {
      for (let i = sleepStart; i < minute.minute; i++){
        if (!guardInfo[guardId]) guardInfo[guardId] = []
        if (!guardInfo[guardId][i]) guardInfo[guardId][i] = 0
        guardInfo[guardId][i]++
      }
    } else minute.type == 'G' ? guardId = minute.guardId : sleepStart = minute.minute
  })
})
let p1GuardTotal = 0, p1Time = null, p2Time = null
Object.keys(guardInfo).forEach(guardId => {
  let sum = 0
  guardInfo[guardId].forEach((minutes, i) => { sum += minutes; if(minutes > p2Time) { p2Time = minutes; p2Minute = i; p2Guard = guardId } })
  if (sum > p1GuardTotal) { p1Guard = guardId; p1GuardTotal = sum }
})
guardInfo[p1Guard].forEach((minute, i) => { if (minute > p1Time) { p1Time = minute; p1Minute = i } })
console.log(`Part 1: ${p1Guard * p1Minute}  Part 2: ${p2Guard * p2Minute}`)
