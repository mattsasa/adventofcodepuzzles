const fs = require('fs')
let polyString = fs.readFileSync('../data/input.txt').toString().split("\n")[0]
let polyStringOriginal = polyString

function isUpperCase(character){ return character == character.toUpperCase() }
function explodeAtIndex(i) { return polyString.slice(0, i - 1) + polyString.substring(i + 1) }
function willExplode(i) { return polyString[i-1].toUpperCase() == polyString[i].toUpperCase() && isUpperCase(polyString[i-1]) != isUpperCase(polyString[i]) }
function startReaction(i) { while (i != 0 && i != polyString.length && willExplode(i)) { polyString = explodeAtIndex(i); i-- } return i }

function removeAllType(type) {
  let shorterPolyString = ""
  for (let i = 0; i < polyStringOriginal.length; i++){
    if (polyStringOriginal[i].toLowerCase() != type) {
      shorterPolyString += polyStringOriginal[i]
    }
  }
  return shorterPolyString
}

for (let letter = 97; letter < 123; letter++){
  let character = String.fromCharCode(letter)
  console.log(character);
  polyString = removeAllType(character)
  for (let i = 1; i < polyString.length; i++){ if (willExplode(i)) { i = startReaction(i) } }
  console.log(polyString.length);
}
