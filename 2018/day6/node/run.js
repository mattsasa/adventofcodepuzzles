const fs = require('fs')
let coordinates = []
let strings = fs.readFileSync('../data/input.txt').toString().split("\n")
strings.forEach(str => { coordinates.push({x: parseInt(str.split(",")[0]), y: parseInt(str.split(",")[1])})})

function calcManhattanDistance(coord1, coord2) { return Math.abs(coord1.x - coord2.x) + Math.abs(coord1.y - coord2.y) }
function findMaxGridSize() {
  let height = 0, width = 0
  coordinates.forEach(coord => {
    if (coord.x > width) { width = coord.x }
    if (coord.y > height) { height = coord.y }
  })
  return { width, height }
}

const { width, height } = findMaxGridSize()

// Part 1
// for (let j = 0; j < coordinates.length; j++){
//   totalPoints = 0
//   thisOneExtendsInfinitely = false
//   for (let h = 0; h <= height; h++) {
//     for (let w = 0; w <= width; w++) {
//       anotherOneIsCloserToThisPoint = false
//       for (let i = 0; i < coordinates.length; i++) {
//         if (i != j && calcManhattanDistance(coordinates[i], { x: w, y: h }) <= calcManhattanDistance(coordinates[j], { x: w, y: h })) {
//           anotherOneIsCloserToThisPoint = true
//         }
//       }
//       if (!anotherOneIsCloserToThisPoint) {
//         totalPoints++
//         if(w == width || w == 0 || h == height || h == 0) { thisOneExtendsInfinitely = true }
//       }
//     }
//   }
//   if (!thisOneExtendsInfinitely) { console.log("Total Points: "+totalPoints) }
// }

function findTotalDistToAll(coord) {
  let sum = 0
  for (i = 0; i < coordinates.length; i++) {
    sum += calcManhattanDistance(coord, coordinates[i])
  }
  return sum
}

let totalInRegion = 0
for (h = 0; h <= height; h++) {
  for (w = 0; w <= width; w++) {
    if (findTotalDistToAll({ x: w, y: h }) < 10000) {
      totalInRegion++
    }
  }
}
console.log("Total In Region: " + totalInRegion);