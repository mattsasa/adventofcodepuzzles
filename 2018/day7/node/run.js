const fs = require('fs')
let instructions = [], preReqs = [], availableSteps = [], order = ""
let strings = fs.readFileSync('../data/input.txt').toString().split("\n")
strings.forEach(str => { instructions.push({ prereq: str[5], step: str[36]}); preReqs.push(str[5]) })
let workers = [-1, -1, -1, -1, -1], second = 0
let workerJobs = [null,null,null,null,null]

function findfirstSteps(){ instructions.forEach(ins => { preReqs = preReqs.filter(e => e !== ins.step) }) }

function fillFirstAvailSteps() { preReqs.forEach(step => { if (!availableSteps.includes(step)) { availableSteps.push(step) } }) }

function otherPreReqsMet(step) {
  noOtherPreReqs = true
  instructions.forEach(ins => { if (ins.step == step) { if (!order.includes(ins.prereq)) { noOtherPreReqs = false } } })
  return noOtherPreReqs
}

function startNewJob(workerIndex) {
    let earliestAlphabet = 'Z'
    availableSteps.forEach(step => { if(step.charCodeAt(0) < earliestAlphabet.charCodeAt(0)) { earliestAlphabet = step } } )
    workerJobs[workerIndex] = earliestAlphabet
    availableSteps = availableSteps.filter(e => e != earliestAlphabet)
    workers[workerIndex] = 60 + earliestAlphabet.charCodeAt(0) - 64
}

function finishJob(step) {
  order += step
  instructions.forEach(ins => { if (ins.prereq == step && otherPreReqsMet(ins.step) && !availableSteps.includes(ins.step)) availableSteps.push(ins.step) })
}

function incrementAllWorkers() {
  workers.forEach((worker, index) => {
    if (worker <= 0) {
      if (workerJobs[index] != null) { finishJob(workerJobs[index]); workerJobs[index] = null }
      if (availableSteps.length != 0) { startNewJob(index) }
    }
    workers[index] += -1
  })
}


findfirstSteps()
fillFirstAvailSteps()

while (order.length < 26) {
  incrementAllWorkers()
  console.log(second + " " + workerJobs[0] + " " + workerJobs[1] + " " + workerJobs[2] + " " + workerJobs[3] + " " + workerJobs[4] + " " + order);
  second++
}

