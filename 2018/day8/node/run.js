const fs = require('fs')
let license = fs.readFileSync('../data/input.txt').toString().split(" ")
function indexSliderRecurse(str, counterIndex) {
  const dataCount = parseInt(str[counterIndex+1]), childCount = parseInt(str[counterIndex])
  if (parseInt(str[counterIndex]) == 0) counterIndex += 2 + dataCount
  else {
    counterIndex += 2
    for (let j = 0; j < childCount; j++) counterIndex = indexSliderRecurse(str, counterIndex)
    counterIndex += dataCount
  }
  return counterIndex
}
function findChildren(str, childCount) {
  let children = [], counterIndex = 0
  while (children.length < childCount) {
    const loopTimes = parseInt(str[counterIndex]), dataCount = parseInt(str[counterIndex+1])
    if (loopTimes == 0 ){
      children.push(str.slice(counterIndex, counterIndex + dataCount + 2))
      counterIndex += dataCount + 2
    } else {
      let oldCounterIndex = counterIndex
      counterIndex += 2
      for (let i = 0; i < loopTimes; i++) counterIndex = indexSliderRecurse(str, counterIndex)
      counterIndex += dataCount
      children.push(str.slice(oldCounterIndex, counterIndex))
    }
  }
  return children
}
function recurseData(str) {
  let rootNode = { value: 0, childCount: parseInt(str[0]), dataCount: parseInt(str[1]), children: [] }
  if (rootNode.childCount != 0) {
    const children = findChildren(str.slice(2, str.length - rootNode.dataCount), rootNode.childCount)
    children.forEach(child => rootNode.children.push(recurseData(child)))
  }
  for(i = str.length - rootNode.dataCount; i < str.length; i++) {
    const meta = parseInt(str[i])
    metaSum += meta
    if (meta - 1 < rootNode.children.length) rootNode.value += rootNode.children[meta - 1].value
    if (rootNode.children.length == 0) rootNode.value += meta
  }
  return rootNode
}
metaSum = 0
const rootNode = recurseData(license)
console.log(`Meta Data Sum: ${metaSum}   Root Node Value: ${rootNode.value}`);


