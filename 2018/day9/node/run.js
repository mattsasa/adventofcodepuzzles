const totalPlayers = 419, lastMarble = 71052*100
let playerScores = new Array(totalPlayers)
for (i = 0; i < playerScores.length; i++) playerScores[i] = 0
let marbleCircle = [0], currentMarble = {}, currentPlayer = 1

function placeMarble(marbleNumber) {
  if ( marbleNumber % 1000 == 0) console.log("Adding Marble #: " + marbleNumber)
  if (marbleCircle.length == 1) {
    marbleCircle.push(marbleNumber)
    currentMarble = { index: 1, number: 1 }
    return
  }
  if (marbleNumber % 23 == 0) {
    playerScores[currentPlayer-1] += marbleNumber
    let targetIndex  = currentMarble.index - 7
    if (targetIndex < 0) targetIndex += marbleCircle.length
    playerScores[currentPlayer-1] += marbleCircle[targetIndex]
    marbleCircle.splice(targetIndex,1)
    currentMarble = { index: targetIndex, number: marbleCircle[targetIndex] }
  }
  else {
    let targetIndex = currentMarble.index + 1
    if (targetIndex >= marbleCircle.length) targetIndex = 0
    marbleCircle.splice(targetIndex + 1, 0, marbleNumber)
    currentMarble = { index: targetIndex + 1, number: marbleNumber }
  }
}

for (let i = 1; i <= lastMarble; i++) {
  placeMarble(i)
  currentPlayer++
  if (currentPlayer > totalPlayers) currentPlayer = 1
}

//console.log("Circle: "+marbleCircle.toString());
//console.log("Scores: "+playerScores.toString());
let highestScore = 0
playerScores.forEach(score => { if(score > highestScore) highestScore = score })
console.log("Highest Score: " + highestScore);