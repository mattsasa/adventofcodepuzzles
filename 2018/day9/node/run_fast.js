const totalPlayers = 419, lastMarble = 71052*100
let playerScores = new Array(totalPlayers)
for (i = 0; i < playerScores.length; i++) playerScores[i] = 0
let marbleCircle = { left: null, value: 0, right: null }, currentMarble = marbleCircle, currentPlayer = 1

function placeMarble(marbleNumber) {
  if (marbleCircle.left == null) {
    let newMarble = { left: currentMarble, value: 1, right: currentMarble }
    currentMarble.left = newMarble
    currentMarble.right = newMarble
    currentMarble = newMarble
    return
  }
  if (marbleNumber % 23 == 0) {
    playerScores[currentPlayer-1] += marbleNumber
    let targetNode = currentMarble.left.left.left.left.left.left.left
    playerScores[currentPlayer-1] += targetNode.value
    let previousNode = targetNode.left
    previousNode.right = targetNode.right
    currentMarble = previousNode.right
  }
  else {
    let leftofNewNode = currentMarble.right
    let rightofNewNode = currentMarble.right.right
    let newNode = { value: marbleNumber }
    leftofNewNode.right = newNode
    rightofNewNode.left = newNode
    newNode.left = leftofNewNode
    newNode.right = rightofNewNode
    currentMarble = newNode
  }
}

for (let i = 1; i <= lastMarble; i++) {
  placeMarble(i)
  currentPlayer++
  if (currentPlayer > totalPlayers) currentPlayer = 1
}
//console.log("Scores: "+playerScores.toString());
let highestScore = 0
playerScores.forEach(score => { if(score > highestScore) highestScore = score })
console.log("Highest Score: " + highestScore);