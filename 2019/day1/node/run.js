const fs = require('fs')

const masses = fs.readFileSync('../data/input.txt').toString().split("\n")

let total = 0
masses.forEach(mass => {
  let smallTotal = 0
  fuelRequirement = Math.floor(mass / 3) - 2
  smallTotal += fuelRequirement
  while (fuelRequirement > 0) {
    newRequirement = Math.floor(fuelRequirement / 3) - 2
    if (newRequirement < 0) newRequirement = 0
    fuelRequirement = newRequirement
    smallTotal += newRequirement
  }
  total += smallTotal
})

console.log(total);



