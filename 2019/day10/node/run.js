const fs = require('fs')

const rows = fs.readFileSync('../data/input.txt').toString().split("\n")

const asteroids = []

rows.forEach((row,i) => {
  rowArray = row.split('')
  rowArray.forEach((item,j) => {
    if (item == '#') asteroids.push({x: j, y: i})
  })
})

let bestAsteroid = {}, bestAmount = 0
for (let i = 0; i < asteroids.length; i++) {
  const uniqueAngles = {}
  for (let j = 0; j < asteroids.length; j++) {
    if (i == j) continue
    let angle = getAngle(asteroids[i], asteroids[j])
    if (angle < -Math.PI) angle += Math.PI*2
    if (angle > Math.PI) angle -= Math.PI*2
    angle += Math.PI
    angle = Math.PI*2 - angle

    if (asteroids[i].y == asteroids[j].y) {
      angle = asteroids[i].x < asteroids[j].x ? Math.PI*(3/2) : Math.PI/2
    }
    if (asteroids[i].x == asteroids[j].x) {
      angle = asteroids[i].y < asteroids[j].y ? 0.0 : Math.PI
    }
    uniqueAngles[angle] = 1
  }
  if (Object.keys(uniqueAngles).length > bestAmount) {
    bestAmount = Object.keys(uniqueAngles).length
    bestAsteroid = asteroids[i]
  }
}

console.log("Part 1 -- Best Location and how many asteroids can be detected: ");
console.log(bestAsteroid);
console.log(bestAmount);
console.log();


const uniqueAngles = {}
for (let i = 0; i < asteroids.length; i++) {
  if (asteroids[i].x == bestAsteroid.x && asteroids[i].y == bestAsteroid.y) continue
  let angle = getAngle(asteroids[i], bestAsteroid)
  if (angle < -Math.PI) angle += Math.PI*2
  if (angle > Math.PI) angle -= Math.PI*2
  angle += Math.PI
  angle = Math.PI*2 - angle
  dist = Math.sqrt(  Math.pow(bestAsteroid.x - asteroids[i].x,2) + Math.pow(bestAsteroid.y - asteroids[i].y,2)  )

  if (asteroids[i].y == bestAsteroid.y) {
    angle = asteroids[i].x < bestAsteroid.x ? Math.PI*(3/2) : Math.PI/2
  }
  if (asteroids[i].x == bestAsteroid.x) {
    angle = asteroids[i].y < bestAsteroid.y ? 0.0 : Math.PI
  }

  if (!uniqueAngles[angle]) uniqueAngles[angle] = []
  uniqueAngles[angle].push({ asteroid: asteroids[i], dist: dist })
}

let arrayMap = Object.keys(uniqueAngles).sort((a,b) => { return parseFloat(a) - parseFloat(b) })

let count = 0
arrayMap.forEach((key, i) => {
  currentAngleList = uniqueAngles[key]
  closest = findClosest(currentAngleList)
  //currentAngleList = currentAngleList.filter(item => { return item.dist != closest.dist })
  count++
  if (count == 200 ) {
    console.log("Part 2 -- 200th Asteroid to be destroyed: ");
    console.log(closest);
  }
})


function findClosest(list) {
  let smallestDistance = 100000, closest = {}
  list.forEach(item => {
    if (item.dist < smallestDistance) {
      smallestDistance = item.dist
      closest = item
    }
  })
  return closest
}



function getAngle(p1, p2) {

  const diff_lon = p1.y - p2.y
  const diff_lat = p1.x - p2.x

  let angle = Math.atan(diff_lat / diff_lon)

  if (diff_lat >= 0 && diff_lon >= 0); // Q1
  else if (diff_lat >= 0 && diff_lon <= 0) { // Q2
    angle = Math.PI + angle
  } else if (diff_lat <= 0 && diff_lon <= 0) { // Q3
    angle = -1 * Math.PI + angle
  } else if (diff_lat <= 0 && diff_lon >= 0); // Q4

  return angle
}


