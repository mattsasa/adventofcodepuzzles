const memory = [ 3,8,1005,8,318,1106,0,11,0,0,0,104,1,104,0,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,29,1,107,12,10,2,1003,8,10,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,1002,8,1,59,1,108,18,10,2,6,7,10,2,1006,3,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,1002,8,1,93,1,1102,11,10,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,101,0,8,118,2,1102,10,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,145,1006,0,17,1006,0,67,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,173,2,1109,4,10,1006,0,20,3,8,102,-1,8,10,1001,10,1,10,4,10,108,0,8,10,4,10,102,1,8,201,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,4,10,1002,8,1,224,1006,0,6,1,1008,17,10,2,101,5,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,256,2,1107,7,10,1,2,4,10,2,2,12,10,1006,0,82,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,1002,8,1,294,2,1107,2,10,101,1,9,9,1007,9,988,10,1005,10,15,99,109,640,104,0,104,1,21102,1,837548352256,1,21102,335,1,0,1105,1,439,21102,1,47677543180,1,21102,346,1,0,1106,0,439,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21102,1,235190374592,1,21101,393,0,0,1105,1,439,21102,3451060455,1,1,21102,404,1,0,1105,1,439,3,10,104,0,104,0,3,10,104,0,104,0,21102,837896909668,1,1,21102,1,427,0,1105,1,439,21102,1,709580555020,1,21102,438,1,0,1105,1,439,99,109,2,21201,-1,0,1,21102,1,40,2,21102,1,470,3,21102,460,1,0,1106,0,503,109,-2,2105,1,0,0,1,0,0,1,109,2,3,10,204,-1,1001,465,466,481,4,0,1001,465,1,465,108,4,465,10,1006,10,497,1101,0,0,465,109,-2,2105,1,0,0,109,4,1201,-1,0,502,1207,-3,0,10,1006,10,520,21101,0,0,-3,21202,-3,1,1,22101,0,-2,2,21101,1,0,3,21101,0,539,0,1106,0,544,109,-4,2105,1,0,109,5,1207,-3,1,10,1006,10,567,2207,-4,-2,10,1006,10,567,21202,-4,1,-4,1105,1,635,22101,0,-4,1,21201,-3,-1,2,21202,-2,2,3,21101,0,586,0,1105,1,544,22102,1,1,-4,21102,1,1,-1,2207,-4,-2,10,1006,10,605,21102,1,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,627,21202,-1,1,1,21101,627,0,0,105,1,502,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2105,1,0 ]

class Machine {
  constructor() {
    this.memory = [...memory]

    for (let i = 0; i < 10000; i++){
      this.memory.push(0)
    }

    this.pc = 0
    this.relativeBase = 0
    this.output = []
    this.halted = false

    this.input = []
  }

  runProgram(newInput) {
    this.input.push(newInput)
    while (!this.halted && this.output.length < 2) { this.processOpcode(this.memory) }
    if (this.halted) return -1
    return this.output.splice(-2,2)
  }

  getArg(arg, mode) {
    switch (mode) {
      case 0: return this.memory[this.memory[this.pc + arg]]
      case 1: return this.memory[this.pc + arg]
      case 2: return this.memory[this.memory[this.pc + arg] + this.relativeBase]
    }
  }

  setMemory(arg, mode, result) {
    switch (mode) {
      case 0: this.memory[this.memory[this.pc + arg]] = result; return
      case 2: this.memory[this.memory[this.pc + arg] + this.relativeBase] = result; return
    }
  }

  getParamaterMode(arg){
    let str = memory[this.pc].toString()
    return str[str.length - 2 - arg] ? parseInt(str[str.length - 2 - arg]) : 0;
  }

  getOpcode() {
    let str = memory[this.pc].toString()
    let opcode = parseInt(str[str.length - 1])
    if (str.length > 1) opcode += 10  * parseInt(str[str.length - 2])
    return opcode
  }

  processOpcode(memory) {
    const opcode = this.getOpcode()
    const arg1mode = this.getParamaterMode(1)
    const arg2mode = this.getParamaterMode(2)
    const arg3mode = this.getParamaterMode(3)

    switch (opcode) {
      case 1:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) +  this.getArg(2,arg2mode));
        this.pc += 4; break
      case 2:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) *  this.getArg(2,arg2mode));
        this.pc += 4; break
      case 7:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) <  this.getArg(2,arg2mode) ? 1 : 0);
        this.pc += 4; break
      case 8:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) == this.getArg(2,arg2mode) ? 1 : 0);
        this.pc += 4; break
      case 9:
        this.relativeBase += this.getArg(1, arg1mode);
        this.pc += 2; break
      case 5:
        if (this.getArg(1, arg1mode) != 0)  { this.pc = this.getArg(2, arg2mode); break }
        this.pc += 3; break
      case 6:
        if (this.getArg(1, arg1mode) == 0)  { this.pc = this.getArg(2, arg2mode); break }
        this.pc += 3; break
      case 3:
        this.setMemory(1, arg1mode, this.input.shift());
        this.pc += 2; break
      case 4:
        this.output.push(this.getArg(1, arg1mode));
        this.pc += 2; break

      case 99: this.halted = true; break

      default: console.log("unimplemented opcode: " + opcode); process.exit()
    }

    return true
  }
}

let currentLocation = {x: 400, y: 400}, currentDirection = 'Up'

function printImage() {
  for (let y = 350; y < 450; y++){
    for (let x = 350; x < 450; x++) {
      if (x == 400 && y == 400) { process.stdout.write('$'); continue }
      if (x == currentLocation.x && y == currentLocation.y) { process.stdout.write('^'); continue }
      grid[y][x] == 0 ? process.stdout.write('.') : process.stdout.write('#')
    }
    console.log();
  }
}

const machine = new Machine()

const grid = []
for (let i = 0; i < 800; i++){
  let row = []
  for (let j = 0; j < 800; j++){
    row.push(0)
  }
  grid.push(row)
}

const paintedLocations = {}

let output = []
grid[currentLocation.y][currentLocation.x] = 1
while (true) {
  output = machine.runProgram(grid[currentLocation.y][currentLocation.x])
  if (output == -1) { break }
  ///painting current Location
  if (!paintedLocations[currentLocation.y]) paintedLocations[currentLocation.y] = {}
  if (!paintedLocations[currentLocation.y][currentLocation.x]) paintedLocations[currentLocation.y][currentLocation.x] = 1
  grid[currentLocation.y][currentLocation.x] = output[0]


  if (currentDirection == 'Up') {
    currentDirection = output[1] == 0 ? 'Left' : 'Right'
  } else if (currentDirection == 'Left') {
    currentDirection = output[1] == 0 ? 'Down' : 'Up'
  } else if (currentDirection == 'Right') {
    currentDirection = output[1] == 0 ? 'Up' : 'Down'
  } else if (currentDirection == 'Down') {
    currentDirection = output[1] == 0 ? 'Right' :  'Left'
  } else { console.log('direcction glitch'); process.exit(0) }

  if (currentDirection == 'Up') {
    currentLocation.y--
  } else if (currentDirection == 'Left') {
    currentLocation.x--
  } else if (currentDirection == 'Right') {
    currentLocation.x++
  } else if (currentDirection == 'Down') {
    currentLocation.y++
  } else { console.log('direcction glitch'); process.exit(0) }
}

printImage()


const count = Object.keys(paintedLocations).reduce((accumulator, key) => {
  return accumulator + Object.keys(paintedLocations[key]).length
},0)

console.log(count);

console.log('finished');


