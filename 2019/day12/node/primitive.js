const xValues = [-1,2,4,3]
const yValues = [0,-10,-8,5]
const zValues = [2,-7,8,-1]

const vxValues = [0,0,0,0]
const vyValues = [0,0,0,0]
const vzValues = [0,0,0,0]

function updateGravity(index1, index2) {

  if (xValues[index1] > xValues[index2]) { vxValues[index1] = vxValues[index1] - 1; vxValues[index2] = vxValues[index2] + 1; }
  else { if (xValues[index1] < xValues[index2]) { vxValues[index1] = vxValues[index1] + 1; vxValues[index2] = vxValues[index2] - 1; } }

  if (yValues[index1] > yValues[index2]) { vyValues[index1] = vyValues[index1] - 1; vyValues[index2] = vyValues[index2] + 1; }
  else { if (yValues[index1] < yValues[index2]) { vyValues[index1] = vyValues[index1] + 1; vyValues[index2] = vyValues[index2] - 1; } }

  if (zValues[index1] > zValues[index2]) { vzValues[index1] = vzValues[index1] - 1; vzValues[index2] = vzValues[index2] + 1; }
  else { if (zValues[index1] < zValues[index2]) { vzValues[index1] = vzValues[index1] + 1; vzValues[index2] = vzValues[index2] - 1; } }
}

function updatePosition(index) {
  xValues[index] += vxValues[index]
  yValues[index] += vyValues[index]
  zValues[index] += vzValues[index]
}

function moonTotalEnergy(index) { return potentialEnergy(index) * kineticEnergy(index) }

function potentialEnergy(index) { return Math.abs(xValues[index]) + Math.abs(yValues[index]) + Math.abs(zValues[index]) }
function kineticEnergy(index) { return Math.abs(vxValues[index]) + Math.abs(vyValues[index]) + Math.abs(vzValues[index]) }

for (let t = 0; t < 10; t++) {
  updateGravity(0, 1)
  updateGravity(0, 2)
  updateGravity(0, 3)
  updateGravity(1, 2)
  updateGravity(1, 3)
  updateGravity(2, 3)

  updatePosition(0)
  updatePosition(1)
  updatePosition(2)
  updatePosition(3)
}

console.log(xValues);

let totalSystemEnergy = 0
for (let index = 0; index < 4; index++) {
  console.log(moonTotalEnergy(index))
  totalSystemEnergy += moonTotalEnergy(index)
}
console.log(totalSystemEnergy);