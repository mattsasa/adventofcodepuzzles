const moons = [
  { x: -6, y: -5, z: -8, vx: 0, vy: 0, vz: 0 },
  { x: 0, y: -3, z: -13, vx: 0, vy: 0, vz: 0 },
  { x: -15, y: 10, z: -11, vx: 0, vy: 0, vz: 0 },
  { x: -3, y: -8, z: 3, vx: 0, vy: 0, vz: 0 }
]


// const moons = [
//     { x: -8, y: -10, z: 0, vx: 0, vy: 0, vz: 0 },
//     { x: 5, y: 5, z: 10, vx: 0, vy: 0, vz: 0 },
//     { x: 2, y: -7, z: 3, vx: 0, vy: 0, vz: 0 },
//     { x: 9, y: -8, z: -3, vx: 0, vy: 0, vz: 0 }
// ]

const initState = JSON.parse(JSON.stringify(moons))


function matchesInitState() {
  return JSON.stringify(initState) == JSON.stringify(moons)
}


function updateGravity(moon1, moon2) {
  if (moon1.x > moon2.x) { moon1.vx--; moon2.vx++ } else if (moon1.x < moon2.x) { moon1.vx++; moon2.vx-- }
  if (moon1.y > moon2.y) { moon1.vy--; moon2.vy++ } else if (moon1.y < moon2.y) { moon1.vy++; moon2.vy-- }
  if (moon1.z > moon2.z) { moon1.vz--; moon2.vz++ } else if (moon1.z < moon2.z) { moon1.vz++; moon2.vz-- }
}

function updatePosition(moon) {
  moon.x += moon.vx
  moon.y += moon.vy
  moon.z += moon.vz
}

function moonTotalEnergy(moon) { return potentialEnergy(moon) * kineticEnergy(moon) }

function potentialEnergy(moon) { return Math.abs(moon.x) + Math.abs(moon.y) + Math.abs(moon.z) }
function kineticEnergy(moon) { return Math.abs(moon.vx) + Math.abs(moon.vy) + Math.abs(moon.vz) }

let prevT = 0, prevGap = 0

for (let t = 0; t < 1000000; t++){
  updateGravity(moons[0], moons[1])
  updateGravity(moons[0], moons[2])
  updateGravity(moons[0], moons[3])
  updateGravity(moons[1], moons[2])
  updateGravity(moons[1], moons[3])
  updateGravity(moons[2], moons[3])

  updatePosition(moons[0])
  updatePosition(moons[1])
  updatePosition(moons[2])
  updatePosition(moons[3])


  // let boolX = false
  // let boolY = false

  //48072
                  ///135024
  // //92473 + t*(49860+85164)
  // if ((t - 92473) % (49860+85164) == 0) {
  //   //Moon 0 Position X
  //   boolX == true
  // }
  //
  //
  //              ///231614
  // //88451 + t*(176865+54749)
  // if ((t - 88451) % (176865+54749) == 0) {
  //   //Moon 0 Position Y
  //   boolY == true
  // }

  ///48118


  if (moons[1].x == initState[1].x) {
    // console.log('match');
    // console.log(t - prevT);
    let gap = t - prevT
    // console.log(gap);
    if (gap == 44) {
      // if (t - prevGap == 49860) {
      //   console.log(t - prevGap);
      // }
      console.log("gap distance: " + (t - prevGap).toString() + "  current T: " + t);
      //console.log(t);
      prevGap = t
    }
    prevT = t
  }

  // if (moons[0].x == initState[0].x + t) {
  //   console.log(t);
  // }

  // console.log(moons[0].x);
}
// 6, 28, 44,
// 9, 28, 44,
// 18, 28, 44,
// 6, 28, 44,

// 2028, 5898, 4702
// 2028, 5898, 4702
// 2028
// 2028, 5898


console.log(moons);

let totalSystemEnergy = 0
moons.forEach(moon => {
  console.log(moonTotalEnergy(moon))
  totalSystemEnergy += moonTotalEnergy(moon)
})
console.log(totalSystemEnergy);