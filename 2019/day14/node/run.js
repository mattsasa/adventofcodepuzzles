const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")

const reactions = []

lines.forEach(line => {
  const reaction = {}
  const input = line.split("=>")[0].split(',')
  reaction.input = []
  input.forEach(item => {
    items = item.split(' ')
    const newInput = {}
    items.forEach(value => {
      if (isNaN(parseInt(value))){
        if (value != '') newInput.name = value
      } else {
        newInput.quanity = parseInt(value)
      }
    })
    reaction.input.push(newInput)
  })
  const output = line.split("=>")[1].split(' ')
  reaction.output = { quanity: parseInt(output[1]), name: output[2] }
  reactions.push(reaction)
})

let surplusMap = {}

function getMinOreRequirements(requirement) {
  let minOre = 0

  if (!surplusMap[requirement.name]) surplusMap[requirement.name] = 0
  if (surplusMap[requirement.name] >= requirement.quanity) { surplusMap[requirement.name] -= requirement.quanity; return 0 }

  const reaction = reactions.find(reaction => reaction.output.name == requirement.name)
  const numberOfOperations = Math.ceil((requirement.quanity - surplusMap[requirement.name]) / reaction.output.quanity)

  surplusMap[requirement.name] += (numberOfOperations * reaction.output.quanity) - requirement.quanity

  reaction.input.forEach(input => {
      if (input.name == 'ORE') { minOre += numberOfOperations * input.quanity }
      else { minOre += getMinOreRequirements({ name: input.name, quanity: input.quanity * numberOfOperations }) }
  })
  return minOre
}

const answer = getMinOreRequirements({ name: 'FUEL', quanity: 4200533 })

console.log(`Min Ore Needed: ${answer}`);

