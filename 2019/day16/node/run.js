const signal = '59764635797473718052486376718142408346357676818478503599633670059885748195966091103097769012608550645686932996546030476521264521211192035231303791868456877717957482002303790897587593845163033589025995509264282936119874431944634114034231860653524971772670684133884675724918425789232716494769777580613065860450960426147822968107966020797566015799032373298777368974345143861776639554900206816815180398947497976797052359051851907518938864559670396616664893641990595511306542705720282494028966984911349389079744726360038030937356245125498836945495984280140199805250151145858084911362487953389949062108285035318964376799823425466027816115616249496434133896'
let signalStrArray = []
for (let i = 0; i < 10000; i++) {
  signalStrArray.push(...signal.toString().split(''))
}

let signalIntArray = signalStrArray.map(item => { return parseInt(item) })
let offset = parseInt(signalStrArray.slice(0,7).join(''))

signalIntArray = signalIntArray.slice(offset)

const basePatten = [0, 1, 0, -1]

function createPattern(n) {
  let pattern = []
  let count = 1, baseIndex = 0
  while (pattern.length < signalIntArray.length) {
    if (count == n) { baseIndex++; count = 0 }
    if (baseIndex > basePatten.length - 1) baseIndex = 0
    pattern.push(basePatten[baseIndex])
    count++
  }
  return pattern
}

function dotProduct(array1, array2) {
  let sum = 0
  for (let i = 0; i < array1.length; i++) {
    sum += array1[i] * array2[i]
  }
  let sumSplit = sum.toString().split('')
  return parseInt(sumSplit[sumSplit.length - 1])
}

function doPhase() {
  let newArray = []
  for (let i = 0; i < signalIntArray.length; i++) {
    newArray.push(dotProduct(signalIntArray, createPattern(i+1)))
  }
  return newArray
}

const resultState = new Array(signalIntArray.length);

function doPhasePart2() {
  let count = 0
  for (let i = signalIntArray.length - 1; i >= 0; i--) {
      count = (count + signalIntArray[i]) % 10;
      resultState[i] = count;
  }
  signalIntArray = resultState.slice()
}

for (let i = 0; i < 100; i++) {
  // signalIntArray = doPhase()
  doPhasePart2()
}


console.log(JSON.stringify(signalIntArray.slice(0, 8)))

