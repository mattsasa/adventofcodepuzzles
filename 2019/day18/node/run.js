const fs = require('fs')
const clone = require('clone');
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
const grid = [], gridHeight = lines.length, gridWidth = lines[0].length
lines.forEach(line => { grid.push([...line]) })

function printGrid(grid) {
  for (let y = 0; y < gridHeight; y++){
    for (let x = 0; x < gridWidth; x++) {
      if (grid[y][x] == '@') startLocation = { x, y, distance: 0, char: '@' }
      process.stdout.write(grid[y][x].toString())
    }
    console.log();
  }
}

function findStartLocation(xmin, xmax, ymin, ymax) {
  let startLocation
  for (let y = ymin; y < ymax; y++){
    for (let x = xmin; x < xmax; x++) {
      if (grid[y][x] == '@') startLocation = { x, y, distance: 0, char: '@' }
      // process.stdout.write(grid[y][x].toString())
    }
    // console.log();
  }
  // console.log();
  return startLocation
}

function copyGrid(grid) {
  const copy = []
  for (let y = 0; y < gridHeight; y++){
    let row = []
    for (let x = 0; x < gridWidth; x++) {
      row.push(grid[y][x])
    }
    copy.push(row)
  }
  return copy
}

function findExtraEdges(startLocation) {
  const c_grid = copyGrid(grid)
  c_grid[startLocation.y][startLocation.x] = '.'
  let ends = [{...startLocation, distance: 0 }], finalEnds = []

  while (ends.length != 0) {
    let newEnds = []
    ends.forEach(end => {

      if (c_grid[end.y+1][end.x] != '#') {
        const newEnd = {...end, distance: end.distance + 1, y: end.y + 1 }
        if (c_grid[newEnd.y][newEnd.x] == '.') newEnds.push(newEnd)
        else finalEnds.push({ distance: newEnd.distance, targetchar: c_grid[newEnd.y][newEnd.x]})
        c_grid[newEnd.y][newEnd.x] = '#'
      }
      if (c_grid[end.y-1][end.x] != '#') {
        const newEnd = {...end, distance: end.distance + 1, y: end.y - 1 }
        if (c_grid[newEnd.y][newEnd.x] == '.') newEnds.push(newEnd)
        else finalEnds.push({ distance: newEnd.distance, targetchar: c_grid[newEnd.y][newEnd.x]})
        c_grid[newEnd.y][newEnd.x] = '#'
      }
      if (c_grid[end.y][end.x+1] != '#') {
        const newEnd = {...end, distance: end.distance + 1, x: end.x + 1 }
        if (c_grid[newEnd.y][newEnd.x] == '.') newEnds.push(newEnd)
        else finalEnds.push({ distance: newEnd.distance, targetchar: c_grid[newEnd.y][newEnd.x]})
        c_grid[newEnd.y][newEnd.x] = '#'
      }
      if (c_grid[end.y][end.x-1] != '#') {
        const newEnd = {...end, distance: end.distance + 1, x: end.x - 1 }
        if (c_grid[newEnd.y][newEnd.x] == '.') newEnds.push(newEnd)
        else finalEnds.push({ distance: newEnd.distance, targetchar: c_grid[newEnd.y][newEnd.x]})
        c_grid[newEnd.y][newEnd.x] = '#'
      }
    })
    ends = newEnds
  }
  return finalEnds
}

function findCharLocationOnGrid(char, xmin, xmax, ymin, ymax) {
  for (let y = ymin; y < ymax; y++){
    for (let x = xmin; x < xmax; x++) {
      if (grid[y][x] == char) return { x, y }
    }
  }
}

function checkIfKeyOrDoor(grid, end, finalEnds, newEnds) {
  if (grid[end.y][end.x] != '.') { /// should be key or door
    finalEnds.push({ distance: end.distance, child: grid[end.y][end.x], parent: end.char })
    newEnds.push({ x: end.x, y: end.y, distance: 0, char: grid[end.y][end.x] })
    grid[end.y][end.x] = '.'
  } else { newEnds.push(end) }
  grid[end.y][end.x] = '#'
}

function findPathsToKeysandDoors(startLocation) {
  const c_grid = copyGrid(grid)
  c_grid[startLocation.y][startLocation.x] = '.'
  let ends = [startLocation], finalEnds = []

  while (ends.length != 0) {

    let newEnds = []
    let result
    ends.forEach(end => {

      if (c_grid[end.y+1][end.x] != '#') {
        const newEnd = {...end, distance: end.distance + 1, y: end.y + 1}
        checkIfKeyOrDoor(c_grid, newEnd, finalEnds, newEnds)
      }
      if (c_grid[end.y-1][end.x] != '#') {
        const newEnd = {...end, distance: end.distance + 1, y: end.y - 1}
        checkIfKeyOrDoor(c_grid, newEnd, finalEnds, newEnds)
      }
      if (c_grid[end.y][end.x+1] != '#') {
        const newEnd = {...end, distance: end.distance + 1, x: end.x + 1}
        checkIfKeyOrDoor(c_grid, newEnd, finalEnds, newEnds)
      }
      if (c_grid[end.y][end.x-1] != '#') {
        const newEnd = {...end, distance: end.distance + 1, x: end.x - 1}
        checkIfKeyOrDoor(c_grid, newEnd, finalEnds, newEnds)
      }
    })

    ends = newEnds
  }
  return finalEnds
}

function buildGraph(edges, currentChar, omnicientMap) {
  const graph = { char: currentChar, edges: []}
  edges.forEach(edge => {
    if (edge.parent != currentChar) return
    const node = buildGraph(edges, edge.child, omnicientMap)
    node.edges.push({ distance: edge.distance, target: graph })
    graph.edges.push({ distance: edge.distance, target: node })
  })
  omnicientMap[currentChar] = graph
  if (currentChar.toUpperCase() != currentChar) totalKeys++
  return graph
}

function removeNodeByChar(char, root) {
  root.ignoreKeys.push(char)
  root.graphs.forEach((graph, i) => {
    removeNode(moveToLocation(graph, char), graph)
    removeNode(moveToLocation(graph, char.toUpperCase()), graph)
  })
}

function removeNode(node, graph) {

  if (node == null) return
  graph.edges.forEach(edge => {
    if (edge.target.char == node.char) {
      ///remove itself from the node
      edge.target.edges = edge.target.edges.filter(edge => edge.target.char != node.char)
      /// add all other nodes to the node
      node.edges.forEach(otherEdge => {
        const newDistance = edge.distance + otherEdge.distance
        graph.edges.push({ distance: newDistance, target: otherEdge.target })
      })
    }
  })
  node.edges.forEach(edge => {
    ///remove itself from the node
    edge.target.edges = edge.target.edges.filter(edge => edge.target.char != node.char)
    /// add all other nodes to the node
    node.edges.forEach(otherEdge => {
      if (otherEdge == edge) return
      if (edge.target.edges.reduce((bool, edge) => { return bool || edge.target.char == otherEdge.target.char }, false)) return
      const newDistance = edge.distance + otherEdge.distance
      edge.target.edges.push({ distance: newDistance, target: otherEdge.target })
    })
  })

}


function getPickedKeys(ignoreKeys, newchar) {
  const keys = ignoreKeys.filter(key => key != key.toUpperCase())
  if (newchar) keys.push(newchar)
  return keys.sort().toString()
}

function moveToLocation(graph, targetChar) {
  let ends = [graph]
  let visitedLocations = []

  while (ends.length != 0) {

    let newEnds = []
    let result
    ends.forEach(end => {
      if (end.char == targetChar) result = end
      visitedLocations.push(end.char)
      end.edges.forEach(edge => {
        if (!visitedLocations.includes(edge.target.char)) newEnds.push(edge.target)
      })
    })
    if (result) return result
    ends = newEnds
  }
  return null
}

function createGraphFromGridSection(xmin, xmax, ymin, ymax) {
  const omnicientMap = {}
  const startLocation = findStartLocation(xmin, xmax, ymin, ymax)
  const graph = buildGraph(findPathsToKeysandDoors(startLocation), '@', omnicientMap)
  Object.entries(omnicientMap).forEach(([char, node]) => {
    const location = findCharLocationOnGrid(char, xmin, xmax, ymin, ymax)
    const newEdges = findExtraEdges(location)
    newEdges.forEach(newEdge => {
      if (!node.edges.reduce((bool, edge) => { return bool || edge.target.char == newEdge.targetchar }, false)) {
        node.edges.push({ distance: newEdge.distance, target: omnicientMap[newEdge.targetchar] })
      }
    })
  })
  return graph
}

function getRobotPositionsStr(graphs, index, newChar) {
  const positions = graphs.map(graph => graph.char)
  if (newChar) positions[index] = newChar
  return positions.join("")
}

function recurseKeyFinding(root) {
  const theGraph = root.graphs[root.lastRobotMovedID]
  if (theGraph.char == '@') removeNode(theGraph, theGraph)
  else removeNodeByChar(theGraph.char, root)
  if (totalKeys == root.ignoreKeys.length) return 0
  let shortestDistance = 1000000
  root.graphs.forEach((graph,i) => {
    graph.edges.forEach(edge => {
      if (edge.target.char == edge.target.char.toUpperCase()) return
      const tempChar = edge.target.char
      const remainingKeys = getPickedKeys(root.ignoreKeys, tempChar)
      const robotPositions = getRobotPositionsStr(root.graphs, i, tempChar)
      let finalDistance
      if (solutionsCache[robotPositions] && solutionsCache[robotPositions][remainingKeys]) {
        finalDistance = solutionsCache[robotPositions][remainingKeys]
      } else {
        const newGraphsClone = clone(root.graphs)
        newGraphsClone[i] = clone(edge.target)
        finalDistance = recurseKeyFinding({ graphs: newGraphsClone, ignoreKeys: clone(root.ignoreKeys), lastRobotMovedID: i })
      }
      finalDistance += edge.distance
      if (finalDistance < shortestDistance) shortestDistance = finalDistance
    })
  })

  const robotPositions = getRobotPositionsStr(root.graphs)
  if (!solutionsCache[robotPositions]) solutionsCache[robotPositions] = {}
  solutionsCache[robotPositions][getPickedKeys(root.ignoreKeys)] = shortestDistance
  return shortestDistance
}

const solutionsCache = {}
let totalKeys = 0

const root = {
  ignoreKeys: [],
  lastRobotMovedID: 0,
  graphs: [
    createGraphFromGridSection(0, gridWidth/2, 0, gridHeight/2),
    createGraphFromGridSection(Math.floor(gridWidth/2), gridWidth, 0, gridHeight/2),
    createGraphFromGridSection(0, gridWidth/2, Math.floor(gridHeight/2), gridHeight),
    createGraphFromGridSection(Math.floor(gridWidth/2), gridWidth, Math.floor(gridHeight/2), gridHeight)
  ]
}

console.log("Final Answer: " + recurseKeyFinding(root))

