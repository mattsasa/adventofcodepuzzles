const memory = [ 109,424,203,1,21101,11,0,0,1106,0,282,21102,1,18,0,1105,1,259,1202,1,1,221,203,1,21101,0,31,0,1105,1,282,21102,38,1,0,1106,0,259,21002,23,1,2,21202,1,1,3,21102,1,1,1,21102,57,1,0,1106,0,303,1202,1,1,222,20102,1,221,3,20102,1,221,2,21102,259,1,1,21102,80,1,0,1105,1,225,21101,0,145,2,21102,91,1,0,1105,1,303,2101,0,1,223,20101,0,222,4,21102,259,1,3,21101,225,0,2,21102,1,225,1,21102,1,118,0,1105,1,225,20101,0,222,3,21101,0,197,2,21101,133,0,0,1106,0,303,21202,1,-1,1,22001,223,1,1,21101,0,148,0,1105,1,259,1202,1,1,223,21001,221,0,4,21001,222,0,3,21102,1,19,2,1001,132,-2,224,1002,224,2,224,1001,224,3,224,1002,132,-1,132,1,224,132,224,21001,224,1,1,21102,195,1,0,105,1,109,20207,1,223,2,21002,23,1,1,21102,-1,1,3,21101,0,214,0,1105,1,303,22101,1,1,1,204,1,99,0,0,0,0,109,5,1201,-4,0,249,22101,0,-3,1,22101,0,-2,2,21202,-1,1,3,21102,250,1,0,1106,0,225,22101,0,1,-4,109,-5,2105,1,0,109,3,22107,0,-2,-1,21202,-1,2,-1,21201,-1,-1,-1,22202,-1,-2,-2,109,-3,2106,0,0,109,3,21207,-2,0,-1,1206,-1,294,104,0,99,22102,1,-2,-2,109,-3,2105,1,0,109,5,22207,-3,-4,-1,1206,-1,346,22201,-4,-3,-4,21202,-3,-1,-1,22201,-4,-1,2,21202,2,-1,-1,22201,-4,-1,1,21201,-2,0,3,21101,343,0,0,1105,1,303,1105,1,415,22207,-2,-3,-1,1206,-1,387,22201,-3,-2,-3,21202,-2,-1,-1,22201,-3,-1,3,21202,3,-1,-1,22201,-3,-1,2,22101,0,-4,1,21102,384,1,0,1106,0,303,1106,0,415,21202,-4,-1,-4,22201,-4,-3,-4,22202,-3,-2,-2,22202,-2,-4,-4,22202,-3,-2,-3,21202,-4,-1,-2,22201,-3,-2,1,22102,1,1,-4,109,-5,2105,1,0 ]

class Machine {
  constructor() {
    this.memory = [...memory]

    this.pc = 0
    this.relativeBase = 0
    this.output = []
    this.input = []
    this.halted = false
  }

  runProgram(newInput) {
    this.halted = false
    this.pc = 0
    this.memory = [...memory]
    this.input.push(...newInput)
    while (!this.halted) { this.processOpcode(this.memory) }
    return this.output.shift()
  }

  getArg(arg, mode) {
    switch (mode) {
      case 0: return this.memory[this.memory[this.pc + arg]] ? this.memory[this.memory[this.pc + arg]] : 0
      case 1: return this.memory[this.pc + arg] ? this.memory[this.pc + arg] : 0
      case 2: return this.memory[this.memory[this.pc + arg] + this.relativeBase] ? this.memory[this.memory[this.pc + arg] + this.relativeBase] : 0
    }
  }

  setMemory(arg, mode, result) {
    switch (mode) {
      case 0: this.memory[this.memory[this.pc + arg]] = result; return
      case 2: this.memory[this.memory[this.pc + arg] + this.relativeBase] = result; return
    }
  }

  getParamaterMode(arg){
    let str = memory[this.pc].toString()
    return str[str.length - 2 - arg] ? parseInt(str[str.length - 2 - arg]) : 0;
  }

  getOpcode() {
    let str = memory[this.pc].toString()
    let opcode = parseInt(str[str.length - 1])
    if (str.length > 1) opcode += 10  * parseInt(str[str.length - 2])
    return opcode
  }

  processOpcode(memory) {
    const opcode = this.getOpcode()
    const arg1mode = this.getParamaterMode(1)
    const arg2mode = this.getParamaterMode(2)
    const arg3mode = this.getParamaterMode(3)

    switch (opcode) {
      case 1:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) +  this.getArg(2,arg2mode));
        this.pc += 4; break
      case 2:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) *  this.getArg(2,arg2mode));
        this.pc += 4; break
      case 7:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) <  this.getArg(2,arg2mode) ? 1 : 0);
        this.pc += 4; break
      case 8:
        this.setMemory(3,arg3mode, this.getArg(1,arg1mode) == this.getArg(2,arg2mode) ? 1 : 0);
        this.pc += 4; break
      case 9:
        this.relativeBase += this.getArg(1, arg1mode);
        this.pc += 2; break
      case 5:
        if (this.getArg(1, arg1mode) != 0)  { this.pc = this.getArg(2, arg2mode); break }
        this.pc += 3; break
      case 6:
        if (this.getArg(1, arg1mode) == 0)  { this.pc = this.getArg(2, arg2mode); break }
        this.pc += 3; break
      case 3:
        this.setMemory(1, arg1mode, this.input.shift());
        this.pc += 2; break
      case 4:
        this.output.push(this.getArg(1, arg1mode))
        this.pc += 2; break

      case 99: this.halted = true; break

      default: console.log("unimplemented opcode: " + opcode); process.exit()
    }

    return true
  }
}

const machine = new Machine()

let gridLocation = { x: 10, y: 10 }

while (machine.runProgram([gridLocation.x+99,gridLocation.y]) == 0 || machine.runProgram([gridLocation.x,gridLocation.y+99]) == 0) {
  while (machine.runProgram([gridLocation.x+99,gridLocation.y]) == 0) gridLocation.y++
  while (machine.runProgram([gridLocation.x,gridLocation.y+99]) == 0) gridLocation.x++
}

console.log(gridLocation);
console.log(`Answer: ${(gridLocation.x * 10000) + gridLocation.y}`);

// for (let y = gridLocation.y - 5; y < gridLocation.y + 105; y++) {
//   for (let x = gridLocation.x - 5; x < gridLocation.x + 105; x++) {
//     if (y >= gridLocation.y && y < gridLocation.y+100 && x >= gridLocation.x && x < gridLocation.x+100){
//       process.stdout.write('O')
//     } else {
//       const result = machine.runProgram([x,y])
//       result == 1 ? process.stdout.write('#') : process.stdout.write('.')
//       // process.stdout.write(grid[i][j])
//     }
//   }
//   console.log()
// }



