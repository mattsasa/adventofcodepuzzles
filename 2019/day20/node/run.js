const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n")
const grid = [], gridHeight = lines.length
let gridWidth = 0
lines.forEach(line => {
  grid.push([...line])
  if (line.length > gridWidth) gridWidth = line.length
})

function copyGrid(grid) {
  const copy = []
  for (let y = 0; y < gridHeight; y++){
    let row = []
    for (let x = 0; x < gridWidth; x++) {
      row.push(grid[y][x])
    }
    copy.push(row)
  }
  return copy
}

function addPortal(location, grid) {
  let dotLocation
  let portal = [grid[location.y][location.x]]
  grid[location.y][location.x] = '#'

  if (grid[location.y+1][location.x] == '.') dotLocation = { x: location.x, y: location.y + 1 }
  if (grid[location.y-1] && grid[location.y-1][location.x] == '.') dotLocation = { x: location.x, y: location.y - 1 }
  if (grid[location.y][location.x-1] == '.') dotLocation = { x: location.x - 1, y: location.y }
  if (grid[location.y][location.x+1] == '.') dotLocation = { x: location.x + 1, y: location.y }

  if (grid[location.y+1][location.x] && isLetter(grid[location.y+1][location.x])) {
    portal.push(grid[location.y+1][location.x])
    if (grid[location.y+2] && grid[location.y+2][location.x] == '.') dotLocation = { x: location.x, y: location.y + 2 }
    grid[location.y+1][location.x] = '#'
  }

  if (grid[location.y-1] && grid[location.y-1][location.x] && isLetter(grid[location.y-1][location.x])) {
    portal.push(grid[location.y-1][location.x])
    grid[location.y-1][location.x] = '#'
    if (grid[location.y-2][location.x] == '.') dotLocation = { x: location.x, y: location.y - 2 }
  }

  if (grid[location.y][location.x-1] && isLetter(grid[location.y][location.x-1])) {
    portal.push(grid[location.y][location.x-1])
    grid[location.y][location.x-1] = '#'
    if (grid[location.y][location.x-2] == '.') dotLocation = { x: location.x - 2, y: location.y }
  }

  if (grid[location.y][location.x+1] && isLetter(grid[location.y][location.x+1])) {
    portal.push(grid[location.y][location.x+1])
    grid[location.y][location.x+1] = '#'
    if (grid[location.y][location.x+2] == '.') dotLocation = { x: location.x + 2, y: location.y }
  }

  let finalPortal = portal.sort().join('')

  dotLocation.type = (dotLocation.x == 2 || dotLocation.x == gridWidth - 3 || dotLocation.y == 2 || dotLocation.y  == gridHeight - 3) ? "outside" : "inside"

  if (allPortals[finalPortal]) allPortals[finalPortal].push(dotLocation)
  else allPortals[finalPortal] = [dotLocation]
}

function findPortals() {
  const c_grid = copyGrid(grid)
  for (let y = 0; y < gridHeight; y++){
    for (let x = 0; x < gridWidth; x++) {
      if (c_grid[y][x] && isLetter(c_grid[y][x])) addPortal({x,y}, c_grid)
    }
  }
}

function printGrid(grid) {
  for (let y = 0; y < gridHeight; y++){
    for (let x = 0; x < gridWidth; x++) {
      if (grid[y][x] != null) {
        if (grid[y][x].toString().length < 2) process.stdout.write(' ')
        process.stdout.write(grid[y][x].toString())
      }
    }
    console.log();
  }
}

function isLetter(char) {
  if (!isNaN(char)) return false
  return char.charCodeAt(0) >= 65 && char.charCodeAt(0) <= 90
}

function goThroughPortal(newEnds, portal, end) {
  if (portal == 'ZZ') return end
  if (portal == end.lastPortal) return
  target = allPortals[portal].find(side => side.x != end.x && side.y != end.y)
  let direction = target.type == 'outside' ? 1 : -1
  newEnds.push({ x: target.x, y: target.y, distance: end.distance + 1, currentLevel: end.currentLevel + direction, lastPortal: portal })
  gridStack[end.currentLevel + direction][target.y][target.x] = end.distance + 1
}

function pathFinding(startLocation) {
  gridStack[0][startLocation.y][startLocation.x] = 0
  let ends = [{...startLocation, distance: 0, currentLevel: 0 }]


  let result = null
  count = 0
  while (ends.length != 0) {
    count++

    let newEnds = []
    ends.forEach(end => {

      if (gridStack.length - 1 == end.currentLevel){ gridStack.push(copyGrid(sampleInnerGrid)) }

      if (isLetter(gridStack[end.currentLevel][end.y+1][end.x])) {
        let portal = [gridStack[end.currentLevel][end.y+1][end.x],gridStack[end.currentLevel][end.y+2][end.x]].sort().join('')
        result = goThroughPortal(newEnds, portal, end)
      }
      if (isLetter(gridStack[end.currentLevel][end.y-1][end.x])) {
        let portal = [gridStack[end.currentLevel][end.y-1][end.x],gridStack[end.currentLevel][end.y-2][end.x]].sort().join('')
        result = goThroughPortal(newEnds, portal, end)
      }
      if (isLetter(gridStack[end.currentLevel][end.y][end.x+1])) {
        let portal = [gridStack[end.currentLevel][end.y][end.x+1],gridStack[end.currentLevel][end.y][end.x+2]].sort().join('')
        result = goThroughPortal(newEnds, portal, end)
      }
      if (isLetter(gridStack[end.currentLevel][end.y][end.x-1])) {
        let portal = [gridStack[end.currentLevel][end.y][end.x-1],gridStack[end.currentLevel][end.y][end.x-2]].sort().join('')
        result = goThroughPortal(newEnds, portal, end)
      }

      if (gridStack[end.currentLevel][end.y+1][end.x] == '.') {
        newEnds.push({ ...end, distance: end.distance + 1, y: end.y + 1 })
        gridStack[end.currentLevel][end.y+1][end.x] = end.distance + 1
      }
      if (gridStack[end.currentLevel][end.y-1][end.x] == '.') {
        newEnds.push({ ...end, distance: end.distance + 1, y: end.y - 1 })
        gridStack[end.currentLevel][end.y-1][end.x] = end.distance + 1
      }
      if (gridStack[end.currentLevel][end.y][end.x+1] == '.') {
        newEnds.push({ ...end, distance: end.distance + 1, x: end.x + 1 })
        gridStack[end.currentLevel][end.y][end.x+1] = end.distance + 1
      }
      if (gridStack[end.currentLevel][end.y][end.x-1] == '.') {
        newEnds.push({ ...end, distance: end.distance + 1, x: end.x - 1 })
        gridStack[end.currentLevel][end.y][end.x-1] = end.distance + 1
      }
    })
    ends = newEnds

    if (result) return result
  }
}

const allPortals = {}
findPortals()

const outerGrid = copyGrid(grid)
Object.keys(allPortals).forEach(key => {
  if (key == 'ZZ') return
  allPortals[key].forEach(side => { if (side.type == 'outside') outerGrid[side.y][side.x] = '#' })
})
const sampleInnerGrid = copyGrid(grid)
Object.keys(allPortals).forEach(key => {
  if (key == 'AA' ||  key == 'ZZ') allPortals[key].forEach(side => sampleInnerGrid[side.y][side.x] = '#')
})

const startLocation = allPortals['AA'][0]
outerGrid[startLocation.y+1][startLocation.x] = '#'
const gridStack = [outerGrid]

const final = pathFinding(startLocation)
console.log(`Total steps: ${final.distance}`);


