const fs = require('fs')
let lines = fs.readFileSync('../data/input.txt').toString().split("\n")
// lines = [...lines, ...lines]

function mod(n, m) { return ((n % m) + m) % m }

let deck = [], deckSize = 119315717514047

// for (let i = 0; i < deckSize; i++) { deck.push(i) }
//
// const decks = [[...deck],[...deck]]

let side = 0
let otherside = 1

function reverse() {
  decks[side].forEach((card,i) => { decks[otherside][deckSize - i - 1] = card })
}

function cutN(n) {
  decks[side].forEach((card,i) => { decks[otherside][mod(i - n, deckSize)] = card })
 }

function dealWithIncrement(n){
  decks[side].forEach((card,i) => { decks[otherside][n * i % deckSize] = card;
    // let result = n * i % deckSize
    // let a = result
    // while (a % n != 0) a += deckSize
    //
    // a = a / n
    //
    // let count = 0
    // while (result - n >= 0) {
    //   result -= n
    //   count++
    // }
    // // if (count != 0) count++
    // console.log(`i: ${i} -- result: ${n * i % deckSize}  --- finalResult: ${Math.floor(result*(deckSize/n)+count)}   a: ${a}`);

   })
}

// for (let i = 0; i < 2; i++) {
//   lines.forEach(line => {
//     otherside = side ? 0 : 1
//     switch (line.substring(10, 13)) {
//       case 'new': reverse(); break
//       case 'inc': dealWithIncrement(parseInt(line.substring(20))); break
//          default: cutN(parseInt(line.substring(4))); break
//     }
//     side = otherside
//   })
// }

// let index = decks[side].indexOf(2019)
// console.log(`The Position of card 2019: ${index}`);
// console.log(`The value of Position 2019: ${decks[side][2019]}`);

/// track method
let currentIndex = 2020

function reverseInv() { currentIndex = deckSize - currentIndex - 1 }

function cutNInv(n) { currentIndex = mod(currentIndex + n, deckSize) }

function dealWithIncrementInv(n){
  while (currentIndex % n != 0) currentIndex += deckSize
  currentIndex = currentIndex / n
}

lines.reverse()

let lastIndex = 0

let storage = []

// console.log(20336114746 * 5003);
// console.log(101741582076661 - 101741582074238);

for (let i = 0; i < 101741582076661; i++) {
    console.log(`Repeat: ${currentIndex - lastIndex}`);
    lastIndex = currentIndex
  // if (storage.includes(currentIndex)) console.log("includes " + currentIndex);
  // storage.push(currentIndex)
  // if (i % 10000 == 0) console.log(i);
  lines.forEach(line => {
    switch (line.substring(10, 13)) {
      case 'new': reverseInv(); break
      case 'inc': dealWithIncrementInv(parseInt(line.substring(20))); break
         default: cutNInv(parseInt(line.substring(4))); break
    }
  })
}

console.log(`The value/card at position 2019: ${currentIndex}`);

