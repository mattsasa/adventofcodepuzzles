
function neverDecrease(number) {
  let numString = number.toString()
  let digit = 0
  for (let i = 0; i < 6; i++){
    let newDigit = parseInt(numString[i])
    if (digit > newDigit) {
      return false
    }
    digit = newDigit
  }
  return true
}

function twoTheSame(number) {
  let numString = number.toString()
  let sameCount = 0
  let someArray = []
  let digit = -1
  for (let i = 0; i < 6; i++){
    let newDigit = parseInt(numString[i])
    if (digit == newDigit) {
      sameCount++
    } else {
      if (i != 0) someArray.push(sameCount+1)
      sameCount = 0
    }
    digit = newDigit
  }
  someArray.push(sameCount+1)
  return someArray.includes(2)
}

let count = 0
for (let i = 264360; i < 746325; i++){
  let noDecrease = neverDecrease(i)
  let samedigits = twoTheSame(i)
  if (noDecrease && samedigits) count++
}

console.log(count);
