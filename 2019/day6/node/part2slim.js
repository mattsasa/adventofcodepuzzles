const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const reverseOrbits = {}

input.forEach(line => {
  reverseOrbits[line.substring(line.indexOf(')') + 1)] = line.substring(0, line.indexOf(')'))
})

const pathToSanta = [], pathToYou = []

let currentKey = 'SAN'
while (currentKey != 'COM') {
  pathToSanta.push(currentKey)
  currentKey = reverseOrbits[currentKey]
}

currentKey = 'YOU'
while (currentKey != 'COM') {
  pathToYou.push(currentKey)
  currentKey = reverseOrbits[currentKey]
}

let intersectionPoint, youDist, santaDist
for (let i = 0; i < pathToYou.length; i++) {
  if (pathToSanta.includes(pathToYou[i])) { intersectionPoint = pathToYou[i]; youDist = i; break }
}
for (let i = 0; i < pathToSanta.length; i++) {
  if (pathToSanta[i] == intersectionPoint) { santaDist = i; break }
}

console.log("minimum number of orbital transfers required: " + (youDist + santaDist - 2));