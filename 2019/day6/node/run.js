const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const orbits = {}
const reverseOrbits = {}

input.forEach(line => {
  orbits[line.substring(0, line.indexOf(')'))] = line.substring(line.indexOf(')') + 1)
  reverseOrbits[line.substring(line.indexOf(')') + 1)] = line.substring(0, line.indexOf(')'))
  // const index = line.indexOf(')')
  // const orbit = {}
  // orbit.parent = line.substring(0, index)
  // orbit.child = line.substring(index+1)
  // orbits.push(orbit)
})

const pathToSanta = [], pathToYou = []

let currentKey = 'SAN'
while (currentKey != 'COM') {
  pathToSanta.push(currentKey)
  currentKey = reverseOrbits[currentKey]
}

currentKey = 'YOU'
while (currentKey != 'COM') {
  pathToYou.push(currentKey)
  currentKey = reverseOrbits[currentKey]
}

let intersectionPoint, youDist, santaDist
for (let i = 0; i < pathToYou.length; i++) {
  if (pathToSanta.includes(pathToYou[i])) { intersectionPoint = pathToYou[i]; youDist = i; break }
}
for (let i = 0; i < pathToSanta.length; i++) {
  if (pathToSanta[i] == intersectionPoint) { santaDist = i; break }
}

console.log("minimum number of orbital transfers required: " + (youDist + santaDist - 2));


// const root = { name: "COM", children: [] }
//
// while (orbits.length > 0) {
//   for(let i = 0; i < orbits.length; i++) {
//     let orbit = orbits[i]
//     let success = addToTree(root,orbit)
//     if (success) {
//       orbits.splice(i,1)
//       break
//     }
//   }
// }
//
// function addToTree(node, orbit) {
//   if (node.name == orbit.parent) { node.children.push({ name: orbit.child, children: [] }); return true }
//   else {
//     let success = false
//     for (let i = 0; i < node.children.length; i++) {
//         if (success == true) break
//         let child = node.children[i]
//         if (child.name == orbit.parent) { child.children.push({ name: orbit.child, children: [] }); return true }
//         else success = addToTree(child, orbit)
//     }
//     return success
//   }
// }
//
// console.log(JSON.stringify(root, null, 2));
//
// let count = 0
// removeOneLeaf(root, 0)
//
// function removeOneLeaf(node, depth) {
//   count += depth
//   node.depth = depth
//   node.children.forEach(child => {
//     removeOneLeaf(child, depth + 1)
//   })
// }
//
// console.log(count)
