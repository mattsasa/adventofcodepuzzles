const memory = [ 3,8,1001,8,10,8,105,1,0,0,21,34,59,68,85,102,183,264,345,426,99999,3,9,101,3,9,9,102,3,9,9,4,9,99,3,9,1002,9,4,9,1001,9,2,9,1002,9,2,9,101,5,9,9,102,5,9,9,4,9,99,3,9,1001,9,4,9,4,9,99,3,9,101,3,9,9,1002,9,2,9,1001,9,5,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,102,3,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99 ]


class Machine {
  constructor(setting) {
    this.memory = [...memory]
    this.pc = 0
    this.output = []
    this.halted = false

    this.input = [setting]
  }

  runProgram(newInput) {
    this.input.push(newInput)
    while (!this.halted && this.output.length == 0) { this.processOpcode() }

    if (this.halted) return newInput

    return this.output.shift()
  }

  processOpcode() {
    const opcode = this.memory[this.pc]
    const arg1 = this.memory[this.pc + 1], arg2 = this.memory[this.pc + 2], arg3 = this.memory[this.pc + 3]

    switch (opcode) {
      ///        #3rd param           #1st param          #2nd param
      case 1:    this.memory[arg3] = this.memory[arg1] +  this.memory[arg2];         this.pc += 4; break
      case 101:  this.memory[arg3] = arg1              +  this.memory[arg2];         this.pc += 4; break
      case 1001: this.memory[arg3] = this.memory[arg1] +  arg2;                      this.pc += 4; break
      case 1101: this.memory[arg3] = arg1              +  arg2;                      this.pc += 4; break

      case 2:    this.memory[arg3] = this.memory[arg1] *  this.memory[arg2];         this.pc += 4; break
      case 102:  this.memory[arg3] = arg1              *  this.memory[arg2];         this.pc += 4; break
      case 1002: this.memory[arg3] = this.memory[arg1] *  arg2;                      this.pc += 4; break
      case 1102: this.memory[arg3] = arg1              *  arg2;                      this.pc += 4; break

      case 7:    this.memory[arg3] = this.memory[arg1] <  this.memory[arg2];         this.pc += 4; break
      case 107:  this.memory[arg3] = arg1              <  this.memory[arg2];         this.pc += 4; break
      case 1007: this.memory[arg3] = this.memory[arg1] <  arg2;                      this.pc += 4; break
      case 1107: this.memory[arg3] = arg1              <  arg2;                      this.pc += 4; break

      case 8:    this.memory[arg3] = this.memory[arg1] == this.memory[arg2];         this.pc += 4; break
      case 108:  this.memory[arg3] = arg1              == this.memory[arg2];         this.pc += 4; break
      case 1008: this.memory[arg3] = this.memory[arg1] == arg2;                      this.pc += 4; break
      case 1108: this.memory[arg3] = arg1              == arg2;                      this.pc += 4; break

      ///         #condition                    #Set ip to 2nd param
      case 105:  if (arg1 != 0)               { this.pc = this.memory[arg2]; break } this.pc += 3; break
      case 1005: if (this.memory[arg1] != 0)  { this.pc = arg2; break }              this.pc += 3; break
      case 1105: if (arg1 != 0)               { this.pc = arg2; break }              this.pc += 3; break
      case 106:  if (arg1 == 0)               { this.pc = this.memory[arg2]; break } this.pc += 3; break
      case 1006: if (this.memory[arg1] == 0)  { this.pc = arg2; break }              this.pc += 3; break
      case 1106: if (arg1 == 0)               { this.pc = arg2; break }              this.pc += 3; break

      ///        ## 1st-only param
      case 3:    this.memory[arg1] = this.input.shift();                             this.pc += 2; break
      case 4:    this.output.push(this.memory[arg1]);                                this.pc += 2; break
      case 104:  this.output.push(arg1);                                             this.pc += 2; break

      case 99:   this.halted = true; break

      default: console.log("unimplemented opcode: " + opcode); process.exit()

    }
    return true
  }

}

function runAmplifierLoop(settings) {
  const amplifiers = []
  for (let i = 0; i < 5; i++) {
    let phaseSettingInput = parseInt(settings[i])
    amplifiers.push(new Machine(phaseSettingInput))
  }

  let ampID = 0, state = 0
  while (!amplifiers[4].halted) {
    if (!amplifiers[ampID].halted) state = amplifiers[ampID].runProgram(state)
    ampID++
    if (ampID == 5) ampID = 0
  }
  return state
}

function getPermutations(leafs) {
  const branches = []
  if (leafs.length == 1) return leafs
  for (let k in leafs) {
    let leaf = leafs[k]
    getPermutations(leafs.join('').replace(leaf, '').split('')).concat("").map(subtree => {
      branches.push([leaf].concat(subtree))
    })
  }
  return branches
}

let permutations = getPermutations("56789".split(''))

let highestSignal = 0
for (let i = 0; i < permutations.length; i++) {
  let settings = permutations[i]
  let anyempty = false
  settings.forEach(value => { if (value == '') anyempty = true })
  if (anyempty || settings.length < 5) continue
  let finalOutput = runAmplifierLoop(settings)
  if (finalOutput > highestSignal) { highestSignal = finalOutput }
}
console.log("highestSignal: " + highestSignal)








