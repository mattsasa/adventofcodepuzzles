
let memory = [ 3,8,1001,8,10,8,105,1,0,0,21,34,59,68,85,102,183,264,345,426,99999,3,9,101,3,9,9,102,3,9,9,4,9,99,3,9,1002,9,4,9,1001,9,2,9,1002,9,2,9,101,5,9,9,102,5,9,9,4,9,99,3,9,1001,9,4,9,4,9,99,3,9,101,3,9,9,1002,9,2,9,1001,9,5,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,102,3,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99 ]

let pc = 0
let input = []
let output = []

//while (memory[pc] != 99){ processOpcode() }

function getPermutations(leafs) {
  var branches = [];
  if (leafs.length == 1) return leafs;
  for (var k in leafs) {
    var leaf = leafs[k];
    getPermutations(leafs.join('').replace(leaf, '').split('')).concat("").map(function(subtree) {
      branches.push([leaf].concat(subtree));
    });
  }
  return branches;
};

let permutations = getPermutations("56789".split(''))

let highestSignal = 0
for (let i = 0; i < permutations.length; i++) {
  let settings = permutations[i]
  let anyempty = false
  settings.forEach(value => { if (value == '') anyempty = true })
  if (anyempty) continue
  if (settings.length < 5) continue
  let finalOutput = runAllAmplifiers(settings)
  if (finalOutput > highestSignal) { highestSignal = finalOutput; console.log(settings); }
}
console.log("highestSignal: " + highestSignal);


function runAllAmplifiers(settings) {
  let state = 0
  for (let i = 0; i < 5; i++) {
    let phaseSettingInput = parseInt(settings[i])
    if (!phaseSettingInput) phaseSettingInput = 0
    state = runProgram([phaseSettingInput, state])
  }
  //console.log(state);
  return state

}

function runProgram(givenInput){

  memory = [ 3,8,1001,8,10,8,105,1,0,0,21,34,59,68,85,102,183,264,345,426,99999,3,9,101,3,9,9,102,3,9,9,4,9,99,3,9,1002,9,4,9,1001,9,2,9,1002,9,2,9,101,5,9,9,102,5,9,9,4,9,99,3,9,1001,9,4,9,4,9,99,3,9,101,3,9,9,1002,9,2,9,1001,9,5,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,102,3,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99 ]

  pc = 0

  input = givenInput
  output = []

  while (memory[pc] != 99){ processOpcode() }

  //console.log(output);
  return output[0]

}

function processOpcode(){
  const opcode = memory[pc]
  const arg1 = memory[pc + 1], arg2 = memory[pc + 2], arg3 = memory[pc + 3]

  switch (opcode) {
    ///        #3rd param     #1st param     #2nd param
    case 1:    memory[arg3] = memory[arg1] +  memory[arg2];      pc += 4; break
    case 101:  memory[arg3] = arg1         +  memory[arg2];      pc += 4; break
    case 1001: memory[arg3] = memory[arg1] +  arg2;              pc += 4; break
    case 1101: memory[arg3] = arg1         +  arg2;              pc += 4; break

    case 2:    memory[arg3] = memory[arg1] *  memory[arg2];      pc += 4; break
    case 102:  memory[arg3] = arg1         *  memory[arg2];      pc += 4; break
    case 1002: memory[arg3] = memory[arg1] *  arg2;              pc += 4; break
    case 1102: memory[arg3] = arg1         *  arg2;              pc += 4; break

    case 7:    memory[arg3] = memory[arg1] <  memory[arg2];      pc += 4; break
    case 107:  memory[arg3] = arg1         <  memory[arg2];      pc += 4; break
    case 1007: memory[arg3] = memory[arg1] <  arg2;              pc += 4; break
    case 1107: memory[arg3] = arg1         <  arg2;              pc += 4; break

    case 8:    memory[arg3] = memory[arg1] == memory[arg2];      pc += 4; break
    case 108:  memory[arg3] = arg1         == memory[arg2];      pc += 4; break
    case 1008: memory[arg3] = memory[arg1] == arg2;              pc += 4; break
    case 1108: memory[arg3] = arg1         == arg2;              pc += 4; break

    ///         #condition      #Set ip to 2nd param
    case 105:  if (arg1 != 0) { pc = memory[arg2]; break }       pc += 3; break
    case 1005: if (memory[arg1] != 0) { pc = arg2; break }       pc += 3; break
    case 1105: if (arg1 != 0) { pc = arg2; break }               pc += 3; break
    case 106:  if (arg1 == 0) { pc = memory[arg2]; break }       pc += 3; break
    case 1006: if (memory[arg1] == 0) { pc = arg2; break }       pc += 3; break
    case 1106: if (arg1 == 0) { pc = arg2; break }               pc += 3; break

    ///        ## 1st-only param
    case 3:    memory[arg1] = input.shift();                     pc += 2; break
    case 4:    output.push(memory[arg1]);                        pc += 2; break
    case 104:  output.push(arg1);                                pc += 2; break

    default: console.log("unimplemented opcode: " + opcode); process.exit()
  }
}






