const fs = require('fs')

const imageData = fs.readFileSync('../data/input.txt').toString().split("")

const layerLength = 25 * 6

let count = -1
let layers = []

for (let i = 0; i < imageData.length; i++) {
  if (i % layerLength == 0) {
    layers.push([])
    count++
  }
  layers[count].push(imageData[i])
}

////// Part 1
let fewestLayer = -1
let bestAmountOfZeros = 150
layers.forEach((layer,i) => {
  let zeroCount = 0
  layer.forEach(number => {
    if (number == 0) zeroCount++
  })
  if (zeroCount < bestAmountOfZeros) {
    bestAmountOfZeros = zeroCount
    fewestLayer = i
  }
})

let ones = 0, twos = 0
layers[fewestLayer].forEach(number => {
  if (number == 1) ones++
  if (number == 2) twos++
})

console.log("Part 1: " + (ones * twos));
console.log();


/// Part 2

let grid = []
for (let y = 0; y < 6; y++){
  let row = []
  for (let x = 0; x < 25; x++) {
    row.push('')
  }
  grid.push(row)
}


function printImage() {
  for (let y = 0; y < 6; y++){
    for (let x = 0; x < 25; x++) {
      grid[y][x] == 1 ? process.stdout.write('#') : process.stdout.write(' ')
    }
    console.log();
  }
}

for (let i = layers.length - 1; i > -1; i--) {
  let pixelIndex = 0
  for (let y = 0; y < 6; y++){
    for (let x = 0; x < 25; x++) {
      if (layers[i][pixelIndex] == 0 || layers[i][pixelIndex] == 1) {
        grid[y][x] = layers[i][pixelIndex]
      }
      pixelIndex++
    }
  }
}

console.log("Part 2");
printImage()

