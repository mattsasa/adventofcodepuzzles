#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(void) {
    clock_t begin_ = clock();

    FILE *fptr = fopen("../data/input.txt", "r");

    int count = 0;
    int array_size = 100;

    int *entries = malloc(array_size*sizeof(int));

    char str[10];
    char *x = fgets(str, sizeof(str), fptr);
    while (x != NULL) {
        int num = atoi(str);
        entries[count++] = num;

        if (count > array_size) {
            array_size += 100;
            entries = realloc(entries, array_size * sizeof(int));
        }

        x = fgets(str, sizeof(str), fptr);
    }

    fclose(fptr);
    clock_t end_ = clock();
    float time_spent_ = (float)(end_ - begin_) / CLOCKS_PER_SEC;
    printf("exec time: %f\n", time_spent_);

    clock_t begin = clock();
    for (int i = 0; i < count; i++) {
        for (int j = 0; j < count; j++) {
            for (int k = 0; k < count; k++) {
                if (entries[i] + entries[j] + entries[k] == 2020) {
                    printf("found it: %d\n", entries[i] * entries[j] * entries[k]);
                    clock_t end = clock();
                    float time_spent = (float)(end - begin) / CLOCKS_PER_SEC;
                    printf("exec time: %f\n", time_spent);
                    free(entries);
                    return 0;
                }
            }
        }
    }



}