#include <stdio.h>
#include <stdlib.h>
#include <time.h>

__device__ long d_result;

__global__
void search(int count, int *entries)
{
  int index = (blockIdx.x * blockDim.x) + threadIdx.x;
//   int stride = blockDim.x * gridDim.x;

  if (index >= count*count*count) return;

  int j = index % count;
  int i = index / count;
  int k = i % count;
  i = i / count;

  if (entries[i] + entries[j] + entries[k] == 2020) {
    d_result = entries[i] * entries[j] * entries[k];
  }

}



int main(void) {

    clock_t begin_ = clock();

    FILE *fptr = fopen("../data/input.txt", "r");

    int count = 0;
    int array_size = 100;

    int *entries = (int*) malloc(array_size*sizeof(int));

    char str[10];
    char *x = fgets(str, sizeof(str), fptr);
    while (x != NULL) {
        int num = atoi(str);
        entries[count++] = num;

        if (count > array_size-1) {
            array_size += 100;
            entries = (int*) realloc(entries, array_size * sizeof(int));
        }

        x = fgets(str, sizeof(str), fptr);
    }

    fclose(fptr);

    clock_t end_ = clock();
    float time_spent_ = (float)(end_ - begin_) / CLOCKS_PER_SEC;
    printf("exec time: %f\n", time_spent_);



    int blockSize = 1024;
    int numBlocks = ((count*count*count) + blockSize - 1) / blockSize;

    if (numBlocks > 2147483647) {
        printf("Too many blocks.... exiting");
        exit(-1);
    }
    printf("Num Blocks * Blocksize: %d   vs count*count*count: %d \n", numBlocks * blockSize, count*count*count);

    int* cuda_entries;
    cudaMalloc(&cuda_entries, count*sizeof(int));
    cudaMemcpy(cuda_entries, entries, (count)*sizeof(int), cudaMemcpyDefault);

    clock_t begin = clock();
    search<<<numBlocks, blockSize>>>(count, cuda_entries);

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();

    long result;
    cudaMemcpyFromSymbol(&result, d_result, sizeof(result), 0, cudaMemcpyDeviceToHost);
    printf("cuda result: %ld\n", result);
    clock_t end = clock();
    float time_spent = (float)(end - begin) / CLOCKS_PER_SEC;
    printf("exec time: %f\n", time_spent);

    cudaFree(entries);

}