const fs = require('fs')

const start_ = process.hrtime();
const entries = fs.readFileSync('../data/input.txt').toString().split("\n").map(value => { return parseInt(value) })
const stop_ = process.hrtime(start_)
console.log(`Time Taken to execute: ${(stop_[0] * 1e9 + stop_[1])/1e9} seconds`)

const findAnswer = () => {
    for (let i = 0; i < entries.length; i++) {
        for (let j = 0; j < entries.length; j++) {
            for (let k = 0; k < entries.length; k++) {
                if (entries[i] + entries[j] + entries[k] == 2020) {
                    console.log(`product: ${entries[i] * entries[j] * entries[k]}`)
                    return
                }
            }
        }
    }
}

const start = process.hrtime();
findAnswer()
const stop = process.hrtime(start)
console.log(`Time Taken to execute: ${(stop[0] * 1e9 + stop[1])/1e9} seconds`)

