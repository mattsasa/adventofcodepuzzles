const fs = require('fs')
const input_ = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => parseInt(x)).sort((a, b) => { return a - b })

const input = [0, ...input_]
const differences = { 1: 0, 3: 0 }

for (let i = 1; i < input.length; i++) {
    const diff = input[i] - input[i - 1]
    differences[diff]++
}

differences[3]++
console.log("Part 1: ", differences[1] * differences[3])


////////////// Part  2 ////////////////

const lastJoltage = input[input.length - 1]  /// largest number adapter (3 less than the device)
const cache = {}

const findPossibleCombinations = (list) => {

    const startJoltage = list[0]
    if (cache[startJoltage]) return cache[startJoltage]
    if (startJoltage == lastJoltage) return 1

    let combinations = 0
    for (let i = 1; i < 4; i++) {  /// joltage differences 1 - 3 
        const index = list.indexOf(startJoltage + i)
        if (index != -1) {
            combinations += findPossibleCombinations(list.slice(index))
        }
    }

    cache[startJoltage] = combinations

    return combinations
}

console.log("Part 2: ", findPossibleCombinations(input))
