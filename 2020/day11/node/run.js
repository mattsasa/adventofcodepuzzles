const fs = require('fs')
let grid = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => x.split(''))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char);
        })
        console.log()
    })
    console.log()
}

const totalOccupied = (grid) => {
    let count = 0
    grid.forEach(line => {
        line.forEach(char => {
            if (char == '#') count++
        })
    })
    return count
}

printGrid(grid)

const getGridSafe = (x, y) => {
    if (y >= 0 && y < grid.length && x >= 0 && x < grid[0].length) {
        return grid[y][x]
    }
    return 0
}

const findFirstSeatSeen = (x, y, vx, vy) => {
    while (1) {
        x += vx
        y += vy
        switch (getGridSafe(x, y)) {
            case '.': break
            case '#': return true
            case 'L': 
            case 0: return false
        }
    }
}

let anyChange = false

const checkToBecomeOccupied = (x, y, tempGrid) => {
    tempGrid[y][x] = 'L'
    if (findFirstSeatSeen(x, y, -1, -1)) return
    if (findFirstSeatSeen(x, y, 0, -1)) return
    if (findFirstSeatSeen(x, y, +1, -1)) return
    if (findFirstSeatSeen(x, y, -1, 0)) return
    if (findFirstSeatSeen(x, y, +1, 0)) return
    if (findFirstSeatSeen(x, y, -1, +1)) return
    if (findFirstSeatSeen(x, y, 0, +1)) return
    if (findFirstSeatSeen(x, y, +1, +1)) return
    anyChange = true
    tempGrid[y][x] = '#'
}

const checkToBecomeEmpty = (x, y, tempGrid) => {
    let count = 0
    if (findFirstSeatSeen(x, y, -1, -1)) count++
    if (findFirstSeatSeen(x, y, 0, -1)) count++
    if (findFirstSeatSeen(x, y, +1, -1)) count++
    if (findFirstSeatSeen(x, y, -1, 0)) count++
    if (findFirstSeatSeen(x, y, +1, 0)) count++
    if (findFirstSeatSeen(x, y, -1, +1)) count++
    if (findFirstSeatSeen(x, y, 0, +1)) count++
    if (findFirstSeatSeen(x, y, +1, +1)) count++
    
    if (count >= 5) { tempGrid[y][x] = 'L'; anyChange = true }
    else tempGrid[y][x] = '#'
}

while (true) {

    const tempGrid = new Array(grid.length).fill(0).map(() => new Array(grid[0].length).fill('.'))
    anyChange = false

    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] == 'L') checkToBecomeOccupied(x, y, tempGrid)
            if (grid[y][x] == '#') checkToBecomeEmpty(x, y, tempGrid)
        }
    }

    //printGrid(tempGrid)

    grid = tempGrid

    if (!anyChange) break

}

console.log(totalOccupied(grid))

