const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => { return { action: x[0], value: parseInt(x.substring(1)) } } )

let directionIndex = 1001
const directions = ['N', 'E', 'S', 'W']
const location = { x: 0, y: 0 }

const getDirection = () => { return directions[directionIndex % 4] }

input.forEach(instruction => {

    let action = instruction.action == 'F' ? getDirection() : instruction.action

    switch (action) {
        case 'N': location.y += instruction.value; break
        case 'S': location.y -= instruction.value; break
        case 'W': location.x -= instruction.value; break
        case 'E': location.x += instruction.value; break
        case 'L': directionIndex -= (instruction.value / 90); break
        case 'R': directionIndex += (instruction.value / 90); break
    }

})

console.log("Part 1: ", Math.abs(location.x) + Math.abs(location.y))