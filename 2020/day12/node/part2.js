const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => { return { action: x[0], value: parseInt(x.substring(1)) } } )

const shipLocation = { x: 0, y: 0 }
const waypointLocation = { x: 10, y: 1 }

const moveShip = (multiples) => {

    const xDiff = ((shipLocation.x - waypointLocation.x) * multiples)
    const yDiff = ((shipLocation.y - waypointLocation.y) * multiples)

    shipLocation.x += xDiff
    shipLocation.y += yDiff

    waypointLocation.x += xDiff
    waypointLocation.y += yDiff

}

const rotate = (degrees) => {
    const radians = degrees * 0.0174533

    const xDiff = waypointLocation.x - shipLocation.x
    const yDiff = waypointLocation.y - shipLocation.y

    const newX = Math.round((xDiff * Math.cos(radians)) - (yDiff * Math.sin(radians)))
    const newY = Math.round((yDiff * Math.cos(radians)) + (xDiff * Math.sin(radians)))

    waypointLocation.x += (newX - xDiff)
    waypointLocation.y += (newY - yDiff)
}

input.forEach(instruction => {

    switch (instruction.action) {
        case 'N': waypointLocation.y += instruction.value; break
        case 'S': waypointLocation.y -= instruction.value; break
        case 'W': waypointLocation.x -= instruction.value; break
        case 'E': waypointLocation.x += instruction.value; break
        case 'L': rotate(instruction.value); break
        case 'R': rotate(-instruction.value); break
        case 'F': moveShip(instruction.value); break
    }

})


console.log("Part 2: ", Math.abs(shipLocation.x) + Math.abs(shipLocation.y))