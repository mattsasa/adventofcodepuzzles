const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => { return { action: x[0], value: parseInt(x.substring(1)) } } )

const shipLocation = { x: 0, y: 0 }
const waypointLocation = { x: 10, y: 1 }

const moveShip = (multiples) => {
    shipLocation.x += (waypointLocation.x * multiples)
    shipLocation.y += (waypointLocation.y * multiples)
}

const rotate = (degrees) => {
    const radians = degrees * 0.0174533
    const newX = Math.round((waypointLocation.x * Math.cos(radians)) - (waypointLocation.y * Math.sin(radians)))
    const newY = Math.round((waypointLocation.y * Math.cos(radians)) + (waypointLocation.x * Math.sin(radians)))
    waypointLocation.x = newX
    waypointLocation.y = newY
}

input.forEach(instruction => {
    switch (instruction.action) {
        case 'N': waypointLocation.y += instruction.value; break
        case 'S': waypointLocation.y -= instruction.value; break
        case 'W': waypointLocation.x -= instruction.value; break
        case 'E': waypointLocation.x += instruction.value; break
        case 'L': rotate(instruction.value); break
        case 'R': rotate(-instruction.value); break
        case 'F': moveShip(instruction.value); break
    }
})

console.log("Part 2: ", Math.abs(shipLocation.x) + Math.abs(shipLocation.y))