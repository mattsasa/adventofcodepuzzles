const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const yourTime = parseInt(input[0])
const busIds = input[1].split(',').filter(id => id != 'x')

let smallestWaitTime = 1000000
let bestId = -1
busIds.forEach(id => {
    const waitTime = id - (yourTime % id)
    if (waitTime < smallestWaitTime) {
        smallestWaitTime = waitTime
        bestId = id
    }
})

console.log(smallestWaitTime)
console.log(bestId)