const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const busIds = input[1].split(',').map((id, index) => { return { id: parseInt(id), offset: index } }).filter(entry => !isNaN(entry.id))

let timestamp = 0
let interval = 1
let prev = 0

for (bus of busIds) {

    prev = 0

    while (1) {
        if ((timestamp + bus.offset) % bus.id == 0) {
            if (prev == 0) prev = timestamp
            else { interval = timestamp - prev; break }
        }
        timestamp += interval
    }
}

console.log("Part 2: ", prev)
