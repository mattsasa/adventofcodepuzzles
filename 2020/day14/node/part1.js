const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const commands = []

input.forEach(line => {
    const parts = line.split('=')
    if (parts[0] == 'mask ') {
        commands.push({ type: 'mask', mask: parts[1].slice(1) })
    } else {
        const index = parts[0].indexOf('[')
        const index2 = parts[0].indexOf(']')


        const stringValue = parseInt(parts[1].slice(1)).toString(2)

        commands.push({
            type: 'set',
            index: parseInt(parts[0].slice(index + 1, index2)),
            value: new Array(36 - stringValue.length).fill('0').join("") + stringValue
        })

    }
})

let mask
const memory = {}

for (command of commands) {

    if (command.type == 'mask') {
        mask = command.mask
    } else if (command.type == 'set') {
        memory[command.index] = ''
        for (let i = 0; i < 36; i++) {
            if (mask[i] == 'X') memory[command.index] += command.value[i]
            else memory[command.index] += mask[i]
        }
        memory[command.index] = parseInt(memory[command.index], 2) /// convert back to number
    } else throw "ERROR!"
}

const answer = Object.values(memory).reduce(((accum, value) => accum + value), 0)
console.log("part 1: ", answer)

