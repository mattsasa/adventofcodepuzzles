const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const commands = []

input.forEach(line => {
    const parts = line.split('=')
    if (parts[0] == 'mask ') {
        commands.push({ type: 'mask', mask: parts[1].slice(1) })
    } else {
        const index = parts[0].indexOf('[')
        const index2 = parts[0].indexOf(']')

        const stringIndex = parseInt(parts[0].slice(index + 1, index2)).toString(2)

        commands.push({
            type: 'set',
            index: new Array(36 - stringIndex.length).fill('0').join("") + stringIndex,
            value: parseInt(parts[1].slice(1))
        })

    }
})


const generateIndexes = (stringIndex) => {
    for (let i = 0; i < stringIndex.length; i++) {
        if (stringIndex[i] == 'X') {
            return generateIndexes(stringIndex.slice(i + 1)).reduce((accum, str) => {
                return [...accum, stringIndex.slice(0, i) + '0' + str, stringIndex.slice(0, i) + '1' + str ]
            }, [])
        }
    }
    return [stringIndex]
}




let mask
const memory = {}

for (command of commands) {

    if (command.type == 'mask') {
        mask = command.mask
    } else if (command.type == 'set') {

        let result = ''
        for (let i = 0; i < 36; i++) {
            if (mask[i] != '0') result += mask[i]
            else result += command.index[i]
        }
        generateIndexes(result).forEach(idx => { memory[parseInt(idx, 2)] = command.value  })
        
    } else throw "ERROR!"
}

const answer = Object.values(memory).reduce(((accum, value) => accum + value), 0)
console.log("part 2: ", answer)