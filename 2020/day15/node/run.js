const fs = require('fs')
const input = fs.readFileSync('../data/sample.txt').toString().split(',').map(x => parseInt(x))

const spoken = new Array(1000000000)

input.forEach((number, index) => {
    spoken[number] = index + 1
})

let lastSpoken = 0

for (let turn = input.length + 1; turn < 30000000; turn++) {
    const lastSpokenTurn = spoken[lastSpoken]
    if (lastSpokenTurn){
        let difference = turn - lastSpokenTurn
        spoken[lastSpoken] = turn 
        lastSpoken = difference

    } else { // new number
        spoken[lastSpoken] = turn 
        lastSpoken = 0
    }
}

console.log(lastSpoken)