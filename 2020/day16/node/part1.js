const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n').map(x => x.split('\n'))

const fieldRules = input[0].map((line) => {
    const parts = line.split(':')
    const ranges = parts[1].split(" or ")
    const dashA = ranges[0].indexOf('-')
    const dashB = ranges[1].indexOf('-')

    return {
        lowerA: parseInt(ranges[0].slice(0, dashA)),
        upperA: parseInt(ranges[0].slice(dashA + 1, ranges[0].length)),
        lowerB: parseInt(ranges[1].slice(0, dashB)),
        upperB: parseInt(ranges[1].slice(dashB + 1, ranges[1].length)) 
    }
})

const nearbyTickets = input[2].slice(1).map((x) => {
    return x.split(',').map(y => parseInt(y))
})

let sum = 0

nearbyTickets.forEach(ticket => {
    ticket.forEach(num => {
        let valid = false
        fieldRules.forEach(rule => {
            if ((rule.lowerA <= num && num <= rule.upperA) || (rule.lowerB <= num && num <= rule.upperB)) {
                valid = true
            }
        })
        if (!valid) sum += num
    })
})

console.log("Part 1: ", sum)