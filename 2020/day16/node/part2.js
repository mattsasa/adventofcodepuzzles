const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n').map(x => x.split('\n'))

const fieldRules = input[0].map((line) => {
    const parts = line.split(':')
    const ranges = parts[1].split(" or ")
    const dashA = ranges[0].indexOf('-')
    const dashB = ranges[1].indexOf('-')

    return {
        field: parts[0],
        lowerA: parseInt(ranges[0].slice(0, dashA)),
        upperA: parseInt(ranges[0].slice(dashA + 1, ranges[0].length)),
        lowerB: parseInt(ranges[1].slice(0, dashB)),
        upperB: parseInt(ranges[1].slice(dashB + 1, ranges[1].length)) 
    }
})

const myTicket = input[1][1].split(',').map(x => parseInt(x))

const nearbyTickets = input[2].slice(1).map((x) => {
    return x.split(',').map(y => parseInt(y))
})

const tickets = [myTicket]

let sum = 0

nearbyTickets.forEach(ticket => {
    let keepTicket = true
    ticket.forEach(num => {
        let valid = false
        fieldRules.forEach(rule => {
            if ((rule.lowerA <= num && num <= rule.upperA) || (rule.lowerB <= num && num <= rule.upperB)) {
                valid = true
            }
        })
        if (!valid) { sum += num; keepTicket = false }
    })
    if (keepTicket) tickets.push(ticket)
})

console.log("Part 1: ", sum)

const numFields = fieldRules.length

const fieldToCol = {}

while (Object.keys(fieldToCol).length < numFields) {
    fieldRules.forEach(rule => {
        let options = new Array(numFields).fill(0).map((_, i) => i).filter(i => !Object.values(fieldToCol).includes(i))
        tickets.forEach(ticket => {
            ticket.forEach((num, index) => {
                if (!(rule.lowerA <= num && num <= rule.upperA) && !(rule.lowerB <= num && num <= rule.upperB)) {
                    options = options.filter(item => item != index)
                } 
            })
        })
        if (options.length == 1) fieldToCol[rule.field] = options[0]
    })
}

const answer = Object.entries(fieldToCol).reduce((product, [field, col]) => {
    return (field.substring(0, 9) == "departure") ? product * myTicket[col] : product
}, 1)

console.log("Part 2: ", answer)



