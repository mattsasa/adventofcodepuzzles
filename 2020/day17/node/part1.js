const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n')

let grid = new Array(50).fill(0).map(_ => new Array(50).fill(0).map(_ => new Array(50).fill('.')))
let tempGrid

const printGrid = () => {
    grid[25].forEach(line => {
        line.forEach(char => {
            process.stdout.write(char);
        })
        console.log()
    })
    console.log()
}

const countActive = () => {
    let activeCount = 0
    for (let z = 0; z < 50; z++) {
        for (let y = 0; y < 50; y++) {
            for (let x = 0; x < 50; x++) {
                if (grid[z][y][x] == '#') { activeCount++ } 
            }
        }
    }
    return activeCount
}

input.forEach((line, y) => {
    line.split('').forEach((state, x) => {
        grid[25][y + 25][x + 25] = state
    })
})



printGrid(grid)

const getGridSafe = (x, y) => {
    if (y >= 0 && y < grid.length && x >= 0 && x < grid[0].length) {
        return grid[y][x]
    }
    return 0
}


const checkToActivate = (x_, y_, z_) => {

    let activeCount = 0
    for (let z = z_ - 1; z <= z_ + 1; z++) {
        for (let y = y_ - 1; y <= y_ + 1; y++) {
            for (let x = x_ - 1; x <= x_ + 1; x++) {
                if (x == x_ && y == y_ && z == z_) continue
                if (grid[z][y][x] == '#') activeCount++ 
            }
        }
    }

    if (activeCount == 3) {
        tempGrid[z_][y_][x_] = '#'
    } else {
        tempGrid[z_][y_][x_] = '.'
    }
}

const checkToDeactivate = (x_, y_, z_) => {

    let activeCount = 0
    for (let z = z_ - 1; z <= z_ + 1; z++) {
        for (let y = y_ - 1; y <= y_ + 1; y++) {
            for (let x = x_ - 1; x <= x_ + 1; x++) {
                if (x == x_ && y == y_ && z == z_) continue
                if (grid[z][y][x] == '#') activeCount++ 
            }
        }
    }

    if (activeCount == 2 || activeCount == 3) {
        tempGrid[z_][y_][x_] = '#'
    } else {
        tempGrid[z_][y_][x_] = '.'
    }

}


for (let i = 0; i < 6; i++) {
    tempGrid = new Array(50).fill(0).map(_ => new Array(50).fill(0).map(_ => new Array(50).fill('.')))

    for (let z = 1; z < 49; z++) {
        for (let y = 1; y < 49; y++) {
            for (let x = 1; x < 49; x++) {
                if (grid[z][y][x] == '.') { checkToActivate(x, y, z) } /// inactive
                if (grid[z][y][x] == '#') { checkToDeactivate(x, y, z) } /// active
            }
        }
    }

    grid = tempGrid
}

printGrid(grid)

console.log(countActive())