const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n')

let grid = new Array(15).fill(0).map(_ => new Array(15).fill(0).map(_ => new Array(22).fill(0).map(_ => new Array(22).fill('.'))))
let tempGrid = new Array(15).fill(0).map(_ => new Array(15).fill(0).map(_ => new Array(22).fill(0).map(_ => new Array(22).fill('.'))))

const printGrid = () => {
    grid[7][7].forEach(line => {
        line.forEach(char => {
            process.stdout.write(char);
        })
        console.log()
    })
    console.log()
}

const countActive = () => {
    let activeCount = 0
    for (let w = 1; w < 14; w++) {
        for (let z = 1; z < 14; z++) {
            for (let y = 1; y < 21; y++) {
                for (let x = 1; x < 21; x++) {
                    if (grid[w][z][y][x] == '#') { activeCount++ } 
                }
            }
        }
    }
    return activeCount
}

input.forEach((line, y) => {
    line.split('').forEach((state, x) => {
        grid[7][7][y + 7][x + 7] = state
    })
})



//printGrid(grid)

const checkCube = (x_, y_, z_, w_, state) => {

    let activeCount = 0
    for (let w = w_ - 1; w <= w_ + 1; w++) {
        for (let z = z_ - 1; z <= z_ + 1; z++) {
            for (let y = y_ - 1; y <= y_ + 1; y++) {
                for (let x = x_ - 1; x <= x_ + 1; x++) {
                    if (x == x_ && y == y_ && z == z_ && w == w_) continue
                    if (grid[w][z][y][x] == '#') activeCount++ 
                }
            }
        }
    }

    if (activeCount == 3 || (activeCount == 2 && state == '#' )) {
        tempGrid[w_][z_][y_][x_] = '#'
    } else {
        tempGrid[w_][z_][y_][x_] = '.'
    }
}


for (let i = 0; i < 6; i++) {
    
    for (let w = 1; w < 14; w++) {
        for (let z = 1; z < 14; z++) {
            for (let y = 1; y < 21; y++) {
                for (let x = 1; x < 21; x++) {
                    checkCube(x, y, z, w, grid[w][z][y][x])
                }
            }
        }
    }

    let temptemp = grid
    grid = tempGrid
    tempGrid = temptemp
}

//printGrid(grid)

console.log(countActive())