const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n')

const tokenizeLine = (line) => {
    return line.split('').filter(char => char != ' ').map(char => !isNaN(parseInt(char)) ? parseInt(char) : char)
}

const findEnd = (i, tokenList, terminator) => {
    let nestedCount = 0
    for (let j = i + 2; j < tokenList.length; j++) {
        if (tokenList[j] == terminator && nestedCount == 0) return j
        if (tokenList[j] == '(') nestedCount++
        if (tokenList[j] == ')') nestedCount--
    }
}

const doMath = (tokenList) => {

    tokenList = ['+', ...tokenList]

    let total = 0

    for (let i = 0; i < tokenList.length; i += 2) {
        let operand = tokenList[i]

        let value = tokenList[i + 1]

        if (value == '(') {
            let end = findEnd(i, tokenList, ')')
            value = doMath(tokenList.slice(i + 2, end))
            i = end - 1
        }

        //// Part 2
        if (operand == '*' && tokenList[i + 2] == '+') {
            let end = findEnd(i, tokenList, '*')
            value += doMath(tokenList.slice(i + 3, end))
            i = end - 2
        } //// End Part 2

        operand == '+' ? total += value : total *= value
    }

    return total
}

const sum = input.reduce((accum, line) => accum + doMath(tokenizeLine(line)), 0)

console.log("Answer:", sum)