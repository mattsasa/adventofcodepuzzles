const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n')

const rules = Object.fromEntries(input[0].split('\n').map(line => {
    const colonIndex = line.indexOf(':')
    const barIndex = line.indexOf('|') != -1 ? line.indexOf('|') - 1 : line.length

    let first = line.slice(colonIndex+2, barIndex).split(' ')

    if (first[0][0] == '"') first = first[0][1]

    let second 
    if (barIndex != line.length)
        second = line.slice(barIndex + 3).split(' ')

    return [ line.slice(0, colonIndex), { first, second } ]
}))

const cache = {}

const findPossibleMatches = (ruleNum) => {
    if (cache[ruleNum]) return cache[ruleNum]
    const ruleBody = rules[ruleNum]
    if (isNaN(parseInt(ruleBody.first))) return [ruleBody.first]

    const newMatches = []
    let matchesA = findPossibleMatches(ruleBody.first[0])
    let matchesB = ruleBody.first[1] ? findPossibleMatches(ruleBody.first[1]) : [""]
    let matchesC = ruleBody.first[2] ? findPossibleMatches(ruleBody.first[2]) : [""]

    for (let k = 0; k < matchesC.length; k++) {
        for (let i = 0; i < matchesA.length; i++) {
            for (let j = 0; j < matchesB.length; j++) {
                newMatches.push(matchesA[i] + matchesB[j] + matchesC[k])
            }
        }
    }

    if (ruleBody.second) {
        matchesA = findPossibleMatches(ruleBody.second[0])
        matchesB = ruleBody.second[1] ? findPossibleMatches(ruleBody.second[1]) : [""]
        for (let k = 0; k < matchesC.length; k++) {
            for (let i = 0; i < matchesA.length; i++) {
                for (let j = 0; j < matchesB.length; j++) {
                    newMatches.push(matchesA[i] + matchesB[j])
                }
            }
        }
    }

    cache[ruleNum] = newMatches
    return newMatches
}

const rule0matches = findPossibleMatches('0')

let sum = 0
input[1].split('\n').forEach((line, i) => {
    if (rule0matches.includes(line)) sum++
})
console.log("Answer:", sum)