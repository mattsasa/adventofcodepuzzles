const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n')

const rules = Object.fromEntries(input[0].split('\n').map(line => {
    const colonIndex = line.indexOf(':')
    const barIndex = line.indexOf('|') != -1 ? line.indexOf('|') - 1 : line.length

    let first = line.slice(colonIndex+2, barIndex).split(' ')

    if (first[0][0] == '"') first = first[0][1]

    let second 
    if (barIndex != line.length)
        second = line.slice(barIndex + 3).split(' ')

    return [ line.slice(0, colonIndex), { first, second } ]
}))


const isValidforRule = (ruleNum, string) => {

    const initString = string
    const ruleBody = rules[ruleNum]

    if (isNaN(parseInt(ruleBody.first))) {  /// terminating rule
        if (string[0] == ruleBody.first) return string.slice(1)
        else return
    }

    if (ruleNum == '8') {
        string = isValidforRule('42', string)
        if (string == undefined) return
        let finalValid = isValidforRule('11', string) == ""
        while (!finalValid && string) {
            string = isValidforRule('42', string)
            if (string) finalValid = isValidforRule('11', string) == ""
        }
        return string
    }


    if (ruleNum == '11') {
        const storeString = string
        let finalValid = false
        let count = 1
        while (!finalValid) {
            string = storeString // restore
            for (let i = 0; i < count; i++) {
                string = isValidforRule('42', string)
                if (string == undefined) return
            }
            for (let i = 0; i < count; i++) {
                if (string == undefined) continue
                string = isValidforRule('31', string)
                if (string == undefined) continue
            }
            if (string == "") finalValid = true
            count++
        }
        return string
    } 


    for (let i = 0; i < ruleBody.first.length; i++) {
        string = isValidforRule(ruleBody.first[i], string)
        if (string == undefined) break
    }

    if (ruleBody.second && string == undefined) {

        string = initString /// restore
        for (let i = 0; i < ruleBody.second.length; i++) {
            string = isValidforRule(ruleBody.second[i], string)
            if (string == undefined) break
        }
    }

    return string

}

const sum = input[1].split('\n').reduce((sum, line) => isValidforRule('0', line) == "" ? ++sum : sum, 0)
console.log("Answer:", sum)
