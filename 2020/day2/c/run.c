#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct Object {
    int min;
    int max;
    char letter;
    char* password;
};

void getCharsUpToTerminator(char** it, char terminator, char input_part[]) {
    int part_index = 0;
    while (**it != terminator) {
        input_part[part_index] = **it;
        (*it)++;
        part_index++;
    }
    input_part[part_index] = '\0';
}

int main(void) {

    clock_t begin = clock();

    FILE *fptr = fopen("../data/input.txt", "r");

    int count = 0;
    int array_size = 100;

    char** input_strings = malloc(array_size*sizeof(char*));

    char input_str[50];
    char *x = fgets(input_str, sizeof(input_str), fptr);
    while (x != NULL) {
        input_strings[count] = malloc((strlen(input_str)+1)*sizeof(char));
        strcpy(input_strings[count++], input_str);
        if (count > array_size - 1) {
            array_size += 100;
            input_strings = realloc(input_strings, array_size * sizeof(char*));
        }

        x = fgets(input_str, sizeof(input_str), fptr);
    }

    fclose(fptr);

    struct Object objects[count];

    for (int i = 0; i < count; i++) {
        struct Object newObject;

        char* it = input_strings[i];
        char input_part[25];

        getCharsUpToTerminator(&it, '-', input_part);
        newObject.min = atoi(input_part);

        it++;
        getCharsUpToTerminator(&it, ' ', input_part);
        newObject.max = atoi(input_part);

        it++;
        newObject.letter = *it;

        it += 3;
        getCharsUpToTerminator(&it, '\0', input_part);
        newObject.password = malloc((strlen(input_part)+1)*sizeof(char));
        strcpy(newObject.password, input_part);

        objects[i] = newObject;
        free(input_strings[i]);
    }

    free(input_strings);

    int validPasswords = 0;

    for (int j = 0; j < 1000; j++) {
        struct Object obj = objects[j];
        int count = 0;
        for (int i = 0; i < strlen(obj.password); i++) {
            if (obj.password[i] == obj.letter && (i + 1) == obj.min) count++;
            if (obj.password[i] == obj.letter && (i + 1) == obj.max) count++;
        }
        if (count == 1) validPasswords++;
        free(obj.password);
    }

    printf("Valid Passwords: %d\n", validPasswords);

    clock_t end = clock();
    float time_spent = (float)(end - begin) / CLOCKS_PER_SEC;
    printf("exec time: %f\n", time_spent);

    return 0;
}