#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


struct Object {
    int min;
    int max;
    char letter;
    char* password;
};

__device__
void getCharsUpToTerminator(char** it, char terminator, char input_part[]) {
    int part_index = 0;
    while (**it != terminator) {
        input_part[part_index] = **it;
        (*it)++;
        part_index++;
    }
    input_part[part_index] = '\0';
}

// A simple atoi() function
__device__
int myAtoi(char* str)
{
    int res = 0;
    int i;
    for (i = 0; str[i] != '\0'; ++i)
        res = res * 10 + str[i] - '0';
 
    return res;
}

__global__
void checkPasswords(int* validPasswords, Object* objects)
{
    int index = threadIdx.x;

    struct Object obj = objects[index];
    int count = 0;
    for (int i = 0; i < 25; i++) {
        if (obj.password[i] == obj.letter && (i + 1) == obj.min) count++;
        if (obj.password[i] == obj.letter && (i + 1) == obj.max) count++;
    }
    if (count == 1) validPasswords[index]++;
    free(obj.password);
}


__global__
void createObjects(char** input_strings, Object* objects)
{
    int index = threadIdx.x;

    struct Object newObject;

    char* it = input_strings[index];
    char input_part[25];

    getCharsUpToTerminator(&it, '-', input_part);
    newObject.min = myAtoi(input_part);

    it++;
    getCharsUpToTerminator(&it, ' ', input_part);
    newObject.max = myAtoi(input_part);

    it++;
    newObject.letter = *it;

    it += 3;
    getCharsUpToTerminator(&it, '\0', input_part);

    objects[index] = newObject;
    memcpy(objects[index].password, input_part, (25)*sizeof(char));

    free(input_strings[index]);
}

int main(void) {

    clock_t begin = clock();

    FILE *fptr = fopen("../data/input.txt", "r");

    int count = 0;
    int array_size = 100;

    char** input_strings = (char**) malloc(array_size*sizeof(char*));

    char input_str[50];
    char *x = fgets(input_str, sizeof(input_str), fptr);
    while (x != NULL) {
        input_strings[count] = (char*) malloc((strlen(input_str)+1)*sizeof(char));
        strcpy(input_strings[count++], input_str);
        if (count > array_size - 1) {
            array_size += 100;
            input_strings = (char**) realloc(input_strings, array_size * sizeof(char*));
        }

        x = fgets(input_str, sizeof(input_str), fptr);
    }
    fclose(fptr);

    char** input_strings_cuda;
    cudaMallocManaged(&input_strings_cuda, count*sizeof(char*));
    for (int i = 0; i < count; i++) {
        cudaMallocManaged(&input_strings_cuda[i], (strlen(input_strings[i])+1)*sizeof(char));
        cudaMemcpy(input_strings_cuda[i], input_strings[i], (strlen(input_strings[i])+1)*sizeof(char), cudaMemcpyDefault);
        free(input_strings[i]);
    }
    free(input_strings);
 
    Object* objects;
    cudaMallocManaged(&objects, (count)*sizeof(Object));
    for (int i = 0; i < count; i++) {
        cudaMallocManaged(&objects[i].password, (25)*sizeof(char));
    }

    int blockSize = 1024;
    int numBlocks = ((count) + blockSize - 1) / blockSize;
    if (count < blockSize) blockSize = count;

    if (numBlocks > 2147483647) {
        printf("Too many blocks.... exiting");
        exit(-1);
    }
    printf("Num Blocks: %d   vs count: %d \n", numBlocks, count);

    createObjects<<<numBlocks, blockSize>>>(input_strings_cuda, objects);

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();
    cudaFree(input_strings_cuda);

    int* validPasswords;
    cudaMallocManaged(&validPasswords, count*sizeof(int));
    for (int i = 0; i < count; i++) { validPasswords[i] = 0; }

    checkPasswords<<<numBlocks, blockSize>>>(validPasswords, objects);

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();
    cudaFree(objects);

    int finalValidPasswords = 0;
    for (int i = 0; i < count; i++) { finalValidPasswords += validPasswords[i]; }
    cudaFree(validPasswords);

    printf("Final Valid Passwords: %d\n", finalValidPasswords);

    clock_t end = clock();
    float time_spent = (float)(end - begin) / CLOCKS_PER_SEC;
    printf("exec time: %f\n", time_spent);
}