const fs = require('fs')

const start = process.hrtime();

const passwords = fs.readFileSync('../data/input.txt').toString().split("\n")

const objects = passwords.map(line => {
    const parts = line.split(' ')
    const dashIndex = parts[0].indexOf('-')
    return {
        min: parts[0].substring(0, dashIndex),
        max: parts[0].substring(dashIndex + 1, parts[0].length),
        letter: parts[1].substring(0, parts[1].length - 1),
        password: parts[2]
    }
})

let validPasswords = 0

///// Part 1
/*objects.forEach(obj => {
    let count = 0
    for (let i = 0; i < obj.password.length; i++) {
        if (obj.password[i] == obj.letter) count++
    }
    if (count >= obj.min && count <= obj.max) validPasswords++
})*/

////////// Part 2
objects.forEach(obj => {
    let count = 0
    for (let i = 0; i < obj.password.length; i++) {
        if (obj.password[i] == obj.letter && (i + 1) == obj.min) count++
        if (obj.password[i] == obj.letter && (i + 1) == obj.max) count++
    }
    if (count == 1) validPasswords++
})

console.log(validPasswords)

const stop = process.hrtime(start)
console.log(`Time Taken to execute: ${(stop[0] * 1e9 + stop[1])/1e9} seconds`)

