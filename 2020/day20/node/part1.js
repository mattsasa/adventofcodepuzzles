const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n').map(x => x.split('\n'))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

const edgeMap = {}

const tiles = input.map(section => {
    const tileId = parseInt(section[0].substring(5))

    return {
        tileId,
        grid: section.slice(1).map(line => line.split(''))
    }
})

const getGridTop = (grid) => { return grid[0].join('') }
const getGridBottom = (grid) => { return grid[9].join('') }
const getGridLeft = (grid) => { return grid.map(row => row[0]).join('') }
const getGridRight = (grid) => { return grid.map(row => row[9]).join('') }

const addEdgeToMap = (edge) => {
    if (edgeMap[edge]) edgeMap[edge]++
    else edgeMap[edge] = 1
}

tiles.forEach(tile => {
    let edge
    edge = getGridLeft(tile.grid)
    addEdgeToMap(edge)
    addEdgeToMap(edge.split('').reverse().join(''))
    edge = getGridRight(tile.grid)
    addEdgeToMap(edge)
    addEdgeToMap(edge.split('').reverse().join(''))
    edge = getGridTop(tile.grid)
    addEdgeToMap(edge)
    addEdgeToMap(edge.split('').reverse().join(''))
    edge = getGridBottom(tile.grid)
    addEdgeToMap(edge)
    addEdgeToMap(edge.split('').reverse().join(''))
})

let total = 1

tiles.forEach(tile => {
    let matches = 0
    let edge
    edge = getGridLeft(tile.grid)
    if (edgeMap[edge] == 2) matches++
    edge = edge.split('').reverse().join('')
    if (edgeMap[edge] == 2) matches++
    edge = getGridRight(tile.grid)
    if (edgeMap[edge] == 2) matches++
    edge = edge.split('').reverse().join('')
    if (edgeMap[edge] == 2) matches++
    edge = getGridTop(tile.grid)
    if (edgeMap[edge] == 2) matches++
    edge = edge.split('').reverse().join('')
    if (edgeMap[edge] == 2) matches++
    edge = getGridBottom(tile.grid)
    if (edgeMap[edge] == 2) matches++
    edge = edge.split('').reverse().join('')
    if (edgeMap[edge] == 2) matches++

    if (matches == 4) total *= tile.tileId
})

console.log("Part 1:", total)