const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n').map(x => x.split('\n'))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

const transformGrid = (grid, numRotations, flip) => {

    let newGrid = grid.map(row => row.slice(0))
    if (flip) { newGrid.reverse() }

    let otherGrid = newGrid.map(row => row.slice(0))
    for (let count = 0; count < numRotations; count++) {
        for (let y = 0; y < grid.length; y++) {
            for (let x = 0; x < grid[0].length; x++) {
                newGrid[y][x] = otherGrid[grid.length - 1 - x][y]
            }
        }
        otherGrid = newGrid.map(row => row.slice(0))
    }


    return newGrid
}

const edgeMap = {}

const tiles = Object.fromEntries(input.map(section => {
    const tileId = parseInt(section[0].substring(5))

    return [
        tileId,
        section.slice(1).map(line => line.split(''))
    ]
}))

const getGridTop = (grid) => { return grid[0].join('') }
const getGridBottom = (grid) => { return grid[9].slice(0).reverse().join('') }
const getGridLeft = (grid) => { return grid.slice(0).reverse().map(row => row[0]).join('') }
const getGridRight = (grid) => { return grid.map(row => row[9]).join('') }

const directions = {
    "Top": 0,
    "Left": 1,
    "Bottom": 2,
    "Right": 3
}

const edgeFunctions = {
    0: getGridTop,
    1: getGridLeft,
    2: getGridBottom,
    3: getGridRight
}

const getGridEdge = (metaData, direction) => {

    const grid = tiles[metaData.id]
    direction = directions[direction] + metaData.rotation
    direction = direction % 4

    let reverse = false

    if (metaData.flip == 'vertical' && direction % 2 == 0) {
        direction += 2
        direction = direction % 4
    }

    if (metaData.flip == 'vertical') reverse = true

    let edge = edgeFunctions[direction](grid)
    if (reverse) edge = edge.split('').reverse().join('')

    return edge
}



const addEdgeToMap = (edge, tileId) => {
    const reversedEdge = edge.split('').reverse().join('')

    if (edgeMap[edge]) edgeMap[edge].push(tileId)
    else if (edgeMap[reversedEdge]) edgeMap[reversedEdge].push(tileId)
    else edgeMap[edge] = [tileId]
}


const edgeHasMatch = (edge) => {
    return (edgeMap[edge] || edgeMap[edge.split('').reverse().join('')]).length == 2
}

Object.entries(tiles).forEach(([tileId, grid]) => {
    let edge
    edge = getGridLeft(grid)
    addEdgeToMap(edge, tileId)
    edge = getGridRight(grid)
    addEdgeToMap(edge, tileId)
    edge = getGridTop(grid)
    addEdgeToMap(edge, tileId)
    edge = getGridBottom(grid)
    addEdgeToMap(edge, tileId)
})

const getNeighbor = (edge, tileId) => {
    const reversedEdge = edge.split('').reverse().join('')

    const tiles = edgeMap[edge] || edgeMap[reversedEdge]

    return tiles.filter(id => id != tileId)[0]
}

// const showNeighbors = (metaData) => {
//     //const grid = tiles[tileId]

//     console.log("Left Neighbor ID is:", getNeighbor(getGridEdge(metaData, "Left"), metaData.id))
//     console.log("Right Neighbor ID is:", getNeighbor(getGridEdge(metaData, "Right"), metaData.id))
//     console.log("Top Neighbor ID is:", getNeighbor(getGridEdge(metaData, "Top"), metaData.id))
//     console.log("Bottom Neighbor ID is:", getNeighbor(getGridEdge(metaData, "Bottom"), metaData.id))

//     console.log()
// }



const cornerTiles = []

Object.entries(tiles).forEach(([tileId, grid]) => {
    let matches = 0
    let edge
    edge = getGridLeft(grid)
    if (edgeHasMatch(edge)) matches++
    edge = getGridRight(grid)
    if (edgeHasMatch(edge)) matches++
    edge = getGridTop(grid)
    if (edgeHasMatch(edge)) matches++
    edge = getGridBottom(grid)
    if (edgeHasMatch(edge)) matches++

    if (matches == 2)  { /// is a corner
        cornerTiles.push(tileId)
    }
})


const findCorrectState = (parent, neighbor) => {
    for (let i = 0; i < 4; i++) {
        tileStateMap[neighbor.y][neighbor.x].rotation = i
        const rightEdge = getGridEdge(tileStateMap[parent.y][parent.x], "Right")
        const leftEdge = getGridEdge(tileStateMap[neighbor.y][neighbor.x], "Left")
        if (rightEdge == leftEdge.split('').reverse().join('')) return
    }
    
    tileStateMap[neighbor.y][neighbor.x].flip = "vertical"
    
    for (let i = 0; i < 4; i++) {
        tileStateMap[neighbor.y][neighbor.x].rotation = i
        const rightEdge = getGridEdge(tileStateMap[parent.y][parent.x], "Right")
        const leftEdge = getGridEdge(tileStateMap[neighbor.y][neighbor.x], "Left")
        if (rightEdge == leftEdge.split('').reverse().join('')) return
    }
}

const findCorrectStateVertical = (parent, neighbor) => {
    for (let i = 0; i < 4; i++) {
        //console.log("setting rotation to ", i)
        tileStateMap[neighbor.y][neighbor.x].rotation = i
        const topEdge = getGridEdge(tileStateMap[neighbor.y][neighbor.x], "Top")
        const bottomEdge = getGridEdge(tileStateMap[parent.y][parent.x], "Bottom")
        if (topEdge == bottomEdge.split('').reverse().join('')) return
    }
    
    tileStateMap[neighbor.y][neighbor.x].flip = "vertical"
    
    for (let i = 0; i < 4; i++) {
        //console.log("setting rotation to ", i)
        tileStateMap[neighbor.y][neighbor.x].rotation = i
        const topEdge = getGridEdge(tileStateMap[neighbor.y][neighbor.x], "Top")
        const bottomEdge = getGridEdge(tileStateMap[parent.y][parent.x], "Bottom")
        if (topEdge == bottomEdge.split('').reverse().join('')) return
    }
}


/// sample index 1   - rotation 0 flip vertical
/// input index 0 - - rotation 1 flip vertical

const startTile = cornerTiles[0]
console.log("Start Tile: ", startTile)

const tileSize = tiles[startTile].length

const tileStateMap = { 0: { 0: { id: startTile, rotation: 3, flip: "vertical" } } }

const findRight = (parent) => {
    while (true) {
        const metaData = tileStateMap[parent.y][parent.x]
        const rightTile = getNeighbor(getGridEdge(metaData, "Right"), metaData.id)
        if (rightTile == undefined) break
        tileStateMap[parent.y][parent.x + 1] = { id: rightTile }
        findCorrectState(parent, { x: parent.x + 1, y: parent.y })
        parent.x++
    }
}

let parent = { x: 0, y: 0 }
while (true) {
    findRight({...parent})
    const metaData = tileStateMap[parent.y][parent.x]
    const bottomTile = getNeighbor(getGridEdge(metaData, "Bottom"), metaData.id)
    if (bottomTile == undefined) break
    tileStateMap[parent.y + 1] = { 0: { id: bottomTile } }
    findCorrectStateVertical(parent, { x: parent.x, y: parent.y + 1 })
    parent.y++
    
}

console.log(tileStateMap)

const seaMonsterString = `
                  # 
#    ##    ##    ###
 #  #  #  #  #  #   
`
const seaMonsterGrid = seaMonsterString.split('\n').map(x => x.split('')).slice(1,4)

const tileGridSize = Math.sqrt(input.length)
const combinedGridSize = (tileSize - 2) * tileGridSize
const combinedGrid = new Array(combinedGridSize).fill(0).map(() => new Array(combinedGridSize).fill('.'))
let xOffset = 0, yOffset = 0

let total = 0

for (let y = 0; y < tileGridSize; y++) {
    for (let x = 0; x < tileGridSize; x++) {
        const tile = tileStateMap[y][x]
        const grid = transformGrid(tiles[tile.id], tile.rotation, tile.flip)
        for (let y_ = 1; y_ < tileSize - 1; y_++) {
            for (let x_ = 1; x_ < tileSize - 1; x_++) {
                combinedGrid[yOffset + y_ - 1][xOffset + x_ - 1] = grid[y_][x_]
                if (grid[y_][x_] == '#') total++
            }
        }
        xOffset += (tileSize - 2)
    }
    xOffset = 0
    yOffset += (tileSize - 2)
}

console.log("Total #s",total)

const findSeaMonsters = (grid) => {
    let numSeaMonsters = 0
    for (let y = 0; y <= combinedGridSize - seaMonsterGrid.length; y++) {
        for (let x = 0; x <= combinedGridSize - seaMonsterGrid[0].length; x++) {
            let isSeaMonster = true
            for (let y_s = 0; y_s < seaMonsterGrid.length; y_s++) {
                for (let x_s = 0; x_s < seaMonsterGrid[0].length; x_s++) {
                    if (seaMonsterGrid[y_s][x_s] == '#' && grid[y + y_s][x + x_s] != '#') isSeaMonster = false
                }
            }
            if (isSeaMonster) numSeaMonsters++
        }
    }
    return numSeaMonsters
}

const searchGrid = transformGrid(combinedGrid, 0, false)

for (let i = 0; i < 4; i++) {
    const searchGrid = transformGrid(combinedGrid, i, false)
    console.log(findSeaMonsters(searchGrid))
}

for (let i = 0; i < 4; i++) {
    const searchGrid = transformGrid(combinedGrid, i, true)
    console.log(findSeaMonsters(searchGrid))
}

//printGrid(transformGrid(combinedGrid, 0, false))

console.log("Answer:", total - (15*43))
