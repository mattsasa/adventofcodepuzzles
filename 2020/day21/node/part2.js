const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n')

Array.prototype.addIfNotExists = function (item) { if (!this.includes(item)) this.push(item) }
Array.prototype.removeIntersectionWith = function (otherlist) { return this.filter(item => otherlist.includes(item)) }

let allIngredients = []
const allAllergens = {}

const list = input.map(line => {
    const parts = line.split(' (contains ')
    const ingredients = parts[0].split(' ')
    ingredients.forEach(item => allIngredients.addIfNotExists(item) )
    const allergens = parts[1].slice(0, parts[1].length - 1).split(', ')

    allergens.forEach(allergen => {
        if (allAllergens[allergen] == undefined) allAllergens[allergen] = []
        ingredients.forEach(ingredient => allAllergens[allergen].addIfNotExists(ingredient))
    })

    return { ingredients, allergens }
})

Object.keys(allAllergens).forEach(allergen => {
    list.forEach(line => {
        if (!line.allergens.includes(allergen)) return
        allAllergens[allergen] = allAllergens[allergen].removeIntersectionWith(line.ingredients)
    })
    allAllergens[allergen].forEach(ingredient => {
        allIngredients = allIngredients.filter(item => item != ingredient)
    })
})

const allMatchesFound = () => {
    return Object.values(allAllergens).reduce((allFound, candidates) => allFound && candidates.length <= 1, true)
}

while (!allMatchesFound()) {
    Object.entries(allAllergens).forEach(([allergen, candidates]) => {
        if (candidates.length == 1) {
            const matchedItem = candidates[0]
            const matchedAllergen = allergen
            Object.entries(allAllergens).forEach(([allergen, candidates]) => {
                if (allergen == matchedAllergen) return
                allAllergens[allergen] = candidates.filter(item => item != matchedItem)
            })
        }
    })
}

const sortedAllergens = Object.entries(allAllergens).sort((a, b) => a[0].localeCompare(b[0])).map(([_, candidates]) => candidates[0])

console.log(sortedAllergens.join(','))