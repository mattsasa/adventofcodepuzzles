const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n').map(x => x.split('\n'))
const deck1 = input[0].slice(1).map(x => parseInt(x))
const deck2 = input[1].slice(1).map(x => parseInt(x))

const gameOver = (deck1, deck2) => { return deck1.length == 0 || deck2.length == 0 }

const shouldRecurse = (top1, top2, deck1, deck2) => { return top1 <= deck1.length && top2 <= deck2.length }

const playGame = (deck1, deck2) => {

    const previousStates = {}

    while (!gameOver(deck1, deck2)) {

        const roundKey = JSON.stringify([deck1, deck2])
        if (previousStates[roundKey]) return true
        previousStates[roundKey] = 1

        const top1 = deck1.shift(), top2 = deck2.shift()

        let deck1Won = top1 > top2

        if (shouldRecurse(top1, top2, deck1, deck2)) {
            deck1Won = playGame(deck1.slice(0, top1), deck2.slice(0, top2))
        }

        if (deck1Won) { deck1.push(top1); deck1.push(top2) }
        else { deck2.push(top2); deck2.push(top1) }

    }

    return deck2.length == 0
}

playGame(deck1, deck2)

const score = deck2.reduce((accum, value, i) => accum + value * (deck2.length - i), 0)
console.log("Winning Score", score)