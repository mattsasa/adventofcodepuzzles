const cupsMap = new Array(1000000)
const cups = "523764819".split('').map(x => parseInt(x))
for (let i = 10; i <= 1000000; i++) { cups.push(i) }

let currentCup = { value: cups[0] }
let tempCup = { value: cups[1], next: currentCup }
cupsMap[currentCup.value] = currentCup
cupsMap[tempCup.value] = tempCup
currentCup.next = tempCup

const insert = (value) => {
    const newCup = { value, next: currentCup }
    cupsMap[newCup.value] = newCup
    tempCup.next = newCup
    tempCup = newCup
}

for (let i = 2; i < cups.length; i++) { insert(cups[i]) }

const doMove = () => {

    let destinationValue = currentCup.value - 1
    if (destinationValue == 0) destinationValue = 1000000

    const heldCups = currentCup.next
    const heldCupValues = [ heldCups.value, heldCups.next.value, heldCups.next.next.value ]

    currentCup.next = currentCup.next.next.next.next
    
    while (heldCupValues.includes(destinationValue)) {
        destinationValue--
        if (destinationValue == 0) destinationValue = 1000000
    }
    const destinationCup = cupsMap[destinationValue]

    heldCups.next.next.next = destinationCup.next
    destinationCup.next = heldCups

    currentCup = currentCup.next

}

for (let i = 0; i < 10000000; i++) { doMove() }

currentCup = cupsMap[1].next

console.log("Answer:", currentCup.value * currentCup.next.value)
