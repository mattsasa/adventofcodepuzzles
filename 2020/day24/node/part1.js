const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n')

const tileCommands = input.map(line => {
    const list = []
    for (let i = 0; i < line.length; i++) {
        if (line[i] != 'e' && line[i] != 'w') {
            list.push(line.substring(i, i + 2))
            i++
        }
        else list.push(line[i])
    }
    return list
})

let tileMap = new Array(210).fill(0).map(() => new Array(210).fill(0).map(() => new Array(210).fill(0)))

const calcTileMapSize = () => {
    let count = 0
    for (let x = 0; x < 210; x++) {
        for (let y = 0; y < 210; y++) {
            for (let z = 0; z < 210; z++) {
                if (tileMap[x][y][z] == 1) count++
            }
        }
    }
    return count
}


tileCommands.forEach(command => {
    const coords = [105,105,105]  /// topLeft  topRight  bottom
    command.forEach(step => {
        switch (step) {
            case 'e': { coords[0]--; coords[2]++; break }
            case 'w': { coords[0]++; coords[2]--; break }
            case 'se': { coords[0]--; coords[1]++; break }
            case 'nw': { coords[0]++; coords[1]--; break }
            case 'ne': { coords[1]--; coords[2]++; break }
            case 'sw': { coords[1]++; coords[2]--; break }
            default: console.log("ERROR!!"); proccess.exit()
        }
    })
    if (tileMap[coords[0]][coords[1]][coords[2]] == 0) tileMap[coords[0]][coords[1]][coords[2]] = 1
    else tileMap[coords[0]][coords[1]][coords[2]] = 0
})

console.log("Part 1:", calcTileMapSize())

for (let i = 0; i < 100; i++) {
    const newTileMap = new Array(210).fill(0).map(() => new Array(210).fill(0).map(() => new Array(210).fill(0)))

    for (let x = 1; x < 209; x++) {
        for (let y = 1; y < 209; y++) {
            for (let z = 1; z < 209; z++) {

                let numBlackTiles = 0 ///Exist (=1) (White tiles don't exist)
                if (tileMap[x+1][y-1][z]) numBlackTiles++
                if (tileMap[x-1][y+1][z]) numBlackTiles++
                if (tileMap[x][y+1][z-1]) numBlackTiles++
                if (tileMap[x][y-1][z+1]) numBlackTiles++
                if (tileMap[x-1][y][z+1]) numBlackTiles++
                if (tileMap[x+1][y][z-1]) numBlackTiles++

                if (tileMap[x][y][z] == 1) { /// is a black tile
                    if (numBlackTiles == 0 || numBlackTiles > 2) { } /// flip to white do nothing
                    else newTileMap[x][y][z] = 1 //stays black
                } else { /// is a white tile
                    if (numBlackTiles == 2) newTileMap[x][y][z] = 1 /// flips to black
                }

            }
        }
    }
    tileMap = newTileMap

}

console.log("Part 2:", calcTileMapSize())

