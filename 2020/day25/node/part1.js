const subNumber = 7
const cardPubKey = 7573546
const doorPubKey = 17786549

const findLoopSize = (pubKey, subNumber) => {
    let value = 1
    let loop_size = 0
    while (value != pubKey) {
        value *= subNumber
        value %= 20201227
        loop_size++
    }

    return loop_size
    
}

const findEncryptionKey = (loop_size, subNumber) => {
    let value = 1
    for (let i = 0; i < loop_size; i++) {
        value *= subNumber
        value %= 20201227
    }
    return value
}


console.log(findEncryptionKey(10985209, 17786549))

/// card loop size is 10985209
