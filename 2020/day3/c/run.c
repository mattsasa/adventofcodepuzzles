#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int height = 0;
int width = 0;

int countTreesForSlope(char** grid, int yStep, float xStep) {
    int treeCount = 0;
    for (int y = 0; y < height; y += yStep) {
        int fullX = (y * xStep);
        int closeX = fullX % width;
        if (grid[y][closeX] == '#') treeCount++;
    }
    return treeCount;
}

int main(void) {

    FILE *fptr = fopen("../data/input.txt", "r");

    int array_size = 100;

    char** grid = malloc(array_size*sizeof(char*));

    char input_str[50];
    char *x = fgets(input_str, sizeof(input_str), fptr);
    while (x != NULL) {
        width = strlen(input_str) - 1;
        grid[height] = malloc((strlen(input_str)+1)*sizeof(char));
        strcpy(grid[height++], input_str);
        if (height > array_size - 1) {
            array_size += 100;
            grid = realloc(grid, array_size * sizeof(char*));
        }

        x = fgets(input_str, sizeof(input_str), fptr);
    }

    fclose(fptr);

    printf("Part 1: %d \n", countTreesForSlope(grid, 1, 3));
    printf("Part 2: %d \n", countTreesForSlope(grid, 1, 1) * countTreesForSlope(grid, 1, 3) * countTreesForSlope(grid, 1, 5) * countTreesForSlope(grid, 1, 7) * countTreesForSlope(grid, 2, 0.5));

    for (int i = 0; i < height; i++) { free(grid[i]); }
    free(grid);
}