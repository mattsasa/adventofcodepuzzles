#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int height = 0;
int width = 0;
int blockSize = 0;
int numBlocks = 0;

__global__
void countTreesForSlope(char** grid, int* hitTrees, int height, int width, float xStep, bool skipOdds) {
    int y = threadIdx.x;
    if (skipOdds && (y % 2 == 1)) return;

    int fullX = (y * xStep);
    int closeX = fullX % width;
    if (grid[y][closeX] == '#') { hitTrees[y]++; }
}

int countTrees(char** cuda_grid, int* hitTrees, float xStep, bool skipOdds) {
    for (int i = 0; i < height; i++) { hitTrees[i] = 0; }
    countTreesForSlope<<<numBlocks, blockSize>>>(cuda_grid, hitTrees, height, width, xStep, skipOdds);
    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();
    int totalHitTrees = 0;
    for (int i = 0; i < height; i++) { totalHitTrees += hitTrees[i]; }
    return totalHitTrees;
}

int main(void) {

    FILE *fptr = fopen("../data/input.txt", "r");

    int array_size = 100;

    char** grid = (char**) malloc(array_size*sizeof(char*));

    char input_str[50];
    char *x = fgets(input_str, sizeof(input_str), fptr);
    while (x != NULL) {
        width = strlen(input_str) - 1;
        grid[height] = (char *) malloc((strlen(input_str)+1)*sizeof(char));
        strcpy(grid[height++], input_str);
        if (height > array_size - 1) {
            array_size += 100;
            grid = (char**) realloc(grid, array_size * sizeof(char*));
        }

        x = fgets(input_str, sizeof(input_str), fptr);
    }

    fclose(fptr);

    char** cuda_grid;
    cudaMallocManaged(&cuda_grid, height*sizeof(char*));
    for (int i = 0; i < height; i++) {
        cudaMallocManaged(&cuda_grid[i], (strlen(grid[i])+1)*sizeof(char));
        cudaMemcpy(cuda_grid[i], grid[i], (strlen(grid[i])+1)*sizeof(char), cudaMemcpyDefault);
        free(grid[i]);
    }
    free(grid);

    int* hitTrees;
    cudaMallocManaged(&hitTrees, height*sizeof(int));
    for (int i = 0; i < height; i++) { hitTrees[i] = 0; }

    blockSize = 1024;
    numBlocks = ((height) + blockSize - 1) / blockSize;
    if (height < blockSize) blockSize = height;

    if (numBlocks > 2147483647) {
        printf("Too many blocks.... exiting");
        exit(-1);
    }
    printf("Num Blocks: %d   vs height: %d \n", numBlocks, height);


    printf("Part 1: %d \n", countTrees(cuda_grid, hitTrees, 3.0, false));
    printf("Part 2: %d \n", countTrees(cuda_grid, hitTrees, 1.0, false) * countTrees(cuda_grid, hitTrees, 3.0, false) * countTrees(cuda_grid, hitTrees, 5.0, false) * countTrees(cuda_grid, hitTrees, 7.0, false) * countTrees(cuda_grid, hitTrees, 0.5, true));

    for (int i = 0; i < height; i++) { cudaFree(grid[i]); }
    cudaFree(cuda_grid);
    cudaFree(hitTrees);

}