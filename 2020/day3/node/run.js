const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt').toString().split("\n").map(line => line.split(''))

/*const printGrid = () => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char);
        })
        console.log()
    })
}*/

//printGrid()

const height = grid.length
const width = grid[0].length

const countTreesForSlope = (yStep, xStep) => {
    let treeCount = 0
    for (let y = 0; y < height; y+=yStep) {
        const closeX = (y * xStep) % width
        if (grid[y][closeX] == '#') treeCount++
    }
    return treeCount
}

console.log("Part 1 Answer: ", countTreesForSlope(1, 3))
console.log("Part 2 Answer: ", countTreesForSlope(1, 1) * countTreesForSlope(1, 3) * countTreesForSlope(1, 5) * countTreesForSlope(1, 7) * countTreesForSlope(2, 0.5))
