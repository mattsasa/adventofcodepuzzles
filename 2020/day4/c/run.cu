#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

struct Passport {
    int byr;
    int iyr;
    int eyr;
    int hgt;
    bool is_cm;
    char* hcl;
    char* ecl;
    char* pid;
};

unsigned long hash(const char *str) {
    unsigned long hash = 5381;  
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c;
    return hash;
}

#define BYR 193487826
#define IYR 193495449
#define EYR 193491093
#define HGT 193493768
#define HCL 193493628
#define ECL 193490361
#define PID 193502530


void addFieldToStruct(struct Passport* pp, char fieldname[], char* fielddata) {

    int len = 0;
    switch (hash(fieldname)) {
        case BYR:
            pp->byr = atoi(fielddata);
            break;
        case EYR:
            pp->eyr = atoi(fielddata);
            break;
        case IYR:
            pp->iyr = atoi(fielddata);
            break;
        case HGT:
            pp->hgt = atoi(fielddata);
            len = strlen(fielddata);
            pp->is_cm = fielddata[len-2] == 'c';
            break;
        case HCL:
            pp->hcl = fielddata;
            break;
        case ECL:
            pp->ecl = fielddata;
            break;
        case PID:
            pp->pid = fielddata;
            break;
        default:
            break;
    }

}

bool validEyeColor(char* ecl) {

    if (strcmp("amb", ecl) == 0) return true;
    if (strcmp("blu", ecl) == 0) return true;
    if (strcmp("brn", ecl) == 0) return true;
    if (strcmp("gry", ecl) == 0) return true;
    if (strcmp("grn", ecl) == 0) return true;
    if (strcmp("hzl", ecl) == 0) return true;
    if (strcmp("oth", ecl) == 0) return true;

    return false;
}

//__global__
void validPassport(char** batchOfFields, int numFields, int* validPassports, int index) {

    //int index = threadIdx.x;

    struct Passport pp;
    pp.hcl = (char*)""; pp.ecl = (char*)""; pp.pid = (char*)"";
    pp.byr = 0; pp.eyr = 0; pp.iyr = 0; pp.hgt = 0;
    
    for (int i = 0; i < numFields; i++) {
        char* field = batchOfFields[i];
        char fieldname[3];
        strncpy(fieldname, field, 3);
        field += 4;

        addFieldToStruct(&pp, fieldname, field);
    }

    if (pp.hcl == "") return;
    if (pp.ecl == "") return;
    if (pp.pid == "") return;
    if (pp.byr == 0) return;
    if (pp.eyr == 0) return;
    if (pp.iyr == 0) return;
    if (pp.hgt == 0) return;

    if (pp.byr < 1920 || pp.byr > 2002) return;
    if (pp.eyr < 2020 || pp.eyr > 2030) return;
    if (pp.iyr < 2010 || pp.iyr > 2020) return;

    if (pp.is_cm) { if (pp.hgt < 150 || pp.hgt > 193) return; }
    else { if (pp.hgt < 59 || pp.hgt > 76) return; }

    if (strlen(pp.hcl) != 7) return;
    char *s = pp.hcl;
    for (*s++; *s; *s++) { if (*s > 'f' || *s < '0') return; }

    if (!validEyeColor(pp.ecl)) return;

    if (strlen(pp.pid) != 9) return;
    for (char* s = pp.pid; *s; *s++) { if (!isdigit(*s)) return; }

    validPassports[index] = 1;
    //return 1;
}


int main(void) {

    clock_t begin = clock();

    FILE *fptr = fopen("../data/input.txt", "r");

    int count = 0;
    int array_size = 100;
    int numPassports = 1;

    char** input_strings = (char**) malloc(array_size*sizeof(char*));

    char input_str[100];
    char *x = fgets(input_str, sizeof(input_str), fptr);
    while (x != NULL) {

        if (input_str[0] == '\n') numPassports++;

        input_strings[count] = (char*) malloc((strlen(input_str)+1)*sizeof(char));
        strcpy(input_strings[count++], input_str);
        if (count > array_size - 1) {
            array_size += 100;
            input_strings = (char**) realloc(input_strings, array_size * sizeof(char*));
        }
        
        x = fgets(input_str, sizeof(input_str), fptr);
    }

    fclose(fptr);

    char** passport_strings[numPassports];
    int passport_lengths[numPassports];
    int passport_index = 0;
    int current_size = 0;

    for (int i = 0; i < count; i++) {
        char* line = input_strings[i];
        if (line[0] == '\n') { 
            passport_index++;
            current_size = 0;
        }
        else {
            if (current_size == 0) { passport_strings[passport_index] = (char**) malloc(sizeof(char*)); } // first line
            int numEntries = 1;
            for (char* s = line; *s; *s++) { if (*s == ' ') { numEntries++; } }
            int entry_index = current_size;
            current_size += numEntries;
            passport_strings[passport_index] = (char**) realloc(passport_strings[passport_index], current_size * sizeof(char*));
            passport_lengths[passport_index] = current_size;
            char entry_buffer[25];
            int char_index = 0;
            for (char* s = line; *s; *s++) {
                if (*s == ' ' || *s == '\n') {
                    entry_buffer[char_index++] = '\0';
                    passport_strings[passport_index][entry_index] = (char*) malloc(char_index * sizeof(char*));
                    strcpy(passport_strings[passport_index][entry_index], entry_buffer);
                    char_index = 0;
                    entry_index++;
                } 
                else {
                    entry_buffer[char_index++] = *s;
                }
            }
        }
    }

    int* cuda_passport_lengths;
    cudaMallocManaged(&cuda_passport_lengths, numPassports*sizeof(int));

    int* validPassports;
    cudaMallocManaged(&validPassports, numPassports*sizeof(int));
    for (int i = 0; i < numPassports; i++) { 
        validPassports[i] = 0;
        cuda_passport_lengths[i] = passport_lengths[i];
    }

    int blockSize = 1024;
    int numBlocks = ((numPassports) + blockSize - 1) / blockSize;
    if (numPassports < blockSize) blockSize = numPassports;

    if (numBlocks > 2147483647) {
        printf("Too many blocks.... exiting");
        exit(-1);
    }
    printf("Num Blocks: %d   vs numPassports: %d \n", numBlocks, numPassports);

    ///call kernel
    //validPassport<<<numBlocks, blockSize>>>()
    for (int i = 0; i < numPassports; i++) { validPassport(passport_strings[i], passport_lengths[i], validPassports, i); }

    int sum = 0;
    for (int i = 0; i < numPassports; i++) { sum += validPassports[i]; }

    printf("num valid passports: %d \n", sum);

}