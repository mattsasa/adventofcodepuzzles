const fs = require('fs')
const batches = fs.readFileSync('../data/input.txt').toString().split("\n\n").map(b => b.split("\n"))

const requiredFields = [ "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" ]

let validPassports = 0

batches.forEach(batch => {
    const entries = batch.reduce((arr, line) => [...arr, ...line.split(" ")], [])

    const pp = Object.fromEntries(entries.map(entry => [ entry.split(":")[0], entry.split(":")[1] ] ))

    for (field of requiredFields) { if (!Object.keys(pp).includes(field)) return }

    if (parseInt(pp.byr) < 1920 || parseInt(pp.byr) > 2002) return
    if (parseInt(pp.iyr) < 2010 || parseInt(pp.iyr) > 2020) return
    if (parseInt(pp.eyr) < 2020 || parseInt(pp.eyr) > 2030) return

    const suffix = pp.hgt.substring(pp.hgt.length - 2, pp.hgt.length)
    if (!["cm", "in"].includes(suffix)) return

    const height =  pp.hgt.substring(0, pp.hgt.length - 2)
    if (suffix == "cm") { if (parseInt(height) < 150 || parseInt(height) > 193) return }
    else { if (parseInt(height) < 59 || parseInt(height) > 76) return }

    if (pp.hcl.length != 7) return
    for (char of pp.hcl.substring(1, pp.hcl.length)) {
        if (isNaN(char) && !['a', 'b', 'c', 'd', 'e', 'f'].includes(char)) return
    }

    if (![ "amb", "blu", "brn", "gry", "grn", "hzl", "oth" ].includes(pp.ecl)) return

    if (pp.pid.length != 9) return
    for (char of pp.pid) if (isNaN(char)) return

    validPassports++
})

console.log(validPassports)