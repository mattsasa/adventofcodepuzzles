const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const arr = input.map(line => {
    let num = 0
    for (let i = 0; i < 10; i++) {
        if (line[i] == "B" || line[i] == "R") num |= Math.pow(2, 9 - i)
    }
    return num
}).sort((a,b) => { return b - a })

console.log(arr[0])

