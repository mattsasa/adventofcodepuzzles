const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n")

const printGrid = () => {
    grid.forEach((line, i) => {
        process.stdout.write(i.toString() + "  ")
        line.forEach(value => {
            process.stdout.write(value ? "#" : '.');
        })
        console.log()
    })
}

const grid = new Array(128).fill(0).map(() => new Array(8).fill(false))

input.forEach(line => {
    let row = 0
    for (let i = 0; i < 7; i++) { if (line[i] == "B") row |= Math.pow(2, 6 - i) }

    let col = 0
    for (let i = 0; i < 3; i++) { if (line[i+7] == "R") col |= Math.pow(2, 2 - i) }

    grid[row][col] = true
})

printGrid()
