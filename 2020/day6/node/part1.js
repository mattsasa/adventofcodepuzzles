const fs = require('fs')
const batches = fs.readFileSync('../data/input.txt').toString().split("\n\n").map(b => b.split("\n"))

let total = 0

batches.forEach(batch => {

    const entries = batch.reduce((arr, line) => [...arr, ...line.split("")], [])

    const object = {}
    entries.forEach(item => object[item] = 1)

    total += Object.keys(object).length
})

console.log(total)

