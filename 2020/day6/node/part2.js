const fs = require('fs')
const batches = fs.readFileSync('../data/input.txt').toString().split("\n\n").map(b => b.split("\n"))

let total = 0

const questions = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r','s', 't', 'u', 'v', 'w', 'x', 'y','z']

batches.forEach(batch => {

    const entries = batch.map(x => x.split(''))

    questions.forEach(question => {
        let answer = true

        entries.forEach(entry => {
            if (!entry.includes(question)) answer = false
        })

        if (answer) total++

    })
})

console.log(total)

