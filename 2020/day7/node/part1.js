const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => x.split(" "))

const rules = Object.fromEntries(input.map((line) => {
    const pair = [ line.slice(0,2).join("-"), [] ]

    const list = line.slice(4)

    if (list[0] != "no") {
        for (let i = 0; i < list.length; i+=4) {
            pair[1].push({ quantity: list[i], color: list.slice(i+1, i+3).join("-") })
        }
    }
    
    return pair
}))


const leadsToShinyGold = (colorKey) => {
    const contains = rules[colorKey]
    for (let i = 0; i < contains.length; i++) {
        if (contains[i].color == 'shiny-gold' || leadsToShinyGold(contains[i].color)) return true
    }

    return false

}

let validStartColors = 0

Object.keys(rules).forEach((colorKey) => {
    if (leadsToShinyGold(colorKey)) { validStartColors++ }
})

console.log(validStartColors)