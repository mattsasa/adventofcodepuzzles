const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => x.split(" "))

const rules = Object.fromEntries(input.map((line) => {
    const pair = [ line.slice(0,2).join("-"), [] ]

    const list = line.slice(4)

    if (list[0] != "no") {
        for (let i = 0; i < list.length; i+=4) {
            pair[1].push({ quantity: list[i], color: list.slice(i+1, i+3).join("-") })
        }
    }
    
    return pair
}))

//console.log(rules)

const countBags = (bagslist) => {
    return bagslist.reduce((accum, bag) => {
        return accum + (parseInt(bag.quantity) * (1 + countBags(rules[bag.color])))
    }, 0)
}

console.log(countBags(rules['shiny-gold']))
