const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => x.split(' '))

const testInput = (input) => {
    let global = 0
    let pc = 0
    
    let visited = []
    
    while (pc < input.length) {
    
        visited.push(pc)
        const instruction = input[pc]

        switch (instruction[0]) {
            case "acc": global += parseInt(instruction[1]); pc++; break 
            case "jmp": pc += parseInt(instruction[1]); break
            case "nop": pc++; break
        }
    
        if (visited.includes(pc)) return false
    }

    console.log("Terminated correctly!: ", global)

    return true
}

input.forEach((instruction, i) => {
    const moddedInstruction = [...instruction]

    switch (moddedInstruction[0]) {
        case "jmp": moddedInstruction[0] = "nop"; break
        case "nop": moddedInstruction[0] = "jmp"; break
        case "acc": return
    }

    const moddedInput = [
        ...input.slice(0,i),
        moddedInstruction,
        ...input.slice(i+1, input.length)
    ]

    testInput(moddedInput)
})
