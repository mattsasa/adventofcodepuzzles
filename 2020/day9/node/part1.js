const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => parseInt(x))

const isValid = (index, target) => {
    for (let i = index - 25; i < index; i++) {
        for (let j = index - 25; j < index; j++) {
            if (i != j) {
                if (input[i] + input[j] == target) return true
            }
        }
    }
    console.log(target)
    return false
}

for (let i = 25; i < input.length; i++) { if (!isValid(i, input[i])) return }