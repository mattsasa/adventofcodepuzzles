const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split("\n").map(x => parseInt(x))

const target = 57195069   /// answer from part 1

const findMinMax = (arr) => {
    arr.sort((a, b) => { return a - b })
    console.log(arr[0] + arr[arr.length-1])
}

const isValid = (start) => {

    let i = start

    let count = input[i]
    while (count < target) { count += input[++i] }
    if (count == target) {
        findMinMax(input.slice(start, i+1))
        return true
    }

    return false
}

for (let i = 0; i < input.length; i++) { if (isValid(i)) return }

