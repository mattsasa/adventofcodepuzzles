#include <napi.h>
#include <string>
#include <iostream>

void process(const Napi::CallbackInfo& info) {
    Napi::Array napi_arr = info[0].As<Napi::Array>();
    std::vector<int> arr = std::vector<int>(napi_arr.Length());
    for (size_t i = 0; i < napi_arr.Length(); i++) { Napi::Value item = napi_arr[i]; arr[i] = item.ToNumber(); }

    int count = 0;
    for (size_t i = 1; i < arr.size(); i++) { if (arr[i] - arr[i-1] > 0) count++; }
    std::cout << "C++ part 1: " << count << std::endl;

    std::vector<int> sums;
    count = 0;
    for (size_t i = 2; i < arr.size(); i++) { sums.push_back(arr[i] + arr[i-1] + arr[i-2]); }
    for (size_t i = 1; i < sums.size(); i++) { if (sums[i] - sums[i-1] > 0) count++; }
    std::cout << "C++ part 2: " << count << std::endl;

}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);