const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => parseInt(x))

///Process C++
require('./build/Release/day1.node').process(lines)