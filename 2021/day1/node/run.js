const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => parseInt(x))

let count = 0
for (let i = 1; i < lines.length; i++) { if (lines[i] - lines[i-1] > 0) count++ }

console.log(count)