const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => parseInt(x))

let sums = [], count = 0

for (let i = 2; i < lines.length; i++) { sums.push(lines[i] + lines[i-1] + lines[i-2]) }
for (let i = 1; i < sums.length; i++) { if (sums[i] - sums[i-1] > 0) count++ }

console.log("Part 2:", count)