const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n')

const errorTable = { ')': 3, ']': 57, '}': 1197, '>': 25137 }
const completionTable = { '(': 1, '[': 2, '{': 3, '<': 4 }

let error_score = 0

const completion_scores = []

lines.forEach(line => {
    const openChars = []
    for (char of line) {
        if (['(', '[', '{', '<'].includes(char)) openChars.push(char)
        else {
            const lastOpenChar = openChars[openChars.length - 1].charCodeAt()
            const isMatch = lastOpenChar + 1 == char.charCodeAt() || lastOpenChar + 2 == char.charCodeAt()
            if (isMatch) openChars.splice(-1, 1)
            else { error_score += errorTable[char]; return }
        }
    }
    if (openChars.length > 0) {
        completion_scores.push(openChars.reverse().reduce((score, char) => (score * 5) + completionTable[char], 0))
    }
})

completion_scores.sort((a, b) => a > b ? -1 : 1)

console.log("Part 1:", error_score)
console.log("Part 2:", completion_scores[Math.floor(completion_scores.length / 2)])