const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split('').map(x => parseInt(x)))


let flashesThisStep = {}

let total_flashes = 0

// const printGrid = () => {
//     for (let y = 0; y < grid.length; y++){
//         for (let x = 0; x < grid[y].length; x++) {
//             process.stdout.write(grid[y][x].toString())
//         }
//         console.log()
//     }
//     console.log()
// }

const increaseEnergy = () => {
    for (let y = 0; y < grid.length; y++){
        for (let x = 0; x < grid[y].length; x++) {
            grid[y][x]++
        }
    }
}

const getFlashes = () => {
    const flashes = []
    for (let y = 0; y < grid.length; y++){
        for (let x = 0; x < grid[y].length; x++) {
            if (grid[y][x] > 9 && !flashesThisStep[[x, y]]) { flashes.push({ x, y }); flashesThisStep[[x, y]] = 1 }
        }
    }
    total_flashes += flashes.length
    return flashes
}

const doFlash = (x, y) => {
    if (y+1 < grid.length) grid[y+1][x]++
    if (y-1 >= 0) grid[y-1][x]++
    if (x+1 < grid[0].length)  grid[y][x+1]++
    if (x-1 >= 0) grid[y][x-1]++

    if (y+1 < grid.length && x+1 < grid[0].length) grid[y+1][x+1]++
    if (y-1 >= 0 && x+1 < grid[0].length) grid[y-1][x+1]++
    if (y+1 < grid.length && x-1 >= 0) grid[y+1][x-1]++
    if (y-1 >= 0 && x-1 >= 0) grid[y-1][x-1]++

}

for (let steps = 0; steps < 100; steps++) {

    flashesThisStep = {}

    increaseEnergy()

    let flashes = getFlashes()
    while (flashes.length > 0) {
        flashes.forEach(flash => doFlash(flash.x, flash.y))
        flashes = getFlashes()
    }

    Object.keys(flashesThisStep).forEach(pair => { grid[pair[2]][pair[0]] = 0 })
}

console.log("Part 1 - Total Flashes:", total_flashes)