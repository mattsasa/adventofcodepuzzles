const fs = require('fs')
const connects = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split('-'))

const completed_paths = []

const iterateBFS = (path, ends) => {
    const current_cave = path[path.length-1]
    connects.filter(pair => pair[0] == current_cave || pair[1] == current_cave).map(pair => pair[0] == current_cave ? pair[1] : pair[0]).forEach(destination => {

        if (destination == "end") completed_paths.push({ path: [...path, destination] })
        else if (destination != destination.toLowerCase() || !path.includes(destination)) {
            ends.push({ path: [...path, destination] })
        } 
        
    })
}


let ends = [{ path: ["start"] }]

while (ends.length != 0) {
    let new_ends = []
    ends.forEach(end => { iterateBFS(end.path, new_ends) })
    ends = new_ends
}

console.log("Part 1:", completed_paths.length)


