const fs = require('fs')
const connects = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split('-'))

const completed_paths = []

const allowedToVisit = (pathObj, destination) => {
    if (destination == "start") return false
    if (destination != destination.toLowerCase()) return pathObj
    const numOccurences = pathObj.path.reduce((accum, cave) => cave == destination ? accum + 1 : accum, 0)
    if (numOccurences == 0) return pathObj
    if (numOccurences == 1 && !pathObj.tainted) { return { path: pathObj.path, tainted: true } }
}

const iterateBFS = (pathObj, paths) => {
    const path = pathObj.path
    const current_cave = path[path.length-1]
    connects.filter(pair => pair[0] == current_cave || pair[1] == current_cave).map(pair => pair[0] == current_cave ? pair[1] : pair[0]).forEach(destination => {

        if (destination == "end") completed_paths.push({ path: [...path, destination] })
        else {
            const result = allowedToVisit(pathObj, destination)
            if (result) paths.push({ path: [...path, destination], tainted: result.tainted  })
        }

    })
}


let paths = [{ path: ["start"] }]

while (paths.length != 0) {
    let new_paths = []
    paths.forEach(path => { iterateBFS(path, new_paths) })
    paths = new_paths
}

console.log("Part 2:", completed_paths.length)


