const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n')

const dots = input[0].split('\n').map(line => line.split(',').map(x => parseInt(x)))
const folds = input[1].split('\n').map(line => line.slice(11)).map(line => line.split('=')).map(x => [x[0], parseInt(x[1])])

const dims = dots.reduce((output, dot) => {
    if (dot[0] > output[0]) output[0] = dot[0]
    if (dot[1] > output[1]) output[1] = dot[1]
    return output
}, [0,0])


const grid = new Array(dims[1]+1).fill(0).map(() => new Array(dims[0]+1).fill('.'))

// const printGrid = () => {
//     for (let y = 0; y < grid.length; y++){
//         for (let x = 0; x < grid[0].length; x++) {
//             process.stdout.write(grid[y][x].toString())
//         }
//         console.log()
//     }
// }

const performYFold = (y_fold) => {
    let count = 0
    for (let y = y_fold; y < grid.length; y++){
        const diff = y - y_fold;
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] == '#') grid[y_fold - diff][x] = '#'
            if (grid[y_fold - diff][x] == '#') count++
        }
    }
    return count
}

const performXFold = (x_fold) => {
    let count = 0
    for (let y = 0; y < grid.length; y++){
        for (let x = 0; x < x_fold; x++) {
            const diff = x_fold - x;
            if (grid[y][x] == '#') grid[y][x_fold + diff] = '#'
            if (grid[y][x_fold + diff] == '#') count++
        }
    }

    return count
}


dots.forEach(dot => grid[dot[1]][dot[0]] = '#')
const dots_after_first_fold = folds[0][0] == 'x' ? performXFold(folds[0][1]) : performYFold(folds[0][1])

// printGrid()

console.log("Part 1:", dots_after_first_fold)
