const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n')

const dots = input[0].split('\n').map(line => line.split(',').map(x => parseInt(x)))
const folds = input[1].split('\n').map(line => line.slice(11)).map(line => line.split('=')).map(x => [x[0], parseInt(x[1])])

const dims = dots.reduce((output, dot) => {
    if (dot[0] > output[0]) output[0] = dot[0]
    if (dot[1] > output[1]) output[1] = dot[1]
    return output
}, [0,0])

let grid = new Array(dims[1]+1).fill(0).map(() => new Array(dims[0]+1).fill('.'))

const printGrid = () => {
    for (let y = 0; y < grid.length; y++){
        for (let x = 0; x < grid[0].length; x++) {
            process.stdout.write(grid[y][x].toString())
        }
        console.log()
    }
    console.log()
    for (let y = 0; y < grid.length; y++){
        for (let x = grid[0].length-1; x >= 0; x--) {
            process.stdout.write(grid[y][x].toString())
        }
        console.log()
    }
}

const performYFold = (y_fold) => {
    const shift = y_fold < grid.length - 1 - y_fold ? grid.length - 1 - (y_fold*2) : 0
    const new_height = grid.length - Math.min(y_fold, grid.length - 1 - y_fold) - 1
    const new_grid = new Array(new_height).fill(0).map(() => new Array(grid[0].length).fill('.'))

    for (let y = y_fold + 1; y < grid.length; y++){
        const diff = y - y_fold;
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] == '#') new_grid[y_fold - diff + shift][x] = '#'
        }
    }
    for (let y = 0; y < y_fold; y++) {
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] == '#') new_grid[y + shift][x] = '#'
        }
    }
    grid = new_grid
}

const performXFold = (x_fold) => {
    const shift = x_fold + 1
    const new_width = grid[0].length - Math.min(x_fold, grid[0].length - 1 - x_fold) - 1
    const new_grid = new Array(grid.length).fill(0).map(() => new Array(new_width).fill('.'))
    for (let y = 0; y < grid.length; y++){
        for (let x = 0; x < x_fold; x++) {
            const diff = x_fold - x;
            if (grid[y][x] == '#') new_grid[y][x_fold + diff - shift] = '#'
        }
    }
    for (let y = 0; y < grid.length; y++){
        for (let x = x_fold + 1; x < grid[0].length; x++) {
            const diff = x_fold - x;
            if (grid[y][x] == '#') new_grid[y][x - shift] = '#'
        }
    }
    grid = new_grid

}

dots.forEach(dot => grid[dot[1]][dot[0]] = '#')
folds.forEach(fold => fold[0] == 'x' ? performXFold(fold[1]) : performYFold(fold[1]))
printGrid()

