const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n')

let poly_template = lines[0]
const rules = lines.slice(2).map(x => x.split(' -> '))

const poly_pairs = { }

for (let i = 0; i < poly_template.length - 1; i++) {
    const key = poly_template[i] + poly_template[i+1]
    poly_pairs[key] ? poly_pairs[key]++ : poly_pairs[key] = 1
}


for (let steps = 0 ; steps < 40; steps++) {  // 10 steps for part 1
    const temp_pairs = {...poly_pairs}
    rules.forEach(rule => {
        if (temp_pairs[rule[0]]) {
            poly_pairs[rule[0]] -= temp_pairs[rule[0]]
            const first = rule[0][0] + rule[1]
            const second = rule[1] + rule[0][1]
            if (poly_pairs[first] == undefined) poly_pairs[first] = 0
            if (poly_pairs[second] == undefined) poly_pairs[second] = 0
            poly_pairs[first] += temp_pairs[rule[0]]
            poly_pairs[second] += temp_pairs[rule[0]]
        }
    })
} 

const elements = [...new Set(rules.map(rule => rule[1]))] 
const values = elements.map(element => Object.entries(poly_pairs).reduce((sum, [k, v]) => k[1] == element ? sum + v : sum, 0))
const sortedValues = values.sort((a, b) => a > b ? -1 : 1)
console.log("Answer: ", sortedValues[0] - sortedValues[sortedValues.length - 1])




