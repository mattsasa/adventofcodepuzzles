const fs = require('fs')
let riskGrid = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split('').map(x => parseInt(x)))

let gridSize = riskGrid.length

const megaRiskGrid = new Array(gridSize*5).fill(0).map(() => new Array(gridSize*5).fill(0))

for (let tile_w = 0; tile_w < 5; tile_w++) {
    for (let tile_h = 0; tile_h < 5; tile_h++) {
        for (let h = 0; h < gridSize; h++) {
            for (let w = 0; w < gridSize; w++) {
                const newvalue = riskGrid[h][w] + tile_h + tile_w
                megaRiskGrid[h + tile_h*gridSize][w + tile_w*gridSize] = newvalue > 9 ? newvalue - 9 : newvalue
            }
        }
    }
}

gridSize *= 5

riskGrid = megaRiskGrid


const lowestCostGrid = new Array(gridSize).fill(0).map(() => new Array(gridSize).fill(9999))

const printGrid = (grid) => {
    for (let y = 0; y < grid.length; y++){
        for (let x = 0; x < grid[y].length; x++) {
            process.stdout.write(grid[y][x].toString())
        }
        console.log()
    }
    console.log()
}

lowestCostGrid[0][0] = 0

let ends = [{ location: [0,0], currentCost: 0 }]

const iterateBFS = (end, new_ends) => {
    let h = end.location[0], w = end.location[1]
    if (h+1 < riskGrid.length && lowestCostGrid[h+1][w] > end.currentCost + riskGrid[h+1][w] ) { // down
        const new_end = {}
        new_end.location = [h+1, w]
        new_end.currentCost = end.currentCost + riskGrid[h+1][w]
        lowestCostGrid[h+1][w] = end.currentCost + riskGrid[h+1][w]
        new_ends.push(new_end)
    }
    if (h-1 >= 0 && lowestCostGrid[h-1][w] > end.currentCost + riskGrid[h-1][w] ) { // down
        const new_end = {}
        new_end.location = [h-1, w]
        new_end.currentCost = end.currentCost + riskGrid[h-1][w]
        lowestCostGrid[h-1][w] = end.currentCost + riskGrid[h-1][w]
        new_ends.push(new_end)
    }
    if (w+1 < riskGrid.length && lowestCostGrid[h][w+1] > end.currentCost + riskGrid[h][w+1] ) { // down
        const new_end = {}
        new_end.location = [h, w+1]
        new_end.currentCost = end.currentCost + riskGrid[h][w+1]
        lowestCostGrid[h][w+1] = end.currentCost + riskGrid[h][w+1]
        new_ends.push(new_end)
    }
    if (w-1 < riskGrid.length && lowestCostGrid[h][w-1] > end.currentCost + riskGrid[h][w-1] ) { // down
        const new_end = {}
        new_end.location = [h, w-1]
        new_end.currentCost = end.currentCost + riskGrid[h][w-1]
        lowestCostGrid[h][w-1] = end.currentCost + riskGrid[h][w-1]
        new_ends.push(new_end)
    }
}


while (ends.length != 0) {
    let new_ends = []
    ends.forEach(end => { iterateBFS(end, new_ends) })
    // if (lowestCostGrid[gridSize - 1][gridSize - 1] != 9999) console.log("BREAK!")
    ends = new_ends
}

console.log("Part 2:", lowestCostGrid[gridSize - 1][gridSize - 1])

