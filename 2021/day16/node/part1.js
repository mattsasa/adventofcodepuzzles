const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString()

const hex2bin = hex => {
    let binString = ""
    for (char of hex) {
        binString += parseInt(char, 16).toString(2).padStart(4, '0')
    }
    return binString
}

const bin = hex2bin(input)

let version_sum = 0

const process_packet = (packet) => {
    const version = parseInt(packet.substr(0, 3), 2)
    version_sum += version
    const type = parseInt(packet.substr(3, 3), 2)
    if (type != 4) { /// operator packet
        const length_type = parseInt(packet[6])
        if (length_type == 1) {  /// 11 bits represent number of sub-packets
            const num_packets = parseInt(packet.substr(7, 11), 2)
            let remainingStr = packet.substr(18)

            for (let p = 0; p < num_packets; p++) {
                let p_length = process_packet(remainingStr)
                remainingStr = remainingStr.substr(p_length)
            }

            return packet.length - remainingStr.length
        } else {   ///15 bits represent total length in bits
            let counter = 0
            const total_length = parseInt(packet.substr(7, 15), 2)
            let remainingStr = packet.substr(22)
            while (1) {
                if (counter == total_length) break
                let p_length = process_packet(remainingStr)
                counter += p_length
                remainingStr = remainingStr.substr(p_length)
            }

            return total_length + 7 + 15
        }
    } else {  /// literal value packet
        const remainingStr = packet.substr(6)
        let index = 0
        let literal_value = ""
        while (remainingStr[index] == '1') {
            literal_value += remainingStr.substr(index+1, 4)
            index += 5
        }
        literal_value += remainingStr.substr(index+1, 4)
        return index + 6 + 5
    }
}

process_packet(bin)

console.log("Part 1: ", version_sum)
