const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString()

const hex2bin = hex => {
    let binString = ""
    for (char of hex) {
        binString += parseInt(char, 16).toString(2).padStart(4, '0')
    }
    return binString
}

const bin = hex2bin(input)

const process_packet = (packet) => {

    const type = parseInt(packet.substr(3, 3), 2)
    if (type != 4) { /// operator packet
        const sub_packet_values = []
        let return_length
        const length_type = parseInt(packet[6])
        if (length_type == 1) {  /// 11 bits represent number of sub-packets
            const num_packets = parseInt(packet.substr(7, 11), 2)
            let remainingStr = packet.substr(18)

            for (let p = 0; p < num_packets; p++) {
                const results = process_packet(remainingStr)
                remainingStr = remainingStr.substr(results[0])
                sub_packet_values.push(results[1])
            }

            return_length = packet.length - remainingStr.length
        } else {   ///15 bits represent total length in bits
            let counter = 0
            const total_length = parseInt(packet.substr(7, 15), 2)
            let remainingStr = packet.substr(22)
            while (1) {
                if (counter == total_length) break
                const results = process_packet(remainingStr)
                counter += results[0]
                remainingStr = remainingStr.substr(results[0])
                sub_packet_values.push(results[1])
            }

            return_length = total_length + 7 + 15
        }

        let total_packet_value = 0
        if (type == 0) {
            for (value of sub_packet_values) { total_packet_value += value }
        }
        if (type == 1) {
            total_packet_value = 1
            for (value of sub_packet_values) { total_packet_value *= value }
        }
        if (type == 2) {
            total_packet_value = Math.min(...sub_packet_values)
        }
        if (type == 3) {
            total_packet_value = Math.max(...sub_packet_values)
        }
        if (type == 5) {
            total_packet_value = sub_packet_values[0] > sub_packet_values[1] ? 1 : 0
        }
        if (type == 6) {
            total_packet_value = sub_packet_values[0] < sub_packet_values[1] ? 1 : 0
        }
        if (type == 7) {
            total_packet_value = sub_packet_values[0] == sub_packet_values[1] ? 1 : 0
        }

        return [return_length, total_packet_value]

    } else {  /// literal value packet
        const remainingStr = packet.substr(6)
        let index = 0
        let literal_value = ""
        while (remainingStr[index] == '1') {
            literal_value += remainingStr.substr(index+1, 4)
            index += 5
        }
        literal_value += remainingStr.substr(index+1, 4)
        return [index + 6 + 5, parseInt(literal_value, 2)]
    }
}

process_packet(bin)

console.log("Part 2: ", process_packet(bin)[1])
