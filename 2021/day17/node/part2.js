const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().slice(13).split(', ').map(x => x.slice(2).split('..').map(x => parseInt(x)))

const x_range = input[0]
const y_range = input[1]

// const gridSize = 5000
// const half = gridSize / 2

// const grid = new Array(gridSize).fill(0).map(() => new Array(gridSize).fill('.'))

// const setGrid = (y, x, value) => { grid[half - y][x+half] = value }

// setGrid(0,0, 'S')

// for (let y = y_range[0]; y < y_range[1]; y++) {
//     for (let x = x_range[0]; x < x_range[1]; x++) {
//         setGrid(y,x, 'T')
//     }
// }

// const printGrid = () => {
//     for (let y = 0; y < grid.length; y++){
//         for (let x = half; x < half+100; x++) {
//             process.stdout.write(grid[y][x].toString())
//         }
//         console.log()
//     }
//     console.log()
// }

let total_hits = 0
for (let x_vel = 0; x_vel < 100; x_vel++) {

    for (let y_vel = -200; y_vel < 200; y_vel++) {

        const velocity = { x: x_vel, y: y_vel }
        const position = { x: 0, y: 0 }
                
        while (1) {
            position.x += velocity.x
            position.y += velocity.y
            velocity.y--
            if (velocity.x > 0) velocity.x--
            if (velocity.x < 0) velocity.x++
            // setGrid(position.y, position.x, '#')
        
            if (position.x >= x_range[0] && position.x <= x_range[1] && position.y >= y_range[0] && position.y <= y_range[1]) { total_hits++; break }
            if (position.y < y_range[0]) break
        }

    }
}

console.log("Total hits:", total_hits)


// printGrid()
