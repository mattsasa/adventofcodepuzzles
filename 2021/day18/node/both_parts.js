const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n')

const addToLeftMostNumber = (additive, pair) => {
    if (Number.isInteger(pair[0])) pair[0] += additive
    else addToLeftMostNumber(additive, pair[0])
}

const addToRightMostNumber = (additive, pair) => {
    if (Number.isInteger(pair[1])) pair[1] += additive
    else addToRightMostNumber(additive, pair[1])
}

const checkForExplode = (pair, depth) => {

    if (depth == 4) return pair

    if (Array.isArray(pair[0])) {
        const result = checkForExplode(pair[0], depth + 1)

        if (result && result[1] && result[1].special) { 
            if (Number.isInteger(pair[1])) { pair[1] += result[1].value; return [undefined, undefined] }
            addToLeftMostNumber(result[1].value, pair[1])
            return [undefined, undefined]
        }
        if (result) {
            if (result[0] != undefined && result[1] != undefined) pair[0] = 0
            if (result[1] == undefined) return [result[0], undefined]
            if (Number.isInteger(pair[1])) { pair[1] += result[1]; return [result[0], undefined] }
            
            if (result[1]) addToLeftMostNumber(result[1], pair[1])
            if (result[0]) return [{ value: result[0], special: true }, undefined]
            return [undefined, undefined]
        }
    }

    if (Array.isArray(pair[1])) {
        const result = checkForExplode(pair[1], depth + 1)

        if (result && result[0] && result[0].special) { 
            if (Number.isInteger(pair[0])) { pair[0] += result[0].value; return [undefined, undefined] }
            addToRightMostNumber(result[0].value, pair[0]);
            return [undefined, undefined]
        }

        if (result) {
            if (result[0] != undefined && result[1] != undefined) pair[1] = 0
            if (result[0] == undefined) return [undefined, result[1]]
            if (Number.isInteger(pair[0])) { pair[0] += result[0]; return [undefined, result[1]] }

            if (result[0])  addToRightMostNumber(result[0], pair[0]) 
            if (result[1]) return [undefined, { value: result[1], special: true }]
            return [undefined, undefined]
        }
    }

}

const checkForSplit = (pair) => {
    if (Array.isArray(pair[0])) { if (checkForSplit(pair[0])) return true }
    else if (pair[0] >= 10) { 
        pair[0] = [ Math.floor(pair[0]/2), Math.ceil(pair[0]/2) ]
        return true
    }

    if (Array.isArray(pair[1])) { if (checkForSplit(pair[1])) return true }
    else if (pair[1] >= 10) {
        pair[1] = [ Math.floor(pair[1]/2), Math.ceil(pair[1]/2) ]
        return true
    }

    return false
}

const reduceSnailfishNumber = (pair) => {
    let result = checkForExplode(pair, 0)
    let actionTaken = Array.isArray(result)
    while (actionTaken) {
        result = checkForExplode(pair, 0)
        actionTaken = Array.isArray(result)
        if (actionTaken) continue
        actionTaken = checkForSplit(pair)
    }
    return pair
}

const calculateMagnitude = (pair) => {
    return Array.isArray(pair) ? 3*calculateMagnitude(pair[0]) + 2*calculateMagnitude(pair[1]) : pair
}


let largest_magnitude = 0
for (let i = 0; i < lines.length; i++) {
    for(let j = 0; j < lines.length; j++) {
        if (i == j) continue;
        const mag = calculateMagnitude(reduceSnailfishNumber([JSON.parse(lines[i]), JSON.parse(lines[j])]))
        if (mag > largest_magnitude) largest_magnitude = mag
    }
}

let homework_sum = JSON.parse(lines[0])
for (let i = 1; i < lines.length; i++) {
    homework_sum = [homework_sum, JSON.parse(lines[i])]
    reduceSnailfishNumber(homework_sum)
}


console.log("Part 1 - Total Magnitude: ", calculateMagnitude(homework_sum))
console.log("Part 2 - Largest Magnitude: ", largest_magnitude)

