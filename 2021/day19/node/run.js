const fs = require('fs')
const scanners = fs.readFileSync('../data/input.txt').toString().split('\n\n')
    .map(section => section.split('\n').slice(1).map(line => line.split(',').map(x => parseInt(x))))

const vecAdd = (a, b) => [a[0] + b[0], a[1] + b[1], a[2] + b[2]]
const vecSub = (a, b) => [a[0] - b[0], a[1] - b[1], a[2] - b[2]]

const vector_list_includes = (vecs, target) => {
    for (let i = 0; i < vecs.length; i++) {
        if (target[0] == vecs[i][0] && target[1] == vecs[i][1] && target[2] == vecs[i][2]) return true 
    }
}

const cache = new Array(30)

const checkForOverLap = (scannerA, scannerB, rotation, a, b) => {

    b = b.toString() + "-" + rotation.toString()
    a = a.toString() + "-" + "0"

    let best_match = { matches: 0 }

    for (let indexA = 0; indexA < scannerA.length; indexA++) {

        let diffsA = []
        if (!cache[a]) cache[a] = new Array(30)
        if (cache[a][indexA]) diffsA = cache[a][indexA]
        else {
            for (let i = 0; i < scannerA.length; i++) {
                if (i != indexA) diffsA.push(vecSub(scannerA[i], scannerA[indexA]))
            }
            cache[a][indexA] = diffsA
        }

        for (let indexB = 0; indexB < scannerB.length; indexB++) {
            let diffsB = []
            if (!cache[b]) cache[b] = new Array(30)
            if (cache[b][indexB]) { diffsB = cache[b][indexB] }
            else {
                for (let i = 0; i < scannerB.length; i++) {
                    if (i != indexB) diffsB.push(vecSub(scannerB[i], scannerB[indexB]))    
                }
                cache[b][indexB] = diffsB
            }

            const matches = diffsB.filter(b => vector_list_includes(diffsA, b)).length
            if (matches > best_match.matches) best_match = { matches: matches + 1, indexA, indexB }
        }

    }

    return best_match
}


const rotationFuncs = [
    x => [x[0], x[1], x[2]],
    x => [x[1], -x[0], x[2]],
    x => [-x[1], x[0], x[2]],
    x => [x[0], -x[2], x[1]],
    x => [x[0], x[2], -x[1]],
    x => [-x[2], x[1], x[0]],
    x => [x[2], x[1], -x[0]],
    x => [-x[0], -x[1], x[2]],
    x => [x[0], -x[1], -x[2]],
    x => [-x[0], x[1], -x[2]],
    x => [x[1], -x[2], -x[0]],
    x => [-x[1], x[2], -x[0]],
    x => [x[2], -x[0], -x[1]], 
    x => [x[2], x[0], x[1]], 
    x => [-x[1], -x[2], x[0]], 
    x => [x[1], x[2], x[0]], 
    x => [-x[2], -x[0], x[1]], 
    x => [-x[2], x[0], -x[1]], 
    x => [-x[2], -x[1], -x[0]], 
    x => [x[2], -x[1], x[0]], 
    x => [-x[1], -x[0], -x[2]], 
    x => [x[1], x[0], -x[2]], 
    x => [-x[0], x[2], x[1]], 
    x => [-x[0], -x[2], -x[1]], 
]

const findInverseRotation = (rotation) => {
    const first_rot = rotation([1,2,3])
    for (let i = 0; i < rotationFuncs.length; i++) {
        const inverse = rotationFuncs[i](first_rot)
        if (inverse[0] == 1 && inverse[1] == 2 && inverse[2] == 3) return rotationFuncs[i]
    }
}

const checkForOverLapAllDirections = (scannerA, scannerB, i, j) => {

    let best_match

    rotationFuncs.forEach(rotation => {
        const result = checkForOverLap(scannerA, scannerB.map(rotation), rotation, i, j)
        if (result.matches >= 12) best_match = { ...result, rotation }
    })

    if (best_match == undefined) return

    const referenceBeaconA = scannerA[best_match.indexA]
    const referenceBeaconB = best_match.rotation(scannerB[best_match.indexB])

    return { rotation: best_match.rotation, translation: vecSub(referenceBeaconA, referenceBeaconB) }
}

const overlapMap = {}

for (let i = 0; i < scanners.length; i++) {
    overlapMap[i] = {}
    for (let j = i + 1; j < scanners.length; j++) {
        const result = checkForOverLapAllDirections(scanners[i], scanners[j], i, j)
        if (result) overlapMap[i][j] = result
    }
}

/// make bidirectional map
Object.entries(overlapMap).forEach(([parentkey, value]) => {
    Object.entries(value).forEach(([dstkey, data]) => {
        const newData = {}
        newData.rotation = findInverseRotation(data.rotation)
        newData.translation = newData.rotation(vecSub([0,0,0], data.translation))
        overlapMap[dstkey][parentkey] = newData
    })
})

const fullBeaconMap = Object.fromEntries(scanners[0].map(beacon => [beacon, 1]))

const scannerPoses = { 0: { rotations: [], translation: [0, 0, 0] } }

const allScanners = [0]

let recentScanners = [0]

while (recentScanners.length > 0) {
    const new_recents = []
    recentScanners.forEach(src_i => {
        let keys = Object.keys(overlapMap[src_i]).map(x => parseInt(x)).filter(key => !allScanners.includes(key))

        keys.forEach(dst => {

            let translation = overlapMap[src_i][dst].translation
            const src_translation = scannerPoses[src_i].translation

            scannerPoses[src_i].rotations.slice(0).reverse().forEach(rotation => translation = rotation(translation))

            translation = vecAdd(src_translation, translation)

            const newRotations =  [...scannerPoses[src_i].rotations, overlapMap[src_i][dst].rotation]
            scannerPoses[dst] = { rotations: newRotations, translation }

            scanners[dst].forEach(beacon => {
                scannerPoses[dst].rotations.slice(0).reverse().forEach(rotation => beacon = rotation(beacon))
                const newBeacon = vecAdd(beacon, scannerPoses[dst].translation)
                fullBeaconMap[newBeacon] = 1
            })
        })

        allScanners.push(...keys)
        new_recents.push(...keys)
    })
    recentScanners = new_recents
}


console.log("Part 1: ", Object.keys(fullBeaconMap).length)

////////// Start Part 2

const scannerLocations = Object.values(scannerPoses).map(x => x.translation)

let largest_man_dist = 0

for (let i = 0; i < scannerLocations.length; i++) {
    for (let j = i+1; j < scannerLocations.length; j++) {
        const x_diff = (scannerLocations[i][0] - scannerLocations[j][0])
        const y_diff = (scannerLocations[i][1] - scannerLocations[j][1])
        const z_diff = (scannerLocations[i][2] - scannerLocations[j][2])
        const dist = Math.abs(x_diff) + Math.abs(y_diff) + Math.abs(z_diff)
        if (dist > largest_man_dist) largest_man_dist = dist
    }
}

console.log("Part 2: ", largest_man_dist)

