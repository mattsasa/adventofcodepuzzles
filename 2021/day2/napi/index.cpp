#include <napi.h>
#include <string>
#include <iostream>

void process(const Napi::CallbackInfo& info) {

    int h = 0; int depth = 0; int aim = 0;

    Napi::Array napi_arr = info[0].As<Napi::Array>();
    for (size_t i = 0; i < napi_arr.Length(); i++) {
        Napi::Value item = napi_arr[i];
        Napi::Array pair = item.As<Napi::Array>();
        Napi::Value first = pair[(uint)0];
        Napi::Value second = pair[(uint)1];
        char direction = first.ToString().Utf8Value().at(0);
        int mag = second.ToNumber();

        if (direction == 'f') { h += mag; depth += (aim * mag); }
        if (direction == 'u') aim -= mag;
        if (direction == 'd') aim += mag;
    }

    std::cout << "Part 2: " << h * depth << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);