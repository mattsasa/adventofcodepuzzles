const fs = require('fs')
const pairs = fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => x.split(' ')).slice(0, -1)

///Process C++
require('./build/Release/day2.node').process(pairs)