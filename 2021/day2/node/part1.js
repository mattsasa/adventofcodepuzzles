const fs = require('fs')

let h = 0, depth = 0

fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => x.split(' ')).forEach(pair => {
    const direction = pair[0][0]
    const mag = parseInt(pair[1])

    if (direction == 'f') h += mag
    if (direction == 'u') depth -= mag
    if (direction == 'd') depth += mag

})

console.log("Part 1:", h * depth)