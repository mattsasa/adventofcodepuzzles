const fs = require('fs')

let h = 0, depth = 0, aim = 0

fs.readFileSync('../data/input.txt').toString().split("\n").map((x) => x.split(' ')).forEach(pair => {
    const direction = pair[0][0]
    const mag = parseInt(pair[1])

    if (direction == 'f') { h += mag; depth += (aim * mag) }
    if (direction == 'u') aim -= mag
    if (direction == 'd') aim += mag

})

console.log("Part 2:", h * depth)
