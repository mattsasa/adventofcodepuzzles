const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n') //.map(x => JSON.parse(x))

const iam_data = lines[0]

// const printGrid = (grid) => {
//     for (let y = 0; y < grid.length; y++){
//         for (let x = 0; x < grid[y].length; x++) {
//             process.stdout.write(grid[y][x].toString())
//         }
//         console.log()
//     }
//     console.log()
// }

const input_image = lines.slice(2).map(x => x.split(''))

const total_steps = 50 /// 2 for part 1,  50 for part 2

const buffer = total_steps + 2

const gridSize = input_image.length + buffer*2

let image = new Array(gridSize).fill(0).map(() => new Array(gridSize).fill('.'))

for (let y = buffer; y < gridSize - buffer; y++) {
    for (let x = buffer; x < gridSize - buffer; x++) {
        image[y][x] = input_image[y - buffer][x - buffer]
    }
}


for (let steps = 0; steps < total_steps; steps++) {
    const new_image = new Array(gridSize).fill(0).map(() => new Array(gridSize).fill('.'))
    for (let y = 1; y < gridSize - 1; y++) {
        for (let x = 1; x < gridSize - 1; x++) {
            const binString = image[y-1][x-1] + image[y-1][x] + image[y-1][x+1] + image[y][x-1] + image[y][x] + image[y][x+1] + image[y+1][x-1] + image[y+1][x] + image[y+1][x+1]
            const binNumber = parseInt(binString.replaceAll('.', '0').replaceAll('#', '1'), 2)
            new_image[y][x] = iam_data[binNumber]
        }
    }
    image = new_image

    if (steps % 2 != 0) {
        for (let x = 0; x < gridSize; x++) {
            image[1][x] = '.'
            image[gridSize - 2][x] = '.'
        }
        for (let y = 0; y < gridSize; y++) {
            image[y][1] = '.'
            image[y][gridSize - 2] = '.'
        }
    }
}


let count = 0
for (let y = 0; y < gridSize; y++) {
    for (let x = 0; x < gridSize; x++) {
        if (image[y][x] == '#') count++
    }
}

console.log("Lit Pixels: ", count)