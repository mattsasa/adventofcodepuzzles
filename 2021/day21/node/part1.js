let p1 = 7, p2 = 6   //my input 7 6,  sample 4 8

let p1_score = 0, p2_score = 0

const ddice = { total_rolls: 0, current_roll: 0 }

const roll_dice = () => {
    ddice.current_roll++
    if (ddice.current_roll > 100) ddice.current_roll = 1

    ddice.total_rolls++

    return ddice.current_roll
}

let loser_score

while (true) {
    p1 += roll_dice() + roll_dice() + roll_dice()
    while (p1 > 10) p1 -= 10
    p1_score += p1
    if (p1_score >= 1000) { loser_score = p2_score; break }

    p2 += roll_dice() + roll_dice() + roll_dice()
    while (p2 > 10) p2 -= 10
    p2_score += p2
    if (p2_score >= 1000) { loser_score = p1_score; break }

}

console.log("Part 1: ", ddice.total_rolls * loser_score)