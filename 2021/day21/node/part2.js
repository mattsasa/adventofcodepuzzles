let p1 = 7, p2 = 6 

const cache = {}
const cache2 = {}

const gen_more_combinations = (arr, index) => {
    return arr.map(arr => {
        let newarr = []
        for (let i = 1; i < 4; i++) {
            let x = [...arr]
            x[index] = i
            newarr.push(x)
        }
        return newarr
    })
}

let combinations = [[1,1,1]]
for (let i = 0; i < 3; i++) { combinations = gen_more_combinations(combinations, i).flat() }

const process_dice_rolls = (dice_rolls, p_score, p_pos) => {
    p_pos += dice_rolls[0] + dice_rolls[1] + dice_rolls[2]
    while (p_pos > 10) p_pos -= 10
    p_score += p_pos
    if (p_score >= 21) return 1
    else return [p_score, p_pos]
}

const doTurnAllOutcomesP2 = (p1_score, p2_score, p1, p2) => {
    const wins = { p1_wins: 0, p2_wins: 0 }

    combinations.forEach(dice_rolls => {

        const result = process_dice_rolls(dice_rolls, p2_score, p2)

        if (!Array.isArray(result)) {  // P2 won
            wins.p2_wins++
            return
        } 
        
        /// both players did not win
        const p2_ = result[1], p2_score_ = result[0]
        const key_string = `${p1_score},${p2_score_},${p1},${p2_}`

        if (cache2[key_string]) {
            wins.p1_wins += cache2[key_string].p1_wins
            wins.p2_wins += cache2[key_string].p2_wins
            return
        }

        const recursive_result = doTurnAllOutcomes(p1_score, p2_score_, p1, p2_)
        cache2[key_string] = recursive_result
        wins.p1_wins += recursive_result.p1_wins
        wins.p2_wins += recursive_result.p2_wins
    })

    return wins
}

const doTurnAllOutcomes = (p1_score, p2_score, p1, p2) => {

    const wins = { p1_wins: 0, p2_wins: 0 }

    combinations.forEach(dice_rolls => {
        const result = process_dice_rolls(dice_rolls, p1_score, p1)

        if (!Array.isArray(result)) {  // P1 won
            wins.p1_wins++
            return
        }  

        const p1_ = result[1], p1_score_ = result[0]
        const key_string = `${p1_score_},${p2_score},${p1_},${p2}`

        if (cache[key_string]) {
            wins.p1_wins += cache[key_string].p1_wins
            wins.p2_wins += cache[key_string].p2_wins
            return
        }

        /// roll for player 2
        const wins2 = doTurnAllOutcomesP2(p1_score_, p2_score, p1_, p2)
        cache[key_string] = wins2
        wins.p1_wins += wins2.p1_wins
        wins.p2_wins += wins2.p2_wins
    })

    return wins
}

const wins = doTurnAllOutcomes(0, 0, p1, p2)

console.log("Part 2: ", Math.max(wins.p1_wins, wins.p2_wins))


