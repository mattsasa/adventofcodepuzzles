const fs = require('fs')
const lines = fs.readFileSync('../data/sample.txt').toString().split('\n').map(x => {
    const obj = {}
    obj.on = x[1] == 'n' ? 1 : 0
    const dims = x.slice(3).replace(" x", "x").split(',').map(x => x.split('='))
    dims.forEach(dim => {
        obj[dim[0]] = dim[1].split('..').map(x => parseInt(x))
    })
    return obj
})

const gridSize = 101

const cube_area = new Array(gridSize).fill(0).map(() => new Array(gridSize).fill(0).map(() => new Array(gridSize).fill(0)))

lines.forEach(line => {

    // console.log(line)

    const minx = Math.max(0, line.x[0] + 50)
    const miny = Math.max(0, line.y[0] + 50)
    const minz = Math.max(0, line.z[0] + 50)

    const maxx = Math.min(100, line.x[1] + 50)
    const maxy = Math.min(100, line.y[1] + 50)
    const maxz = Math.min(100, line.z[1] + 50)


    for (let x = minx; x <= maxx; x++) {
        for (let y = miny; y <= maxy; y++) {
            for (let z = minz; z <= maxz; z++) {
                // console.log(x, y, z)
                cube_area[x][y][z] = line.on

            }
        }
    }

})

let count = 0
cube_area.forEach(plane => {
    plane.forEach(row => {
        row.forEach(item => {
            count += item
        })
    })
})

console.log(count)