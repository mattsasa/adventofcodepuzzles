const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => {
    const obj = {}
    obj.on = x[1] == 'n' ? 1 : 0
    const dims = x.slice(3).replace(" x", "x").split(',').map(x => x.split('='))
    dims.forEach(dim => {
        const range = dim[1].split('..').map(x => parseInt(x))
        obj[dim[0]] = [range[0], range[1]+1]
    })
    return obj
})

const intersects = (a, b) => {
    return a.x[0] <= b.x[1] && a.x[1] >= b.x[0] && a.y[0] <= b.y[1] && a.y[1] >= b.y[0] && a.z[0] <= b.z[1] && a.z[1] >= b.z[0]
}

const findCutIndexes = (origCube, newCube, dim) => {
    const cuts = [origCube[dim][0]]

    if (newCube[dim][0] > origCube[dim][0] && newCube[dim][0] < origCube[dim][1]) cuts.push(newCube[dim][0])
    if (newCube[dim][1] > origCube[dim][0] && newCube[dim][1] < origCube[dim][1]) cuts.push(newCube[dim][1])
    cuts.push(origCube[dim][1])

    return cuts
}

const cutCubes = (origCube, newCube) => {
    const keepCubes = []

    const x_cuts = findCutIndexes(origCube, newCube, "x")
    const y_cuts = findCutIndexes(origCube, newCube, "y")
    const z_cuts = findCutIndexes(origCube, newCube, "z")

    for (let x_index = 0; x_index < x_cuts.length - 1; x_index++) {
        for (let y_index = 0; y_index < y_cuts.length - 1; y_index++) {
            for (let z_index = 0; z_index < z_cuts.length - 1; z_index++) {
                keepCubes.push({ 
                    x: [x_cuts[x_index], x_cuts[x_index+1]], 
                    y: [y_cuts[y_index], y_cuts[y_index+1]],
                    z: [z_cuts[z_index], z_cuts[z_index+1]]
                })
            }
        }
    }

    return keepCubes.filter(cube => {
        return !(cube.x[0] >= newCube.x[0] && cube.x[1] <= newCube.x[1]
          && cube.y[0] >= newCube.y[0] && cube.y[1] <= newCube.y[1]
          && cube.z[0] >= newCube.z[0] && cube.z[1] <= newCube.z[1])
    })
}

let onCubes = []

lines.forEach((newCube, ci) => {

    for (let i = 0; i < onCubes.length; i++) {
        if (intersects(newCube, onCubes[i])) { 
            onCubes[i] = cutCubes(onCubes[i], newCube)
        } 
    }

    onCubes = onCubes.flat()
    if (newCube.on) onCubes.push(newCube)
})

const sum = onCubes.reduce((sum, cube) => sum + ((cube.x[1] - cube.x[0]) * (cube.y[1] - cube.y[0]) * (cube.z[1] - cube.z[0])), 0)

console.log("Part 2: ", sum)