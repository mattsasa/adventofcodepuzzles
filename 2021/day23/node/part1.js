const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split(''))

const energyMap = { A: 1, B: 10, C: 100, D: 1000 }
const destinationMap = { A: 2, B: 4, C: 6, D: 8 }
const binStringMap = { A: '00', B: '01', C: '10', D: '11' }
const binStringMapR = { '00': 'A', '01': 'B', '10': 'C', '11': 'D' }

const amphipods = { A: [], B: [], C: [], D: [] }

//// read input to create original state
for (let h = 1; h < 4; h++) {
    for (let w = 1; w < 12; w++) {
        if (['A','B','C','D'].includes(lines[h][w])) amphipods[lines[h][w]].push([h-1,w-1])
    }
}


const obj2Bin = (obj) => { 

    const grid = new Array(3).fill(0).map(x => new Array(11).fill('.'))

    for ([key, pods] of Object.entries(obj)) {
        grid[pods[0][0]][pods[0][1]] = key
        grid[pods[1][0]][pods[1][1]] = key
    }

    let binString = ""

    const hallway = Object.values(obj).flat().filter(loc => loc[0] == 0).map(x => x[1])

    for (let x = 0; x < 11; x++) {
        binString += hallway.includes(x) ? "1" : "0"
    }

    [2, 4, 6, 8].forEach(roomx => {
        const inRoom = Object.values(obj).flat().filter(loc => loc[1] == roomx).map(x => x[0])
        binString += inRoom.find(loc => loc == 1) ? "1" : "0"
        binString += inRoom.find(loc => loc == 2) ? "1" : "0"
    })

    for (let x = 0; x < 11; x++) {
        if (grid[0][x] != '.') binString += binStringMap[grid[0][x]]
    }

    [2, 4, 6, 8].forEach(roomx => {
        if (grid[1][roomx] != '.') binString += binStringMap[grid[1][roomx]]
        if (grid[2][roomx] != '.') binString += binStringMap[grid[2][roomx]]
    })

    return parseInt(binString, 2)
}

const bin2Obj = (bin) => {
    const binString = bin.toString(2).padStart(35, '0')
    const placements = binString.slice(0, 19)
    const amphipodTypes = binString.slice(19)

    let typeIndex = 0

    const amphipods = { A: [], B: [], C: [], D: [] }
    
    //hallway
    for (let i = 0; i < 11; i++) {
        if (placements[i] == '1') {
            const typeString = amphipodTypes.slice(typeIndex, typeIndex + 2)
            typeIndex += 2
            amphipods[binStringMapR[typeString]].push([0, i])
        }
    }

    for (let i = 11; i < placements.length; i++) {
        if (placements[i] == '1') {
            const y = ((i - 1) % 2) + 1
            const x = Math.floor((i - 9) / 2) * 2
            const typeString = amphipodTypes.slice(typeIndex, typeIndex + 2)
            typeIndex += 2
            amphipods[binStringMapR[typeString]].push([y, x])
        }
    }

    return amphipods

}

const moveIfPossible = (amphipods, type, location) => {

    if (location[0] == 0) {   //// In da hallway, must go to destination room

        const destination_x = destinationMap[type]

        //// check for any amphipods that don't belong
        for ([key, pods] of Object.entries(amphipods)) {
            if (key != type) {  /// not the correct amphipod type for destination
                if (pods.find(pod => pod[1] == destination_x)) return
            }
        }

        //// Check to see if there is obstacle to destination room
        const hallway = Object.values(amphipods).flat().filter(loc => loc[0] == 0).map(x => x[1])
        const start = destination_x > location[1] ? location[1] : destination_x
        const end = destination_x > location[1] ? destination_x : location[1]
        for (let i = start + 1; i < end; i++) {
            if (hallway.includes(i)) return /// there is obstacle blocking path to destination room
        }


        let destination_y = 2
        const siblingYsInDst = amphipods[type].filter(x => x[1] == destination_x).map(x => x[0])
        while (siblingYsInDst.find(x => x == destination_y)) destination_y--
        return [[destination_y, destination_x]]

    } else {  //// in a room

        // Check to see if can't move because blocked
        const ysInRoom = Object.values(amphipods).flat().filter(loc => loc[1] == location[1]).map(x => x[0])
        for (let y = 1; y < location[0]; y++) {
            if (ysInRoom.includes(y)) return
        }

        /// should be able to move out of room  - front of room

        /// If already in final destination and everyone below is sibling.... don't move
        if (destinationMap[type] == location[1]) {
            const siblingYsInDst = amphipods[type].filter(x => x[1] == location[1]).map(x => x[0])
            let everySpotBelowIsSibling = true
            for (let y = location[0] + 1; y <= 2; y++) {
                if (!siblingYsInDst.includes(y)) everySpotBelowIsSibling = false
            }
            if (everySpotBelowIsSibling) return
        }


        const newLocs = []
        /// we know [0, location[1]] shoud be free, but can't move there
        let shift_x_left = location[1] - 1
        const hallway = Object.values(amphipods).flat().filter(loc => loc[0] == 0).map(x => x[1])
        while (shift_x_left >= 0 && !hallway.includes(shift_x_left)) {
            if (![2, 4, 6, 8].includes(shift_x_left)) newLocs.push([0, shift_x_left])
            shift_x_left--
        }
        let shift_x_right = location[1] + 1
        while (shift_x_right < 11 && !hallway.includes(shift_x_right)) {
            if (![2, 4, 6, 8].includes(shift_x_right)) newLocs.push([0, shift_x_right])
            shift_x_right++
        }
        return newLocs
    }

}

const cache = {}

const recursiveFindLowestEnergy = (state) => {

    let lowest_energy = 9999999999

    const amphipods = bin2Obj(state)
    Object.entries(amphipods).forEach(([type, locations]) => {
        locations.forEach((location, i) => {
            const newLocations = moveIfPossible(amphipods, type, location)
            if (newLocations) {
                newLocations.forEach(newLoc => {
                    const mandist = Math.abs(location[0] - newLoc[0]) + Math.abs(location[1] - newLoc[1])
                    const backup = amphipods[type][i]
                    amphipods[type][i] = newLoc
                    const new_state = obj2Bin(amphipods)
                    amphipods[type][i] = backup

                    let local_energy_total = mandist * energyMap[type]

                    if (new_state != 16713135) { /// not a completed solution
                        /// check the cache and load the cache
                        if (cache[new_state] == undefined) cache[new_state] = recursiveFindLowestEnergy(new_state)
                        local_energy_total += cache[new_state]
                    }

                    if (local_energy_total < lowest_energy) lowest_energy = local_energy_total

                })
            }
        })
    })

    return lowest_energy
}

console.log(amphipods)

const lowest_energy = recursiveFindLowestEnergy(obj2Bin(amphipods))

console.log("Part 1: ", lowest_energy)