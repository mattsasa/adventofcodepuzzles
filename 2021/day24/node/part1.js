let lowest_z = 99999999

for (let model = 9999999; model > 1111111; model--) {

  // let model_ = "9995999" + model.toString()

  ///4 pick
  ///7 pick
  ///8 pick
  ///9 pick
  ///10 pick
  ///12 pick
  ///13 pick

  const someInput_ = model.toString().split('').map(x => parseInt(x))
  if (someInput_.includes(0)) continue

  const someInput = new Array(14)

  someInput[0] = someInput_[0]
  someInput[1] = someInput_[1]
  someInput[2] = someInput_[2]
  someInput[3] = someInput_[3]
  someInput[5] = someInput_[4]
  someInput[6] = someInput_[5]
  someInput[11] = someInput_[6]


  let my_z = ((((((someInput[0] + 12) * 26) + (someInput[1] + 10)) * 26) + (someInput[2] + 8)) * 26) + (someInput[3] + 4)
  
  someInput[4] = (my_z % 26) < 10 ? (my_z % 26) : 9
  let bool1 = someInput[4] != (my_z % 26)
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[4] + 3 }
  //else { console.log("SKIP first", model_); process.exit(1) }

  // console.log(my_z)
  // bool1 = someInput[5] != (my_z % 26) + 15
  my_z *= 26; my_z += someInput[5] + 10
  // else { console.log("SKIP [5]", model_); process.exit(1) }

  // bool1 = someInput[6] != (my_z % 26) + 13
  my_z*= 26;  my_z += someInput[6] + 6

  someInput[7] = ((my_z % 26) - 12) < 10 ? ((my_z % 26) - 12) : 9
  bool1 = someInput[7] != (my_z % 26) - 12
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[7] + 13 }
  //else { console.log("SKIP [7]", model_, my_z); process.exit(1) }

  someInput[8] = ((my_z % 26) - 15) < 10 ? ((my_z % 26) - 15) : 9
  bool1 = someInput[8] != (my_z % 26) - 15
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[8] + 8 }
  // else { console.log("SKIP [8]", model_, my_z); process.exit(1) }

  someInput[9] = ((my_z % 26) - 15) < 10 ? ((my_z % 26) - 15) : 9
  bool1 = someInput[9] != (my_z % 26) - 15
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[9] + 1 }
  // else { console.log("SKIP [9]", model_, my_z); process.exit(1) }

  someInput[10] = ((my_z % 26) - 4) < 10 ? ((my_z % 26) - 4) : 9
  bool1 = someInput[10] != (my_z % 26) - 4
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[10] + 7 }

  // bool1 = someInput[11] != (my_z % 26) + 10
  my_z*= 26;  my_z += someInput[11] + 6

  someInput[12] = ((my_z % 26) - 5) < 10 ? ((my_z % 26) - 5) : 9
  bool1 = someInput[12] != (my_z % 26) - 5
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[12] + 9 }

  someInput[13] = ((my_z % 26) - 12) < 10 ? ((my_z % 26) - 12) : 9
  bool1 = someInput[13] != (my_z % 26) - 12
  my_z = Math.floor(my_z / 26)
  if (bool1) { my_z *= 26; my_z += someInput[13] + 9 }

  if (my_z == 0) { console.log("VALID: ", model, someInput); process.exit() }

  if (my_z < lowest_z) lowest_z = my_z

}

console.log("DONE - lowest z", lowest_z)
process.exit(1)

const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split(' ')).map(x => { return { instruction: x[0], arg1: x[1], arg2: x[2] } })


const model = 13579246899999

for (let i = 1; i < 10; i++) {
  for (let k = 1; k < 10; k++) {
    for (let j = 1; j < 10; j++) {
      for (let l = 1; l < 10; l++) {
        for (let m = 1; m < 10; m++) {

            const state = { w: 0, x: 0, y: 0, z: 0 }

            const someInput = model.toString().split('').map(x => parseInt(x))

            someInput[0] = i
            someInput[1] = j
            someInput[8] = k
            someInput[11] = l
            someInput[13] = m

            let inputCount = 0

            lines.forEach(line => {
                switch(line.instruction) {
                    case 'inp':
                        state[line.arg1] = someInput[inputCount++]
                        break
                    case 'add':
                        state[line.arg1] += ['w','x','y','z'].includes(line.arg2) ? state[line.arg2] : parseInt(line.arg2)
                        break
                    case 'mul':
                        state[line.arg1] *= ['w','x','y','z'].includes(line.arg2) ? state[line.arg2] : parseInt(line.arg2)
                        break
                    case 'div':
                        state[line.arg1] = Math.floor(state[line.arg1] / (['w','x','y','z'].includes(line.arg2) ? state[line.arg2] : parseInt(line.arg2)))
                        break
                    case 'mod':
                        state[line.arg1] %= ['w','x','y','z'].includes(line.arg2) ? state[line.arg2] : parseInt(line.arg2)
                        break
                    case 'eql':
                        state[line.arg1] = state[line.arg1] == (['w','x','y','z'].includes(line.arg2) ? state[line.arg2] : parseInt(line.arg2)) ? 1 : 0
                        break
                }
            })

            // if (state.z == 0) console.log("VALID") 
            // console.log(state.z, ((someInput[0] + 12) * 26) + (someInput[1] + 10))
            // console.log(state.z, result)
            // if (state.z != result) console.log(i, j, k, l, state.z, result)
            // if (state.x == someInput[4]) console.log(i, j, k, l, m)

            let my_z = ((((((someInput[0] + 12) * 26) + (someInput[1] + 10)) * 26) + (someInput[2] + 8)) * 26) + (someInput[3] + 4)
            let bool1 = someInput[4] != (my_z % 26)
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[4] + 3 }

            bool1 = someInput[5] != (my_z % 26) + 15
            if (bool1) { my_z *= 26; my_z += someInput[5] + 10 }

            bool1 = someInput[6] != (my_z % 26) + 13
            if (bool1) my_z*= 26;  my_z += someInput[6] + 6

            bool1 = someInput[7] != (my_z % 26) - 12
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[7] + 13 }

            bool1 = someInput[8] != (my_z % 26) - 15
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[8] + 8 }

            bool1 = someInput[9] != (my_z % 26) - 15
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[9] + 1 }

            bool1 = someInput[10] != (my_z % 26) - 4
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[10] + 7 }

            bool1 = someInput[11] != (my_z % 26) + 10
            if (bool1) my_z*= 26;  my_z += someInput[11] + 6

            bool1 = someInput[12] != (my_z % 26) - 5
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[12] + 9 }

            bool1 = someInput[13] != (my_z % 26) - 12
            my_z = Math.floor(my_z / 26)
            if (bool1) { my_z *= 26; my_z += someInput[13] + 9 }

            if (state.z != my_z) console.log("WRONG!")
        }
      }
    }
  }
}


