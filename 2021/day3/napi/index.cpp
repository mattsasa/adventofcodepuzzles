#include <napi.h>
#include <string>
#include <iostream>

template<typename T, typename F>
void filterInPlace(std::vector<T> &vec, F&& lambda) {
    vec.erase(std::remove_if(vec.begin(), vec.end(), lambda), vec.end());
}

void process(const Napi::CallbackInfo& info) {

    Napi::Array napi_arr = info[0].As<Napi::Array>();
    std::vector<int> numbers(napi_arr.Length());
    for (size_t i = 0; i < napi_arr.Length(); i++) {
        Napi::Value item = napi_arr[i];
        numbers[i] = item.ToNumber();
    }
    std::vector<int> numbers2(numbers);

    int bit_index = 11;
    while (numbers.size() > 1) {
        int b0 = 0; int b1 = 0;
        for (int n: numbers) { ((n & (1 << bit_index)) == 0) ? b0++ : b1++; }
        auto lambda = [&](int n) { return ((n & (1 << bit_index)) == 0) == (b0 <= b1 ? 1 : 0); };
        filterInPlace(numbers, lambda);
        bit_index--;
    }

    bit_index = 11;
    while (numbers2.size() > 1) {
        int b0 = 0; int b1 = 0;
        for (int n: numbers2) { ((n & (1 << bit_index)) == 0) ? b0++ : b1++; }
        auto lambda = [&](int n) { return ((n & (1 << bit_index)) == 0) == (b0 > b1 ? 1 : 0); };
        filterInPlace(numbers2, lambda);
        bit_index--;
    }

    std::cout << "Part 2: " << numbers[0] * numbers2[0] << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);