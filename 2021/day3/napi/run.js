const fs = require('fs')
const binStrings = fs.readFileSync('../data/input.txt').toString().split("\n")

///Process C++
require('./build/Release/day3.node').process(binStrings.map(str => parseInt(str, 2)))