const fs = require('fs')
const binStrings = fs.readFileSync('../data/input.txt').toString().split("\n")

let gamma = "", epsilon = ""

for (let i = 0; i < binStrings[0].length; i++) {
    let b0 = 0, b1 = 0
    binStrings.forEach(line => { (line[i] == '0') ? b0++ : b1++ })
    if (b0 > b1) { gamma += '0'; epsilon += '1' }
    else { gamma += '1'; epsilon += '0' }
}

console.log(parseInt(epsilon, 2) * parseInt(gamma, 2))