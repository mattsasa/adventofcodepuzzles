const fs = require('fs')
let binStrings = fs.readFileSync('../data/input.txt').toString().split("\n")
let binStrings2 = [...binStrings]

let bit_index = 0

while (binStrings.length > 1) {
    let b0 = 0, b1 = 0
    binStrings.forEach(line => { (line[bit_index] == '0') ? b0++ : b1++ })
    binStrings = binStrings.filter(line => line[bit_index] == (b0 <= b1 ? '1' : '0'))
    bit_index++
}

bit_index = 0

while (binStrings2.length > 1) {
    let b0 = 0, b1 = 0
    binStrings2.forEach(line => { (line[bit_index] == '0') ? b0++ : b1++ })
    binStrings2 = binStrings2.filter(line => line[bit_index] == (b0 > b1 ? '1' : '0'))
    bit_index++
}

console.log(parseInt(binStrings[0], 2) * parseInt(binStrings2[0], 2))

