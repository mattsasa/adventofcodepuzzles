#include <napi.h>
#include <string>
#include <iostream>

template<typename T, typename F>
void filterInPlace(std::vector<T> &vec, F&& lambda) {
    vec.erase(std::remove_if(vec.begin(), vec.end(), lambda), vec.end());
}

struct Board {
    int data[5][5];
};

// typedef int (*Board)[5][5];

bool checkBoard(Board b) {
    for (int h = 0; h < 5; h++) { ///check rows
        int sum = 0;
        for (int w = 0; w < 5; w++) {
            if (b.data[h][w] == -1) sum++;
        }
        if (sum == 5) return true;
    }
    for (int w = 0; w < 5; w++) { ///check cols
        int sum = 0;
        for (int h = 0; h < 5; h++) {
            if (b.data[h][w] == -1) sum++;
        }
        if (sum == 5) return true;
    }
    return false;
}

void checkBoards(std::vector<Board>& boards) { filterInPlace(boards, checkBoard); }

void markBoards(std::vector<Board>& boards, int number) {
    for (Board& b: boards) {
        for (int h = 0; h < 5; h++) {
            for (int w = 0; w < 5; w++) { 
                if (b.data[h][w] == number) b.data[h][w] = -1;
            }
        }
    }
}


void process(const Napi::CallbackInfo& info) {

    Napi::Array napi_drawnNumbers = info[0].As<Napi::Array>();
    std::vector<int> drawnNumbers(napi_drawnNumbers.Length());
    for (size_t i = 0; i < napi_drawnNumbers.Length(); i++) {
        Napi::Value item = napi_drawnNumbers[i];
        drawnNumbers[i] = item.ToNumber();
    }

    Napi::Array napi_boards = info[1].As<Napi::Array>();
    std::vector<Board> boards(napi_boards.Length());
    for (size_t i = 0; i < napi_boards.Length(); i++) {
        Napi::Array rows = ((Napi::Value)napi_boards[i]).As<Napi::Array>();
        for (uint h = 0; h < 5; h++) {
            Napi::Array items = ((Napi::Value)rows[h]).As<Napi::Array>();
            for (uint w = 0; w < 5; w++) {
                boards[i].data[h][w] = ((Napi::Value)items[w]).ToNumber();
            }
        }
    }

    
    int calledNumber = -1;
    Board* lastBoard;

    while (boards.size() > 0) {
        markBoards(boards, drawnNumbers[++calledNumber]);
        checkBoards(boards);
        if (boards.size() == 1) lastBoard = &boards[0];
    }

    int sum = 0;
    for (int h = 0; h < 5; h++) {
        for (int w = 0; w < 5; w++) { 
            if (lastBoard->data[h][w] != -1) sum += lastBoard->data[h][w];
        }
    }

    std::cout << "Part 2: " << drawnNumbers[calledNumber] * sum << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);