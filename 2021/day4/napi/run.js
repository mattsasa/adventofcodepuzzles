const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n\n')
const drawnNumbers = lines[0].split(',').map((x) => parseInt(x))
const boards = lines.slice(1).map(board => board.split('\n').map(line => line.split(' ').filter(x => x != '').map((x) => parseInt(x) )))

///Process C++
require('./build/Release/day4.node').process(drawnNumbers, boards)