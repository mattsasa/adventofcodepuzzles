const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n\n')
const drawnNumbers = lines[0].split(',').map((x) => parseInt(x))
const boards = lines.slice(1).map(board => board.split('\n').map(line => line.split(' ').filter(x => x != '').map((x) => parseInt(x) )))

const allMarked = arr => arr.every( x => x == -1)

const markBoards = (number) => {
    boards.forEach(board => { board.forEach(row => { row.forEach((item, i) => { if (item == number) row[i] = -1 }) }) })
}

const checkBoard = (board) => {
    for (let colIndex = 0; colIndex < board.length; colIndex++) {
        const col = []
        for (let rowIndex = 0; rowIndex < board.length; rowIndex++) {
            if (allMarked(board[rowIndex])) return true
            col.push(board[rowIndex][colIndex])
        }
        if (allMarked(col)) return true
    }
}

const checkBoards = () => {
    for (let i = 0; i < boards.length; i++) { if (checkBoard(boards[i])) return i }
}

let winner, calledNumber = -1

while (winner == undefined) { 
    markBoards(drawnNumbers[++calledNumber])
    winner = checkBoards()
}

const sum = boards[winner].reduce((sum, row) => { return row.reduce((sum, item) => { return item != -1 ? sum + item : sum }, sum) }, 0)
console.log("Part 1 Answer:", drawnNumbers[calledNumber] * sum)

