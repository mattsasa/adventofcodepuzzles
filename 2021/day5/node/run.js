const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split('->').map(x => x.split(',').map(x => parseInt(x)))).map(items => {
    return { x1: items[0][0], y1: items[0][1], x2: items[1][0], y2: items[1][1] }
})

const gridSize = 1000

const grid = new Array(gridSize).fill(0).map(() => new Array(gridSize).fill(0))

// const printGrid = () => {
//     for (let y = 0; y < gridSize; y++){
//         for (let x = 0; x < gridSize; x++) {
//             if (grid[y][x] == 0) process.stdout.write('.')
//             else process.stdout.write(grid[y][x].toString())
//         }
//         console.log()
//     }
// }

const setDirection = (a, b) => a == b ? 0 : (a < b ? 1 : -1) 
  
lines.forEach(line => {

    // if (line.x1 != line.x2 && line.y1 != line.y2) return    // Uncomment for part 1

    const x_direction = setDirection(line.x1, line.x2)
    const y_direction = setDirection(line.y1, line.y2)

    grid[line.y1][line.x1]++
    while (line.x1 != line.x2 || line.y1 != line.y2) {
        line.y1 += y_direction; line.x1 += x_direction
        grid[line.y1][line.x1]++
    }
})

const sum_overlaps = grid.reduce((sum, row) => { return row.reduce((sum, item) => { return item >= 2 ? ++sum: sum }, sum) }, 0)
console.log("Answer:", sum_overlaps)
// printGrid()
