const fs = require('fs')
const fishes = fs.readFileSync('../data/input.txt').toString().split(',').map(x => parseInt(x))
const fishperDay = new Array(7).fill(0)

fishes.forEach(fish => fishperDay[fish]++)

const newFishOnDeck = new Array(7).fill(0)

for (let i = 0; i < 256; i++) {

    const day = i % 7

    newFishOnDeck[(day+2) % 7] = fishperDay[day]
    fishperDay[day] += newFishOnDeck[day]
    newFishOnDeck[day] = 0

}    

console.log("Total Fish:", fishperDay.reduce((previousValue, currentValue) => previousValue + currentValue) + newFishOnDeck.reduce((previousValue, currentValue) => previousValue + currentValue))
