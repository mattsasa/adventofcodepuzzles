const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split(',').map(x => parseInt(x))

const max = lines.reduce((max, value) => max > value ? max : value, 0)

let cheapest = 100000000
for (let i = 0; i < max; i++) {
    const sum = lines.reduce((previousValue, currentValue) => previousValue + Math.abs(currentValue - i), 0)
    if (sum < cheapest) cheapest = sum
}
console.log(cheapest)