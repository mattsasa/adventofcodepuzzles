const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split(',').map(x => parseInt(x))

const max = lines.reduce((max, value) => max > value ? max : value, 0)

const factorial = (n) => {
    let sum = 0
    for (let i = 0; i <= Math.abs(n); i++) { sum += i }
    return sum
    // return new Array(Math.abs(n)).fill(0).reduce((sum, _, i) => sum + i + 1, 0)   - cute 1-liner, but is much computationally slower and expensive
}


let cheapest = 10000000000000
for (let i = 0; i < max; i++) {
    const sum = lines.reduce((sum, value) => sum + factorial(value - i), 0)
    if (sum < cheapest) cheapest = sum
}

console.log(cheapest)
