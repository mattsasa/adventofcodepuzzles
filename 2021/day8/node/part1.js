const fs = require('fs')
const notes = fs.readFileSync('../data/input.txt').toString().split('\n').map(line => line.split('|')[1].split(' ').slice(1) )
console.log("Part 1:", notes.reduce((sum, note) => note.reduce((sum, digit) => [2, 4, 3, 7].includes(digit.length) ? ++sum : sum, sum), 0))