const fs = require('fs')
const notes = fs.readFileSync('../data/input.txt').toString().split('\n').map(line => {
    const parts = line.split('|')
    return { patterns: parts[0].split(' ').slice(0, -1), output_digits: parts[1].split(' ').slice(1) }
})

const processNote = (total_sum, note) => {

    const digitsByLength = Object.fromEntries(new Array(8).fill(0).map((_, i) => [i, []]).slice(2))
    note.patterns.forEach(digit => { digitsByLength[digit.length].push(digit) })
    const right_verticals = digitsByLength[2][0]
    const top_segment = digitsByLength[3][0].replace(right_verticals[0], "").replace(right_verticals[1], "")

    let top_right_segment, bottom_right_segment, top_left_segment, middle_segment, bottom_segment, bottom_left_segment
    const sum = digitsByLength[6].reduce( (sum, digit) => digit.includes(right_verticals[0]) ? ++sum : sum, 0)
    if (sum == 2) { top_right_segment = right_verticals[0]; bottom_right_segment = right_verticals[1] }
    else { top_right_segment = right_verticals[1]; bottom_right_segment = right_verticals[0] }
    
    const extra_four_segments = digitsByLength[4][0].replace(right_verticals[0], "").replace(right_verticals[1], "")
    const sum2 = digitsByLength[5].reduce( (sum, digit) => digit.includes(extra_four_segments[0]) ? ++sum : sum, 0)
    if (sum2 == 3) { middle_segment = extra_four_segments[0]; top_left_segment = extra_four_segments[1] }
    else { middle_segment = extra_four_segments[1]; top_left_segment = extra_four_segments[0] }

    const used_chars = [top_segment, top_right_segment, bottom_right_segment, top_left_segment, middle_segment]

    const reducedDigits = digitsByLength[5].map(digit => used_chars.reduce((digit, char) => digit = digit.replace(char, ""), digit))
    const leftOverChars = used_chars.reduce((digit, char) => digit = digit.replace(char, ""), "abcdefg")
    const sum3 = reducedDigits.reduce( (sum, digit) => digit.includes(leftOverChars[0]) ? ++sum : sum, 0)
    if (sum3 == 3) { bottom_segment = leftOverChars[0]; bottom_left_segment =  leftOverChars[1] }
    else { bottom_segment = leftOverChars[1]; bottom_left_segment =  leftOverChars[0] }

    const display_value = note.output_digits.reduce((display_value, digit) => {

        switch (digit.length) {
            case 2: return display_value + "1"
            case 3: return display_value + "7"
            case 4: return display_value + "4"
            case 7: return display_value + "8"
            case 5:
                if (digit.includes(top_left_segment)) return display_value + "5"
                else if (digit.includes(bottom_left_segment)) return display_value + "2"
                else return display_value + "3"
            case 6:
                if (!digit.includes(bottom_left_segment)) return display_value + "9"
                else if (!digit.includes(top_right_segment)) return display_value + "6"
                else return display_value + "0"
        }

    }, "")

    return total_sum + parseInt(display_value)

}

console.log("Part 2:", notes.reduce(processNote, 0))


