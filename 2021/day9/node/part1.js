const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt').toString().split('\n').map(line => line.split('').map(x => parseInt(x)))

const checkLowPoint = (h, w) => {
    if (h+1 < grid.length && grid[h][w] >= grid[h+1][w]) return
    if (h-1 >= 0 && grid[h][w] >= grid[h-1][w]) return
    if (w+1 < grid[0].length && grid[h][w] >= grid[h][w+1]) return
    if (w-1 >= 0 && grid[h][w] >= grid[h][w-1]) return
    return true
}

let total_sum = 0
for (let h = 0; h < grid.length; h++) {
    for (let w = 0; w < grid[0].length; w++) {
        if (checkLowPoint(h, w)) total_sum += 1 + grid[h][w]
    }
}

console.log("Part 1:", total_sum)