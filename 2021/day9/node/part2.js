const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt').toString().split('\n').map(line => line.split('').map(x => parseInt(x)))

const checkLowPoint = (h, w) => {
    if (h+1 < grid.length && grid[h][w] >= grid[h+1][w]) return
    if (h-1 >= 0 && grid[h][w] >= grid[h-1][w]) return
    if (w+1 < grid[0].length && grid[h][w] >= grid[h][w+1]) return
    if (w-1 >= 0 && grid[h][w] >= grid[h][w-1]) return
    return true
}

const iterateBFS = (h, w, ends) => {
    let basin_size = 0
    if (h+1 < grid.length && grid[h+1][w] != 9) { ends.push({ h: h+1, w }); grid[h+1][w] = 9; basin_size++ }
    if (h-1 >= 0 && grid[h-1][w] != 9) { ends.push({ h: h-1, w }); grid[h-1][w] = 9; basin_size++ }
    if (w+1 < grid[0].length && grid[h][w+1] != 9) { ends.push({ h, w: w+1 }); grid[h][w+1] = 9; basin_size++ }
    if (w-1 >= 0 && grid[h][w-1] != 9) { ends.push({ h, w: w-1 }); grid[h][w-1] = 9; basin_size++ }
    return basin_size
}

const searchBasin = (h, w) => {

    let ends = [], basin_size = 1
    grid[h][w] = 9

    basin_size += iterateBFS(h, w, ends)

    while (ends.length != 0) {
        let new_ends = []
        basin_size = ends.reduce((accum, end) => accum + iterateBFS(end.h, end.w, new_ends), basin_size)
        ends = new_ends
    }

    return basin_size
}

let basin_sizes = []
for (let h = 0; h < grid.length; h++) {
    for (let w = 0; w < grid[0].length; w++) {
        if (checkLowPoint(h, w)) basin_sizes.push(searchBasin(h, w))
    }
}

basin_sizes.sort((a, b) => a > b ? -1 : 1)

console.log("Part 2:", basin_sizes[0] * basin_sizes[1] * basin_sizes[2])
