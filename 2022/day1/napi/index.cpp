#include <napi.h>
#include <string>
#include <set>
#include <iostream>

void process(const Napi::CallbackInfo& info) {
    Napi::Array napi_arr = info[0].As<Napi::Object>().Get("elf_totals").As<Napi::Array>();
    std::vector<int> arr = std::vector<int>(napi_arr.Length());
    for (size_t i = 0; i < napi_arr.Length(); i++) { Napi::Value item = napi_arr[i]; arr[i] = item.ToNumber(); }

    std::set<int> set;
    for (size_t i = 0; i < napi_arr.Length(); i++) { set.insert(arr[i]); }

    std::set<int>::reverse_iterator it = set.rbegin();
    int sum = 0;
    for (int i = 0; i < 3; i++) {
        sum += *it;
        it = std::next(it, 1);
    }

    std::cout << "C++ part 1: " << *set.rbegin() << std::endl;
    std::cout << "C++ part 2: " << sum << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);