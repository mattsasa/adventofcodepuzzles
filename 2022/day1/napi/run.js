const fs = require("fs")
const elf_totals = fs.readFileSync('../data/input.txt').toString().split("\n\n")
                    .map(x => x.split("\n").map((x) => parseInt(x)))
                    .map(x => x.reduce((accum, x) => x + accum, 0))


// console.log("Part 1: ", Math.max(...elf_totals))
// console.log("Part 2: ", elf_totals.sort((a, b) =>  b - a).slice(0,3).reduce((accum, x) => x + accum, 0))

///Process C++
require('./build/Release/day1.node').process({ elf_totals })



