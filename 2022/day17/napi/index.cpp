#include <napi.h>
#include <string>
#include <iostream>

const uint STATE_SPAWN = 0;
const uint STATE_DROPPING = 1;
const uint STATE_COME_TO_REST = 2;

int total_champer_height = 500000;

std::vector<std::vector<char>> grid(total_champer_height);
std::string input_str;
uint input_index = 0;

struct Offset {
    int h;
    int w;
};

int tower_height = total_champer_height;
Offset shape_bl_corner_to_grid;

bool itemInRow(int h) {
    auto& row = grid[h];
    for (char c: row) {
        if (c == '#') return true;
    }
    return false;
}

Offset findNewSpawnLocation() {
    for (int h = 0; h < grid.size(); h++) {
        if (itemInRow(h)) return { h - 4, 2 };
    }
    return { int(grid.size()) - 4, 2 };
}

std::vector<Offset> HORIZONTAL_LINE = { { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 } };
std::vector<Offset> PLUS_SIGN = { { -1, 1 }, { 0, 1 }, { -2, 1 }, { -1, 0 }, { -1, 2 } };
std::vector<Offset> THE_L = { { 0, 0 }, { 0, 1 }, { 0, 2 }, { -1, 2 }, { -2, 2 }  };
std::vector<Offset> VERTICAL_LINE = { { 0, 0 }, { -1, 0 }, { -2, 0 }, { -3, 0 } };
std::vector<Offset> BLOCK_2x2 = { { 0, 0 }, { 0, 1 }, { -1, 1 }, { -1, 0 } };

std::vector<Offset> current_shape;

std::vector<Offset>& nextShape() {

    static uint shape_index = 0;

    switch (shape_index++ % 5) {
        case 0: 
            return HORIZONTAL_LINE;
        case 1: 
            return PLUS_SIGN;
        case 2: 
            return THE_L;
        case 3: 
            return VERTICAL_LINE;
        case 4: 
            return BLOCK_2x2;
    }
}

char getNextCommand() {
    return input_str[input_index++ % input_str.size()];
}

void initGrid() {
    for (std::vector<char>& row: grid) {
        for (int i = 0; i < 7; i++) { row.push_back('.'); }
    }
}

void printChamber() {

    std::vector<std::vector<char>> t_grid = grid;

    for (Offset offset: current_shape) {
        t_grid[offset.h+shape_bl_corner_to_grid.h][offset.w+shape_bl_corner_to_grid.w] = '@';
    }

    for (std::vector<char>& row: t_grid) {
        std::cout << std::endl << "|";
        for (char c: row) {
            std::cout << c;
        }
        std::cout << '|';
    }
    std::cout << std::endl;
    std::cout << "+-------+" << std::endl;
}

void addCurrendShapeToGrid() {
    int highest_h_offset = 0;
    for (Offset offset: current_shape) {
        grid[offset.h+shape_bl_corner_to_grid.h][offset.w+shape_bl_corner_to_grid.w] = '#';
        if (offset.h < highest_h_offset) highest_h_offset = offset.h;
    }
    if (shape_bl_corner_to_grid.h+highest_h_offset < tower_height) {
        tower_height = shape_bl_corner_to_grid.h+highest_h_offset;
    }
}

void shiftIfPossible(int shift) {
    for (Offset offset: current_shape) {
        if (offset.w+shape_bl_corner_to_grid.w+shift < 0 || offset.w+shape_bl_corner_to_grid.w+shift > 6) {
            return;
        }
        if (grid[offset.h+shape_bl_corner_to_grid.h][offset.w+shape_bl_corner_to_grid.w+shift] == '#') {
            return;
        }
    }
    shape_bl_corner_to_grid.w += shift;

}

bool checkHitBottom() {

    for (Offset offset: current_shape) {
        if (offset.h+shape_bl_corner_to_grid.h+1 >= grid.size()) return true;
        if (grid[offset.h+shape_bl_corner_to_grid.h+1][offset.w+shape_bl_corner_to_grid.w] == '#') {
            return true;
        }
    }
    return false;
}

void process(const Napi::CallbackInfo& info) {
    initGrid();

    input_str = info[0].ToString();

    int state = STATE_SPAWN;
    char direction;
    current_shape = HORIZONTAL_LINE;

    int total_rocks = 0;
    int prev = 0;

    while (total_rocks <= 2022) {

        // printChamber();
        switch (state) {
            case STATE_SPAWN:
                total_rocks++;
                current_shape = nextShape();
                shape_bl_corner_to_grid = { tower_height - 4, 2 };
                state = STATE_DROPPING;
                // printChamber();
                break;
            case STATE_DROPPING:
                direction = getNextCommand();
                shiftIfPossible(direction == '>' ? 1 : -1);
                if (checkHitBottom()) {
                    state = STATE_COME_TO_REST;
                } else {
                    shape_bl_corner_to_grid.h++;
                }
                break;
            case STATE_COME_TO_REST:
                addCurrendShapeToGrid();
                state = STATE_SPAWN;
                break;
        }

    }
    // printChamber();

    std::cout << "Part 1: " << total_champer_height - tower_height << std::endl;
    std::cout << "Part 2: " << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);

//sample.txt
//total_height = 10600*(total_rocks/7000) + 7


///// input.txt
/// 2695 height per input cycle

//// 1735 rocks placed in an input cycle


// input_cycles = total_rocks / 1735
//// tower_height = input_cycles * 2695

//// tower_height = ((total_rocks / 1735) * 2695) - 1

//// 4672 -- actual correct answer for 3000
//// 4658
//// calc
/// 2634 + 1977 = 4611


/// Try to figure out 10,000 = 15561

/// division ~ 5.7 we plug in 5,         + (13474)
/// modulo remainder ~ 1325, we compute  + (2087)
/// ----------------------------------------


/// Try to figure out 1000000000000 = 1553314121019

/// division ~ 576368876.081 we plug in 576368876,         + (1553314120819)
/// modulo remainder ~ 140, we compute                     + (200)
/// ----------------------------------------             =    1553314121019