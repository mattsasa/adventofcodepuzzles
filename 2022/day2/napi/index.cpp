#include <napi.h>
#include <string>
#include <unordered_map>
#include <iostream>

std::unordered_map<char, std::unordered_map<char, int>> scoresTable = {
    {'X', {
        {'A', 4},
        {'B', 1},
        {'C', 7}
    }},
    {'Y', {
        {'A', 8},
        {'B', 5},
        {'C', 2}
    }},
    {'Z', {
        {'A', 3},
        {'B', 9},
        {'C', 6}
    }}
};

std::unordered_map<char, std::unordered_map<char, int>> scoresTable2 = {
    {'X', { // Lose
        {'A', 3},
        {'B', 1},
        {'C', 2}
    }},
    {'Y', { // Tie
        {'A', 4},
        {'B', 5},
        {'C', 6}
    }},
    {'Z', { // Win
        {'A', 8},
        {'B', 9},
        {'C', 7}
    }}
};


void process(const Napi::CallbackInfo& info) {

    int total_score = 0, total_score2 = 0;

    Napi::Array napi_arr = info[0].As<Napi::Array>();
    std::vector<std::pair<char, char>> rounds(napi_arr.Length());
    for (size_t i = 0; i < napi_arr.Length(); i++) {
        Napi::Array pair = static_cast<Napi::Value>(napi_arr[i]).As<Napi::Array>();
        rounds[i].first = (static_cast<Napi::Value>(pair[(uint)0]).ToString().Utf8Value())[0];
        rounds[i].second = (static_cast<Napi::Value>(pair[(uint)1]).ToString().Utf8Value())[0];

        total_score += scoresTable[rounds[i].second][rounds[i].first];
        total_score2 += scoresTable2[rounds[i].second][rounds[i].first];
    }


    std::cout << "Part 1: " << total_score << std::endl;
    std::cout << "Part 2: " << total_score2 << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);