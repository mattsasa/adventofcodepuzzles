const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => [x[0], x[2]])

///Process C++
require('./build/Release/day2.node').process(lines)