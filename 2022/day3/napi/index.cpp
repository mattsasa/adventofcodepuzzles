#include <napi.h>
#include <string>
#include <iostream>
#include <unordered_set>

void process(const Napi::CallbackInfo& info) {

    Napi::Array n_sacks = info[0].As<Napi::Array>();
    std::vector<std::vector<int>> sacks(n_sacks.Length());
    for (size_t i = 0; i < n_sacks.Length(); i++) {
        Napi::Array n_sack = ((Napi::Value)n_sacks[i]).As<Napi::Array>();
        std::vector<int> sack(n_sack.Length());
        for (size_t j = 0; j < n_sack.Length(); j++) {
            Napi::Value item = n_sack[j]; sack[j] = item.ToNumber();
        }
        sacks[i] = sack;
    }

    int sum = 0;

    for (uint i = 0; i < sacks.size(); i++) {
        std::vector<int> sack = sacks[i];
        std::unordered_set<int> set;

        for (uint j = 0; j < sack.size(); j++) {
            if (j < (sack.size() / 2)) {
                set.insert(sack[j]);
            } else {
                if (set.count(sack[j])) {
                    sum += sack[j];
                    break;
                }
            }
        }
    }

    std::unordered_set<int> set0;
    std::unordered_set<int> set1;
    int sum2 = 0;

    for (uint i = 0; i < sacks.size(); i++) {
        std::vector<int> sack = sacks[i];

        for (uint j = 0; j < sack.size(); j++) {

            switch (i % 3) {
                case 0: 
                    set0.insert(sack[j]);
                    break;
                case 1: 
                    set1.insert(sack[j]);
                    break;
                case 2:
                    if (set0.count(sack[j]) && set1.count(sack[j])) {
                        sum2 += sack[j];
                        j = sack.size();
                    }
                    break;
            }
        }

        if (i % 3 == 2) { set0.clear(); set1.clear(); }
    }


    std::cout << "Part 1: " << sum << std::endl;
    std::cout << "Part 2: " << sum2 << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);