const fs = require('fs')
const items = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split("").map(char => {
    let priority = char.charCodeAt(0)
    if (priority < 97) priority -= 38
    else priority -= 96
    return priority
}))
///Process C++
require('./build/Release/day3.node').process(items)