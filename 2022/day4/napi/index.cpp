#include <napi.h>
#include <string>
#include <iostream>

struct Assignment {
    int start1;
    int end1;
    int start2;
    int end2;
};

void process(const Napi::CallbackInfo& info) {

    Napi::Array napi_arr = info[0].As<Napi::Array>();
    std::vector<Assignment> assignments(napi_arr.Length());
    for (size_t i = 0; i < napi_arr.Length(); i++) {
        Napi::Array pair = ((Napi::Value)napi_arr[i]).As<Napi::Array>();
        Napi::Array elf1 = ((Napi::Value)pair[(uint)0]).As<Napi::Array>();
        Napi::Array elf2 = ((Napi::Value)pair[(uint)1]).As<Napi::Array>();

        assignments[i].start1 = ((Napi::Value)elf1[(uint)0]).ToNumber();
        assignments[i].end1 = ((Napi::Value)elf1[(uint)1]).ToNumber();
        assignments[i].start2 = ((Napi::Value)elf2[(uint)0]).ToNumber();
        assignments[i].end2 = ((Napi::Value)elf2[(uint)1]).ToNumber();

    }

    int num = 0;
    for (Assignment assn: assignments) {
        if (assn.start1 == assn.start2 || assn.end1 == assn.end2) { num++; continue; }
        if (assn.start1 < assn.start2) {
            if (assn.end1 > assn.end2) num++;
        } else {
            if (assn.end2 > assn.end1) num++;
        }
    }

    int num2 = 0;
    for (Assignment assn: assignments) {
        if (assn.start1 == assn.start2 || assn.end1 == assn.end2 || assn.start1 == assn.end2 || assn.end1 == assn.start2) { num2++; continue; }
        if (assn.start1 < assn.start2) {
            if (assn.end1 > assn.start2) num2++;
        } else {
            if (assn.end2 > assn.start1) num2++;
        }
    }

    std::cout << "Part 1: " << num << std::endl;
    std::cout << "Part 2: " << num2 << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);