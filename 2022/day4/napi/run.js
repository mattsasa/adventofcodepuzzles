const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt').toString().split('\n').map(x => x.split(",").map(x => x.split("-")))

///Process C++
require('./build/Release/day4.node').process(lines)