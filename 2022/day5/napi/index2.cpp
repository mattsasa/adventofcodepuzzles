#include <napi.h>
#include <string>
#include <iostream>
#include <map>
#include <stack>

void process(const Napi::CallbackInfo& info) {

    Napi::Array napi_instructions = info[0].As<Napi::Array>();
    Napi::Object napi_cratesObj = info[1].As<Napi::Object>();

    std::map<int, std::stack<char>> cratesMap;

    for (const auto& e : napi_cratesObj) {
        int stackId = static_cast<Napi::Value>(e.first).ToNumber();
        cratesMap[stackId] = std::stack<char>();

        Napi::Array stackValues = static_cast<Napi::Value>(e.second).As<Napi::Array>();
        for (size_t i = 0; i < stackValues.Length(); i++) {
            cratesMap[stackId].push( static_cast<Napi::Value>(stackValues[i]).ToString().Utf8Value()[0] );
        }
    }

    struct Instruction {
        int count;
        int source;
        int dest;
        // Instruction(): count(0), source(0), dest(0) {}
        Instruction(int c, int s, int d): count(c), source(s), dest(d) {}
    };

    std::vector<Instruction*> instructions(napi_instructions.Length());
    for (size_t i = 0; i < napi_instructions.Length(); i++) {
        Napi::Object obj = ((Napi::Value)napi_instructions[i]).As<Napi::Object>();
        instructions[i] = new Instruction(obj.Get("count").ToNumber(), obj.Get("source").ToNumber(), obj.Get("dest").ToNumber());
    }

    for (Instruction* step: instructions) {
        std::stack<int> temp;
        while (step->count-- > 0) {
            temp.push(cratesMap[step->source].top());
            cratesMap[step->source].pop();
        }
        while (temp.size() > 0) {
            cratesMap[step->dest].push(temp.top());
            temp.pop();
        }
    }

    std::string result = "";
    for (std::pair<int, std::stack<char>> pair: cratesMap) { result += pair.second.top(); }

    std::cout << "Part 2: " << result << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);