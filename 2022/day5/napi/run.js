const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n\n').map(x => x.split('\n'))

const crateGrid = input[0]

const re = new RegExp(/\d+/g)

const instructions = input[1].map(line => {
    const result = line.match(re)
    return {
        count: result[0],
        source: result[1],
        dest: result[2]
    }
})

const crateBase = crateGrid[crateGrid.length - 1]

const cratesMap = { }

for (let i = 1; i < crateBase.length; i += 4) {
    cratesMap[crateBase[i]] = []
}

for (let row = crateGrid.length - 2; row >= 0; row--) {
    for (let y = 1; y < crateBase.length; y += 4) {
        if (crateGrid[row][y] != ' ') {
            cratesMap[crateBase[y]].push(crateGrid[row][y])
        }
    }
}

///Process C++
require('./build/Release/day5.node').process(instructions, cratesMap)
require('./build/Release/day5-part2.node').process(instructions, cratesMap)