#include <napi.h>
#include <string>
#include <map>
#include <queue>
#include <iostream>


std::queue<char> q;
std::map<char, int> current_chars;

void add(char c) {
    current_chars[c]++;
    q.push(c);
}

void remove(char c) {
    current_chars[c]--;

    if (current_chars[c] <= 0) {
        current_chars.erase(c);
    }
}

void process(const Napi::CallbackInfo& info) {

    std::string input = info[0].ToString();

    int result;

    for (int i = 0; i < input.size(); i++) {
        if (i >= 14) {
            char popped = q.front();
            remove(popped);
            q.pop();
        }

        add(input[i]);

        if (current_chars.size() == 14) {
            result = i + 1;
            break;
        }
    }

    std::cout << "Part 2: " << result << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);