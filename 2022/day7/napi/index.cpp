#include <napi.h>
#include <string>
#include <map>
#include <iostream>

struct File {
    std::string filename;
    int size;
    File(std::string filename_, int size_): filename(filename_), size(size_) {};
};

struct Directory {
    std::string dirname;
    std::vector<Directory*> dirs;
    std::vector<File> files;
    Directory* parent;
    Directory(std::string dirname_, Directory* parent_): dirname(dirname_), parent(parent_) {};
};

Directory* root = new Directory("/", nullptr);

int part1_sum = 0;
long part2_sum = 999999999999999;

std::vector<int> all_dir_sizes;

void freeDirMemory(Directory* dir) {
    for (Directory* dir: dir->dirs) {
        delete dir;
    }
    delete dir;
}

int parseDir(Directory* dir, int depth) {

    // std::string indent = "";
    // for (int i = 0; i < depth; i++) { indent += "  "; }

    // std::cout << indent << "Directory name: " << dir->dirname << std::endl;

    int sum = 0;
    for (File file: dir->files) {
        // std::cout << indent << "File name: " << file.filename << "   size: " << file.size << std::endl;
        sum += file.size;
    }

    for (Directory* dir: dir->dirs) {
        int dir_sum = parseDir(dir, depth + 1);
        sum += dir_sum;
    }

    // std::cout << indent << "Directory Name: " << dir->dirname  <<  " Sum: " << sum << std::endl;
    if (sum <= 100000) part1_sum += sum;
    all_dir_sizes.push_back(sum);
    return sum;

}


void process(const Napi::CallbackInfo& info) {

    Napi::Array input = info[0].As<Napi::Array>();

    Directory* current_dir = root;

    for (auto pair: input) {

        Napi::Object obj = static_cast<Napi::Value>(pair.second).As<Napi::Object>();
        std::string type = obj.Get("type").ToString();

        if (type == "command") {
            std::string command_name = obj.Get("command").ToString();
            std::string arg = obj.Get("argument").ToString();
            if (command_name == "cd") {
                if (arg == "..") { /// go to parent
                    current_dir = current_dir->parent;
                } else if (arg != "/") { // go into new dir
                    for (Directory* dir: current_dir->dirs) {
                        if (dir->dirname == arg) {
                            current_dir = dir;
                            continue;
                        }
                    }
                }
            }
        } else if (type == "file") {
            std::string filename = obj.Get("file_name").ToString();
            int size = obj.Get("size_bytes").ToNumber();
            current_dir->files.push_back(File(filename, size));
        } else if (type == "dir") {
            std::string dirname = obj.Get("directory_name").ToString();
            current_dir->dirs.push_back(new Directory(dirname, current_dir));
        }
    }

    int total_used_bytes = parseDir(root, 0);
    freeDirMemory(root);
    // std::cout << "Total used space: " << total_used_bytes << std::endl;
    int total_free_space = 70000000 - total_used_bytes;
    // std::cout << "Total free space: " << total_free_space << std::endl;

    int space_needed_to_free = 30000000 - total_free_space;
    // std::cout << "Space needed to free up: " << space_needed_to_free << std::endl;

    for (int sum: all_dir_sizes) {
        if (sum >= space_needed_to_free) {
            if (sum < part2_sum) part2_sum = sum;
        }
    }

    std::cout << "Part 1: " << part1_sum << std::endl;
    std::cout << "Part 2: " << part2_sum << std::endl;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(
        Napi::String::New(env, "process"),
        Napi::Function::New(env, process)
    );
    return exports;
}
NODE_API_MODULE(thisCanBeAnythingItDoesntMatter, Init);