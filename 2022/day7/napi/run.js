const fs = require('fs')
const input = fs.readFileSync('../data/input.txt').toString().split('\n').map(line => {
    let parts = line.split(' ')
    if (parts[0] == '$') {  /// command
        return {
            type: "command",
            command: parts[1],
            argument: parts[2]
        }
    } else {  /// data
        if (parts[0] == "dir") {
            return {
                type: "dir",
                directory_name: parts[1]
            }
        } else { /// file
            return {
                type: "file",
                size_bytes: parseInt(parts[0]),
                file_name: parts[1]
            }
        }
    }
})

// console.log(input)

///Process C++
require('./build/Release/day7.node').process(input)
