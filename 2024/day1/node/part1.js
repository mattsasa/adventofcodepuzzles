const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt', 'utf8').split('\n').map(x => x.split("  ").map(x => parseInt(x)))

const leftSet = lines.map(pair => pair[0]).sort()
const rightSet = lines.map(pair => pair[1]).sort()

const sum = leftSet.reduce((acc, val, i) => acc + Math.abs(val - rightSet[i]), 0)

console.log("Part 1 Answer:", sum)