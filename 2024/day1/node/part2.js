const fs = require('fs')
const lines = fs.readFileSync('../data/input.txt', 'utf8').split('\n').map(x => x.split("  ").map(x => parseInt(x)))

const leftSet = lines.map(pair => pair[0]).sort()
const rightSet = lines.map(pair => pair[1]).sort()

const counter = (list, number) => list.reduce((count, el) => count + (el == number), 0)
const sum = leftSet.reduce((acc, val) => acc + val * counter(rightSet, val), 0)

console.log("Part 2 Answer:", sum)