const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split("").map(Number))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char.toString())
        })
        console.log()
    })
    console.log()
}


const trailHeads = []
for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == 0) trailHeads.push({ x, y })
    }
}


const iterateBFS = (end, new_ends) => {

    const { x, y } = end

    const targetElevation = grid[y][x] + 1

    // UP
    if (grid[y - 1] && grid[y - 1][x] == targetElevation) {
        new_ends.push({ x, y: y - 1})
    }
    // DOWN
    if (grid[y + 1] && grid[y + 1][x] == targetElevation) {
        new_ends.push({ x, y: y + 1})
    }
    // LEFT
    if (grid[y] && grid[y][x - 1] == targetElevation) {
        new_ends.push({ x: x - 1, y })
    }
    // RIGHT
    if (grid[y] && grid[y][x + 1] == targetElevation) {
        new_ends.push({ x: x + 1, y })
    }
}

const calcScore = (start) => {

    let ends = [{ x: start.x, y: start.y }]

    for (let steps = 0; steps < 9; steps++) {
        let new_ends = []
        ends.forEach(end => { iterateBFS(end, new_ends) })
        ends = new_ends
    }

    return ends.length
}

const answer = trailHeads.reduce((acc, value) => acc + calcScore(value), 0)

console.log(answer)

