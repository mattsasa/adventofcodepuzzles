const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split(" ").map(Number)

// console.log(input)

let prevLength = 1

let stones = input

for (let i = 0; i < 38; i++) {
    let new_stones = []

    for (const stone of stones) {

        if (stone == 0) {
            new_stones.push(1)
            continue
        }

        const digits = stone.toString()
        if (digits.length % 2 == 0) {
            new_stones.push(parseInt(digits.slice(0, digits.length / 2)))
            new_stones.push(parseInt(digits.slice(digits.length / 2, digits.length)))
            continue
        }

        new_stones.push(stone*2024)

    }

    stones = new_stones
    // console.log(stones)

    // if ((i + 5) % 7 == 0) {
        // prevLength = stones.length
    // }

    console.log("Steps:", i + 1, "Number of Stones:", stones.length)
    
}

// console.log(stones.length)
