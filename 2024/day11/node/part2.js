const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split(" ").map(Number)

// console.log(input)

const stones = input

// for (const stone of stones) {

//     if (stone == 0) {
//         new_stones.push(1)
//         continue
//     }

//     const digits = stone.toString()
//     if (digits.length % 2 == 0) {
//         new_stones.push(parseInt(digits.slice(0, digits.length / 2)))
//         new_stones.push(parseInt(digits.slice(digits.length / 2, digits.length)))
//         continue
//     }

//     new_stones.push(stone*2024)

// }

// stones = new_stones

// console.log("Steps:", i + 1, "Number of Stones:", stones.length)

const theCache = {}


const evolveStone = (stone, remainingSteps) => {

    if (remainingSteps == 0) return 1

    if (theCache[`${stone},${remainingSteps}`]) {
        return theCache[`${stone},${remainingSteps}`]
    }

    const digits = stone.toString()
    if (stone == 0) {
        result = evolveStone(1, remainingSteps - 1)
    } else if (digits.length % 2 == 0) {
        const left = parseInt(digits.slice(0, digits.length / 2))
        const right = parseInt(digits.slice(digits.length / 2))
        result = evolveStone(left, remainingSteps - 1) + evolveStone(right, remainingSteps - 1)
    } else {
        result = evolveStone(stone * 2024, remainingSteps - 1)
    }

    theCache[`${stone},${remainingSteps}`] = result
    return result
}

let sum = 0
for (const stone of stones) {
    sum += evolveStone(stone, 75)    
}
console.log(sum)



