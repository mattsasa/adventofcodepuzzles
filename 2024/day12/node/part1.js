const fs = require('fs')
const inputGrid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

// printGrid(inputGrid)

const updateFencesForPlot = (localGrid, x, y) => {

    const plot = localGrid[y][x]
    plot.numFences = 4

    const neighbors = [
        [0, -1], // UP
        [0, 1],  // DOWN
        [-1, 0], // LEFT
        [1, 0],   // RIGHT
    ];

    neighbors.forEach(([dx, dy]) => {
        const neighbor = localGrid[y + dy]?.[x + dx]
        if (neighbor && neighbor.plantType == plot.plantType) {
            plot.numFences--
        }
    })

}


const updateFences = (localGrid, x, y) => {
    const directions = [
        [0, 0],   // Current plot
        [0, 1],   // UP
        [0, -1],  // DOWN
        [1, 0],   // RIGHT
        [-1, 0]   // LEFT
    ]

    directions.forEach(([dx, dy]) => {
        if (localGrid[y + dy] && localGrid[y + dy][x + dx]) {
            updateFencesForPlot(localGrid, x + dx, y + dy)
        }
    })
}

const iterateBFS = (end, new_ends, localGrid) => { 
    const { x, y } = end;
    const plantType = localGrid[y][x].plantType;

    inputGrid[y][x] = '.';

    const directions = [
        [0, -1], // UP
        [0, 1],  // DOWN
        [-1, 0], // LEFT
        [1, 0]   // RIGHT
    ];

    directions.forEach(([dx, dy]) => {
        const nx = x + dx, ny = y + dy
        if (inputGrid[ny]?.[nx] == plantType && localGrid[ny][nx] == null) {
            localGrid[ny][nx] = { plantType }
            updateFences(localGrid, nx, ny)
            new_ends.push({ x: nx, y: ny })
        }
    })
}

/// Create the regions of plants of same type
const regions = []
for (let y = 0; y < inputGrid.length; y++) {
    for (let x = 0; x < inputGrid[0].length; x++) {

        if (inputGrid[y][x] != '.') {
            // Not previously visted

            const localGrid = Array(inputGrid.length).fill().map(() => Array(inputGrid[0].length).fill(null))
            localGrid[y][x] = { plantType: inputGrid[y][x], numFences: 4 }

            let ends = [{x, y}]

            while (ends.length > 0) {
                let new_ends = []
                ends.forEach(end => { iterateBFS(end, new_ends, localGrid) })
                ends = new_ends
            }

            regions.push(localGrid)

        }

    }
}

// console.log(regions[1]);


let sum = 0
for (const region of regions) {
    let area = 0, perimeter = 0
    for (let y = 0; y < inputGrid.length; y++) {
        for (let x = 0; x < inputGrid[0].length; x++) {
            if (region[y][x]) {
                area++
                perimeter += region[y][x].numFences
            }
        }
    }
    sum += (area * perimeter)
}

console.log("Part 1 Total Cost:", sum)
