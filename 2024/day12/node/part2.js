const fs = require('fs')
const inputGrid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

// printGrid(inputGrid)

const updateFencesForPlot = (localGrid, x, y) => {

    const plot = localGrid[y][x]

    plot.numCorners = 0

    const cornerChecks = [
        [-1, -1], // TOP LEFT
        [-1, 1],  // BOTTOM LEFT
        [1, -1], // TOP RIGHT
        [1, 1]   // BOTTOM RIGHT
    ]

    //// Outer Corners
    cornerChecks.forEach(([dx, dy]) => {
        const neighbor1 = localGrid[y + dy]?.[x]
        const neighbor2 = localGrid[y]?.[x + dx]
        const isNeighbor1Different = neighbor1 == undefined || neighbor1.plantType != plot.plantType
        const isNeighbor2Different = neighbor2 == undefined || neighbor2.plantType != plot.plantType
        if (isNeighbor1Different && isNeighbor2Different) {
            plot.numCorners++
        }
    })

    //// Inner Corners
    cornerChecks.forEach(([dx, dy]) => {
        const neighbor1 = localGrid[y + dy]?.[x]
        const neighbor2 = localGrid[y]?.[x + dx]
        const isNeighbor1Different = neighbor1 == undefined || neighbor1.plantType != plot.plantType
        const isNeighbor2Different = neighbor2 == undefined || neighbor2.plantType != plot.plantType

        const neighbor3 = localGrid[y + dy]?.[x + dx]
        const isNeighbor3Different = neighbor3 == undefined || neighbor3.plantType != plot.plantType

        if (!isNeighbor1Different && !isNeighbor2Different && isNeighbor3Different) {
            plot.numCorners++
        }
    })

}


const updateFences = (localGrid, x, y) => {
    const directions = [
        [0, 0],   // Current plot
        [0, 1],   // UP
        [0, -1],  // DOWN
        [1, 0],   // RIGHT
        [-1, 0],   // LEFT

        [-1, -1], // TOP LEFT
        [-1, 1],  // BOTTOM LEFT
        [1, -1], // TOP RIGHT
        [1, 1]   // BOTTOM RIGHT
    ]

    directions.forEach(([dx, dy]) => {
        if (localGrid[y + dy] && localGrid[y + dy][x + dx]) {
            updateFencesForPlot(localGrid, x + dx, y + dy)
        }
    })
}

const iterateBFS = (end, new_ends, localGrid) => { 
    const { x, y } = end;
    const plantType = localGrid[y][x].plantType;

    inputGrid[y][x] = '.';

    const directions = [
        [0, -1], // UP
        [0, 1],  // DOWN
        [-1, 0], // LEFT
        [1, 0]   // RIGHT
    ]

    directions.forEach(([dx, dy]) => {
        const nx = x + dx, ny = y + dy
        if (inputGrid[ny]?.[nx] == plantType && localGrid[ny][nx] == null) {
            localGrid[ny][nx] = { plantType }
            updateFences(localGrid, nx, ny)
            new_ends.push({ x: nx, y: ny })
        }
    })
}

/// Create the regions of plants of same type
const regions = []
for (let y = 0; y < inputGrid.length; y++) {
    for (let x = 0; x < inputGrid[0].length; x++) {

        if (inputGrid[y][x] != '.') {
            // Not previously visted

            const localGrid = Array(inputGrid.length).fill().map(() => Array(inputGrid[0].length).fill(null))
            localGrid[y][x] = { plantType: inputGrid[y][x], numCorners: 4 }

            let ends = [{x, y}]

            while (ends.length > 0) {
                let new_ends = []
                ends.forEach(end => { iterateBFS(end, new_ends, localGrid) })
                ends = new_ends
            }

            regions.push(localGrid)

        }

    }
}

// console.log(regions[1]);


let sum = 0
for (const region of regions) {
    let area = 0, perimeter = 0
    for (let y = 0; y < inputGrid.length; y++) {
        for (let x = 0; x < inputGrid[0].length; x++) {
            if (region[y][x]) {
                area++
                perimeter += region[y][x].numCorners
            }
        }
    }
    sum += (area * perimeter)
}

console.log("Part 2 Total Cost:", sum)
