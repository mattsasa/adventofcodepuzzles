const fs = require('fs')
const machines = fs.readFileSync('../data/input.txt', 'utf8').split("\n\n").map(data => {
    const lines = data.split('\n')

    const ax =  parseInt(lines[0].match(/X\+(\d+),/)[1])
    const ay =  parseInt(lines[0].match(/Y\+(\d+)$/)[1])

    const bx =  parseInt(lines[1].match(/X\+(\d+),/)[1])
    const by =  parseInt(lines[1].match(/Y\+(\d+)$/)[1])

    const targetX = parseInt(lines[2].match(/X\=(\d+),/)[1]) + 10000000000000
    const targetY = parseInt(lines[2].match(/Y\=(\d+)$/)[1]) + 10000000000000

    return { ax, ay, bx, by, targetX, targetY }
})

const modInverse = (a, m) => {
    let m0 = m
    let x0 = 0, x1 = 1

    if (m === 1) return 0 // No inverse exists if modulus is 1.

    while (a > 1) {
        // Compute quotient
        let q = Math.floor(a / m)

        // Update values using the Euclidean algorithm
        let t = m
        m = a % m
        a = t
        t = x0
        x0 = x1 - q * x0
        x1 = t
    }

    // Make x1 positive
    if (x1 < 0) x1 += m0

    return x1
}

const gcd = (a, b) => {
    while (b !== 0) {
        let temp = b
        b = a % b
        a = temp
    }
    return a
}

const gcdThree = (a, b, c) => {
    return gcd(gcd(a, b), c)
}


const solveLinearEquations = (A1, B1, C1, A2, B2, C2) => {
    const D = A1 * B2 - A2 * B1;    
    
    if (D === 0) {
        // Lines are parallel or coincident
        if ((A1 * C2 === A2 * C1) && (B1 * C2 === B2 * C1)) {
            return "Infinite solutions";
        } else {
            return "No solution";
        }
    } else {

        const x = (C1 * B2 - C2 * B1) / D;
        const y = (A1 * C2 - A2 * C1) / D;
        
        return { x, y };
    }
}
  

const processMachine = ({ax, ay, bx, by, targetX, targetY}) => {

    /// 7870 - 84b === 0 (mod 17)
    /// targetX - bx*b === 0 (mod ax)

    let mod = ax    
    let theConst = targetX % mod
    let bCoeff = bx % mod
    

    let divisor = gcdThree(theConst, bCoeff, mod)
    mod /= divisor
    bCoeff /= divisor
    theConst /= divisor

    if (gcd(bCoeff, mod) != 1) {
        return null
    }

    theConst *= modInverse(bCoeff, mod)
    theConst %= mod

    /// b = bTerm + bK1Co*k1
    const bTerm = theConst
    const bK1Co = mod

    /// a = aTerm + aK1Co*k1
    const aTerm = (targetX - (bx*theConst)) / ax
    const aK1Co = (-bx * mod) / ax

    // console.log("a=", aTerm, "+", aK1Co, "k1");
    // console.log("b=", bTerm, "+", bK1Co, "k1");

    // The Y Axis
    mod = ay    
    theConst = targetY % mod
    bCoeff = by % mod

    divisor = gcdThree(theConst, bCoeff, mod)
    mod /= divisor
    bCoeff /= divisor
    theConst /= divisor
    
    if (gcd(bCoeff, mod) != 1) {
        return null
    }

    theConst *= modInverse(bCoeff, mod)
    theConst %= mod

    /// b = bTerm2 + bK1Co2*k2
    const bTerm2 = theConst
    const bK1Co2 = mod

    /// a = aTerm2 + aK1Co2*k2
    const aTerm2 = (targetY - (by*theConst)) / ay
    const aK1Co2 = (-by * mod) / ay

    // console.log("a=", aTerm2, "+", aK1Co2, "k2");
    // console.log("b=", bTerm2, "+", bK1Co2, "k2");

    //k1 is a, k2 is b

    const a1 = aK1Co
    const b1 = -aK1Co2
    const c1 = aTerm2 - aTerm

    // console.log(a1, b1, c1)

    const a2 = bK1Co
    const b2 = -bK1Co2
    const c2 = bTerm2 - bTerm

    // console.log(a2, b2, c2)

    const result = solveLinearEquations(a1, b1, c1, a2, b2, c2)    

    if (!Number.isInteger(result.x) || !Number.isInteger(result.y)) {
        return null
    }

    /// x: k1, y: k2
    // console.log(result)

    const aButtons = aTerm + (aK1Co*result.x)
    const bButtons = bTerm + (bK1Co*result.x)

    return { a: aButtons, b: bButtons }
    
}


let sum = 0
for (const machine of machines) {
    const result = processMachine(machine)
    if (result) {
        sum += 3*result.a + result.b
    }
}

console.log("Answer", sum)
