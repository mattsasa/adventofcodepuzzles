const fs = require('fs')
const robots = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(line => {
    const [posPart, velPart] = line.split(' v=')
    return { 
        pos: posPart.slice(2).split(",").map(Number),
        vel: velPart.split(",").map(Number)
    }
})

const tileWidth = 101
const tileHeight = 103

const iterations = 100

let q1 = 0, q2 = 0, q3 = 0, q4 = 0

for (const robot of robots) {
    const xShift = (Math.abs(robot.vel[0]) * iterations) % tileWidth
    const yShift = (Math.abs(robot.vel[1]) * iterations) % tileHeight

    const newX = (robot.pos[0] + (robot.vel[0] < 0 ? -xShift : xShift) + tileWidth) % tileWidth
    const newY = (robot.pos[1] + (robot.vel[1] < 0 ? -yShift : yShift) + tileHeight) % tileHeight

    const xMiddle = Math.floor(tileWidth / 2)
    const yMiddle = Math.floor(tileHeight / 2)

    if (newX < xMiddle && newY < yMiddle) q1++
    if (newX > xMiddle && newY < yMiddle) q2++

    if (newX < xMiddle && newY > yMiddle) q3++
    if (newX > xMiddle && newY > yMiddle) q4++
}


console.log("Part 1 Answer:", q1*q2*q3*q4)