const fs = require('fs')
const robots = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(line => {
    const [posPart, velPart] = line.split(' v=')
    return { 
        pos: posPart.slice(2).split(",").map(Number),
        vel: velPart.split(",").map(Number)
    }
})

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

const tileWidth = 101
const tileHeight = 103

const iterateRobot = (robot) => {

    const xShift = (Math.abs(robot.vel[0])) % tileWidth
    const yShift = (Math.abs(robot.vel[1])) % tileHeight

    const newX = (robot.pos[0] + (robot.vel[0] < 0 ? -xShift : xShift) + tileWidth) % tileWidth
    const newY = (robot.pos[1] + (robot.vel[1] < 0 ? -yShift : yShift) + tileHeight) % tileHeight

    robot.pos[0] = newX
    robot.pos[1] = newY
}

const hasConsecutiveX = (grid) => {
    for (const row of grid) {
        let consecutiveCount = 0

        for (const cell of row) {
            if (cell === 'X') {
                consecutiveCount++;
                if (consecutiveCount == 10) return true
            } else {
                consecutiveCount = 0
            }
        }
    }
    return false
}

const iterations = 10000

for (let i = 1; i < iterations; i++) {
    const grid = Array(tileHeight).fill().map(() => Array(tileWidth).fill('.'))

    for (const robot of robots) {
        iterateRobot(robot)
        grid[robot.pos[1]][robot.pos[0]] = 'X'
    }

    if (hasConsecutiveX(grid)) {
        // printGrid(grid)
        console.log("Part 2 Answer: ", i)
        break
    }

}
