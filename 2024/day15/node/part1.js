const fs = require('fs')
const [gridStr, movesStr] = fs.readFileSync('../data/input.txt', 'utf8').split("\n\n")

const grid = gridStr.split("\n").map(x => x.split(""))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

const robotLocation = {}

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == '@') {
            robotLocation.x = x
            robotLocation.y = y
        }
    }
}

const robotMoves = movesStr.replace(/\n+/g, '')

const checkForAllowedMovement = (tl, dx, dy) => {

    if (grid[tl.y][tl.x] == '#') return false

    if (grid[tl.y][tl.x] == '.') return tl

    tl.x += dx
    tl.y += dy
    return checkForAllowedMovement(tl, dx, dy)

}

const moveRobot = (dx, dy) => {
    const tl = { x: robotLocation.x + dx, y: robotLocation.y + dy }
    grid[robotLocation.y][robotLocation.x] = '.'
    robotLocation.x += dx
    robotLocation.y += dy
    grid[tl.y][tl.x] = '@'

}


const tryMove = (dx, dy) => {

    const tl = { x: robotLocation.x + dx, y: robotLocation.y + dy }

    if (grid[tl.y][tl.x] == '#') return

    if (grid[tl.y][tl.x] == '.') {
        moveRobot(dx, dy)
        return
    }

    // Assume will be a box (O).  Might move boxes or not.
    const freeSpace = checkForAllowedMovement(tl, dx, dy)

    if (!freeSpace) return

    while (true) {
        const adjacentSpace = { x: freeSpace.x - dx, y: freeSpace.y - dy }

        // adjacentSpace should be robot or box
        if (grid[adjacentSpace.y][adjacentSpace.x] == '@') {
            moveRobot(dx, dy)
            break
        }

        /// is box
        grid[freeSpace.y][freeSpace.x] = 'O'
        grid[adjacentSpace.y][adjacentSpace.x] = '.'
        freeSpace.x -= dx
        freeSpace.y -= dy
    }

}


for (const move of robotMoves) {

    switch (move) {
        case '<':
            tryMove(-1, 0)
            break
        case '>':
            tryMove(1, 0)
            break
        case '^':
            tryMove(0, -1)
            break
        case 'v':
            tryMove(0, 1)
            break
    }
}

// printGrid(grid)


let sum = 0
for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == 'O') {
            sum += (y * 100) + x
        }
    }
}

console.log("Part 1 Answer:", sum)