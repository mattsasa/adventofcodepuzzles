const fs = require('fs')
const [gridStr, movesStr] = fs.readFileSync('../data/input.txt', 'utf8').split("\n\n")

const grid = gridStr.split("\n").map(x => {
    return x.split("").map(x => {
        switch (x) {
            case '#':
                return ["#","#"]
            case '.':
                return [".","."]
            case '@':
                return ["@","."]
            case 'O':
                return ["[","]"]
        }
    }).flat()
})

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

const robotLocation = {}

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == '@') {
            robotLocation.x = x
            robotLocation.y = y
        }
    }
}

const robotMoves = movesStr.replace(/\n+/g, '')


const callBoxRecursionAndMerge = (theHitBox, dx, dy, theReturnSet) => {
    const newSetOfBoxes = getAllBoxes2moveOrFail(theHitBox, dx, dy)
    if (!newSetOfBoxes) {
        return false
    }
    newSetOfBoxes.forEach(box => theReturnSet.add(box))
    return true
}

const getAllBoxes2moveOrFail = (box, dx, dy) => {
    /// return false for wall
    /// return set of boxes with just 1 item (itself) (success no other boxes to move)
    /// return a set of other boxes to move including itself

    const tl = { x: box.x + dx, y: box.y + dy }

    const theReturnSet = new Set()
    theReturnSet.add(`${box.x},${box.y}`)

    if (dy == 0) {
        //horizontal move
        if (dx == 1 ? (grid[tl.y][tl.x + 1] == '#') : (grid[tl.y][tl.x] == '#')) {
            // hit wall
            return false
        }

        if (dx == 1) {
            // right
            if (grid[tl.y][tl.x + 1] == '[') {
                //hit a box
                const theHitBox = { x: tl.x + 1, y: tl.y }
                if (!callBoxRecursionAndMerge(theHitBox, dx, dy, theReturnSet)) {
                    return false
                }

            }
        } else {
            // left
            if (grid[tl.y][tl.x] == ']') {
                //hit a box
                const theHitBox = { x: tl.x - 1, y: tl.y }
                if (!callBoxRecursionAndMerge(theHitBox, dx, dy, theReturnSet)) {
                    return false
                }
            }
        }

    } else {

        //vertical move
        if ((grid[tl.y][tl.x] == '#') || (grid[tl.y][tl.x + 1] == '#')) {
            // hit wall
            return false    
        }

        if (grid[tl.y][tl.x] != '.') {
            //hit a box
            const theHitBox = { x: (grid[tl.y][tl.x] == '[' ? tl.x : tl.x - 1), y: tl.y }
            if (!callBoxRecursionAndMerge(theHitBox, dx, dy, theReturnSet)) {
                return false
            }
        }

        if (grid[tl.y][tl.x + 1] == '[') { // more strict check for this side
            //hit a box
            const theHitBox = { x: tl.x + 1, y: tl.y }
            if (!callBoxRecursionAndMerge(theHitBox, dx, dy, theReturnSet)) {
                return false
            }
        }
    }

    return theReturnSet
}

const moveRobot = (dx, dy) => {
    const tl = { x: robotLocation.x + dx, y: robotLocation.y + dy }
    grid[robotLocation.y][robotLocation.x] = '.'
    robotLocation.x += dx
    robotLocation.y += dy
    grid[tl.y][tl.x] = '@'
}

const moveBox = (boxLocation, dx, dy) => {
    const tl = { x: boxLocation.x + dx, y: boxLocation.y + dy }
    grid[boxLocation.y][boxLocation.x] = '.'
    grid[boxLocation.y][boxLocation.x + 1] = '.'
    grid[tl.y][tl.x] = '['
    grid[tl.y][tl.x + 1] = ']'
}

const sortFunctionsByDirection = {
    "-1,0": (a, b) => a.x - b.x,
    "1,0": (a, b) => b.x - a.x,
    "0,-1": (a, b) => a.y - b.y,
    "0,1": (a, b) => b.y - a.y,
}

const boxSetToSortedList = (set, dx, dy) => {

    const sortFunc = sortFunctionsByDirection[`${dx},${dy}`]
    return [...set]
        .map(box => {
            const [x, y] = box.split(',').map(Number)
            return { x, y }
        })
        .sort(sortFunc)
}

const tryMove = (dx, dy) => {

    const tl = { x: robotLocation.x + dx, y: robotLocation.y + dy }
    const targetCellValue = grid[tl.y][tl.x]

    // Check if robot hits wall
    if (targetCellValue == '#') return

    // Robot moves into empty space without moving a box
    if (targetCellValue == '.') {
        moveRobot(dx, dy)
        return
    }

    // If we are here the robot must be hitting a box
    const boxLocation = { x: (targetCellValue == '[' ? tl.x : tl.x - 1),  y: tl.y }
    const allBoxes2move = getAllBoxes2moveOrFail(boxLocation, dx, dy)

    if (allBoxes2move) {
        boxSetToSortedList(allBoxes2move, dx, dy).forEach(box => moveBox(box, dx, dy))
        moveRobot(dx, dy)
    }
}


for (const move of robotMoves) {

    switch (move) {
        case '<':
            tryMove(-1, 0)
            break
        case '>':
            tryMove(1, 0)
            break
        case '^':
            tryMove(0, -1)
            break
        case 'v':
            tryMove(0, 1)
            break
    }
}

// printGrid(grid)


let sum = 0
for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == '[') {
            sum += (y * 100) + x
        }
    }
}

console.log("Part 2 Answer:", sum)