const fs = require('fs')
const inputGrid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

// printGrid(inputGrid)

const scoreGrid = inputGrid.map(line => line.map(() => Infinity))

const endTile = {}

let currentFrontier = []

/// Add the first node to frontier
for (let y = 0; y < inputGrid.length; y++) {
    for (let x = 0; x < inputGrid[0].length; x++) {
        if (inputGrid[y][x] == 'S') { scoreGrid[y][x] = 0; currentFrontier.push({ x, y, direction: '>', score: 0, lastStepWasRotation: false }) }
        if (inputGrid[y][x] == 'E') { endTile.x = x; endTile.y = y; }
    }
}

const directionMap = {
    '^': { dx: 0, dy: -1 },
    'v': { dx: 0, dy: 1 },
    '<': { dx: -1, dy: 0 },
    '>': { dx: 1, dy: 0 }
}

const iterateBFS = (node, nextFrontier) => {

    const { x, y, direction, score, lastStepWasRotation } = node

    // Reached Goal
    if (inputGrid[y][x] == 'E') return

    if (!lastStepWasRotation) {
        const nextDirections = (direction == '^' || direction == 'v') ? ['<', '>'] : ['v', '^']

        nextFrontier.push({ x, y, direction: nextDirections[0], score: score + 1000, lastStepWasRotation: true })
        nextFrontier.push({ x, y, direction: nextDirections[1], score: score + 1000, lastStepWasRotation: true })
    }


    const { dx, dy } = directionMap[direction]

    const newX = x + dx
    const newY = y + dy

    if (inputGrid[newY][newX] == '.' || inputGrid[newY][newX] == 'E') {
        if (score + 1 < scoreGrid[newY][newX]) {
            // better score
            scoreGrid[newY][newX] = score + 1
            nextFrontier.push({ x: newX, y: newY, direction, score: score + 1, lastStepWasRotation: false })
        }
    }

}

while (currentFrontier.length > 0) {
    let nextFrontier = []
    currentFrontier.forEach(node => { iterateBFS(node, nextFrontier) })
    currentFrontier = nextFrontier
}

console.log("Part 1 Answer:", scoreGrid[endTile.y][endTile.x])
