const fs = require('fs')
const [ registerStrs, programStr ] = fs.readFileSync('../data/input.txt', 'utf8').split("\n\n")
const registers = (([a, b, c]) => ({ a, b, c }))(registerStrs.split("\n").map(x => Number(x.slice(12))))

registers.ip = 0

const program = programStr.slice(9).split(",").map(Number)

// console.log(registers);
// console.log(program);

const getComboOperandValue = (comboNumber) => {
    switch (comboNumber) {
        case 0:
        case 1:
        case 2:
        case 3:
            return comboNumber
        case 4:
            return registers.a
        case 5:
            return registers.b
        case 6:
            return registers.c
        case 7:
            console.log("ERROR unknown value");
            process.exit()
    }
}

let output = []

while (registers.ip < program.length) {

    const opcode = program[registers.ip]
    const operand = program[registers.ip + 1]

    // console.log("index:", registers.ip, "opcode:", opcode, "operand:", operand);
    // console.log(registers);    

    let didJump = false

    switch (opcode) {

        // adv
        case 0:    
            registers.a = Math.floor(registers.a / Math.pow(2, getComboOperandValue(operand)))            
            break

        // bxl
        case 1:
            registers.b ^= operand
            break

        // bst
        case 2:
            registers.b = getComboOperandValue(operand) % 8
            break
        
        // jnz
        case 3:
            if (registers.a != 0) {                
                registers.ip = operand
                didJump = true
            }
            break

        // bxc
        case 4:
            registers.b ^= registers.c
            break

        // out
        case 5:
            output.push(getComboOperandValue(operand) % 8)
            break

        // bdv
        case 6:
            registers.b = Math.floor(registers.a / Math.pow(2, getComboOperandValue(operand)))  
            break

        // cdv
        case 7:
            registers.c = Math.floor(registers.a / Math.pow(2, getComboOperandValue(operand)))
            break

    }

    if (!didJump) {
        registers.ip += 2
    }

    // console.log(registers);
    // console.log("-----");
    
    
}

console.log("Part 1 Answer", output)