
const fs = require('fs')
const [ _, programStr ] = fs.readFileSync('../data/input.txt', 'utf8').split("\n\n")

const program = programStr.slice(9).split(",").map(Number)

const bigIntXOR = (a, b) => {
    // Convert BigInts to binary strings
    let aBin = a.toString(2)
    let bBin = b.toString(2)
  
    // Pad so both strings are the same length
    const maxLen = Math.max(aBin.length, bBin.length)
    aBin = aBin.padStart(maxLen, '0')
    bBin = bBin.padStart(maxLen, '0')
  
    // XOR bit by bit
    let resultBits = ''
    for (let i = 0; i < maxLen; i++) {
      // If bits are the same, it's '0'; if different, it's '1'
      resultBits += (aBin[i] === bBin[i]) ? '0' : '1'
    }
  
    // Convert result binary string back to BigInt
    return BigInt('0b' + resultBits)
}


const output = program

const iterateBFS = (a, i, nextFrontier) => {
    for (let j = 0; j < 8; j++) {
        let candidate_a = (a * BigInt(8)) + BigInt(j)
        let b = candidate_a % BigInt(8)
        b = bigIntXOR(b, 2)
        let c = candidate_a / (BigInt(2) ** b)
        b = bigIntXOR(b, 7)
        b = bigIntXOR(b, c)

        if (b % BigInt(8) == BigInt(output[i])) {
            nextFrontier.push(candidate_a)
        }
    }
}

let currentFrontier = [ BigInt(0) ] 
for (let i = output.length - 1; i >= 0; i--) {
    let nextFrontier = []
    currentFrontier.forEach(node => { iterateBFS(node, i, nextFrontier) })
    currentFrontier = nextFrontier
}

const smallestValue = currentFrontier.reduce((acc, current) => current < acc ? current : acc, Infinity)
console.log("Smallest Possible Value:", Number(smallestValue))
