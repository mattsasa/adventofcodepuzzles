const fs = require('fs')
const reports = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(' ').map(Number))


const processReport = (levels) => {

    let direction = -1

    if (levels[1] > levels[0]) {
        /// increasing
        direction = 1
    }

    for (let i = 0; i < levels.length - 1; i++) {

        const diff = Math.abs(levels[i] - levels[i+1])

        if (diff < 1 || diff > 3) return false // unsafe

        if (levels[i+1] > levels[i]) {
            //increasing
            if (direction != 1) return false // unsafe
        } else {
            //decreasing
            if (direction != -1) return false // unsafe
        }

    }

    return true

}

const processReportVariations = (levels) => {

    if (processReport(levels)) return true

    for (let i = 0; i < levels.length; i++) {
        const mod = [...levels.slice(0,i),  ...levels.slice(i+1)]
        if (processReport(mod)) return true
    }

    return false
}


const sum = reports.reduce((acc, val) => { return acc + processReportVariations(val)}, 0)
console.log("Part 2 Answer:", sum)