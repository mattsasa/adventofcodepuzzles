const fs = require('fs')

const locks = []
const keys = []

fs.readFileSync('../data/input.txt', 'utf8').split("\n\n").forEach(data => {

    const grid = data.split("\n")

    if (grid[0][0] == '.') {
        // Key
        const heights = []
        for (let c = 0; c < 5; c++) {
            for (let h = 0; h <= 6; h++) {
                if (grid[6 - h][c] == '.') {
                    heights.push(h - 1)
                    break
                }
            }
        }
        keys.push(heights)

    } else if (grid[0][0] == '#') {
        // Lock
        const heights = []
        for (let c = 0; c < 5; c++) {
            for (let h = 0; h <= 6; h++) {
                if (grid[h][c] == '.') {
                    heights.push(h - 1)
                    break
                }
            }
        }
        locks.push(heights)

    } else {
        console.log("error")
        process.exit()
    }
})


const checkForOverlap = (lock, key) => {
    let overlap = false
    for (let i = 0; i < 5; i++) {
        if (lock[i] + key[i] > 5) {
            overlap = true
        }
    }
    return overlap
}

let validPairs = 0
for (const key of keys) {
    for (const lock of locks) {
        if (!checkForOverlap(lock, key)) {
            validPairs++
        }
    }
}
console.log("Part 1 Answer:", validPairs)