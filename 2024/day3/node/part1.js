const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8')

const myregex = /mul\(\d+,\d+\)/g

const matches = input.match(myregex)

let sum = 0

matches.forEach(str => {
    str = str.slice(4, -1)
    let numbers = str.split(",").map(Number)
    sum += (numbers[0] * numbers[1])
})

console.log("Part 1 Answer:", sum)