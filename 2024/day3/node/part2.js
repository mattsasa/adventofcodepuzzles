const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8')

const myregex = /(mul\(\d+,\d+\)|do\(\)|don't\(\))/

let code = input
let enabled = true
let sum = 0

while (code.match(myregex)) {
    let theMatch = code.match(myregex)
    code = code.slice(theMatch.index+1)
    if (theMatch[0] == "do()") {
        enabled = true
    } else if (theMatch[1] == "don't()") {
        enabled = false
    } else if (enabled) {
        let str = theMatch[0].slice(4, -1)
        let numbers = str.split(",").map(Number)
        sum += (numbers[0] * numbers[1])    
    }
}

console.log("Part 2 Answer:", sum)