const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

const horizontal = input.map(row => row.join(""))

const vertical = []
for (let h = 0; h < input[0].length; h++) {
    let col = "";
    for (let v = 0; v < input.length; v++) {
        col += input[v][h];
    }
    vertical.push(col)
}

const diagonal = []
for (let h = 0; h < input[0].length - 3; h++) {
    for (let v = 0; v < input.length - 3; v++) {
        let diag = "";
        for (let h2 = h, v2 = v; v2 < v + 4; h2++, v2++) {
            diag += input[v2][h2];
        }
        diagonal.push(diag)
    }
}

const reverseDiagonal = []
for (let h = 3; h < input[0].length; h++) {
    for (let v = 0; v < input.length - 3; v++) {
        let diag = "";
        for (let h2 = h, v2 = v; v2 < v + 4; h2--, v2++) {
            diag += input[v2][h2];
        }
        reverseDiagonal.push(diag)
    }
}

const allStrings = [...horizontal, ...vertical, ...diagonal, ...reverseDiagonal]

const sum = allStrings.reduce((acc, string) => acc + (string.match(/XMAS/g) || []).length + (string.match(/SAMX/g) || []).length, 0)

console.log("Part 1 Answer:", sum)