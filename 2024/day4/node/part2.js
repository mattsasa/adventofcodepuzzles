const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

const diagonalCoords = []
for (let h = 0; h < input[0].length - 2; h++) {
    for (let v = 0; v < input.length - 2; v++) {
        let diag = "";
        for (let h2 = h, v2 = v; v2 < v + 3; h2++, v2++) {
            diag += input[v2][h2];
        }
        if (diag == "MAS" || diag == "SAM") {
            diagonalCoords.push({ h, v })
        }
    }
}

const reverseDiagonalCoords = []
for (let h = 2; h < input[0].length; h++) {
    for (let v = 0; v < input.length - 2; v++) {
        let diag = "";
        for (let h2 = h, v2 = v; v2 < v + 3; h2--, v2++) {
            diag += input[v2][h2];
        }
        if (diag == "MAS" || diag == "SAM") {
            reverseDiagonalCoords.push(`${h},${v}`)
        }
    }
}


const sum = diagonalCoords.reduce((acc, coord) => acc + (reverseDiagonalCoords.includes(`${coord.h + 2},${coord.v}`)), 0)
console.log("Part 2 Answer:", sum)