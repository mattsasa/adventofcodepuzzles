const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split("\n\n")

const rules = input[0].split("\n").map(x => x.split("|").map(Number))

const updates = input[1].split("\n").map(x => x.split(",").map(Number))

let sum = 0

const invalidUpdates = []

for (let update of updates) {

    let isValid = true

    for (let rule of rules) {
        let first = update.indexOf(rule[0])
        let second = update.indexOf(rule[1])

        if (first != -1 && second != -1 && first > second) {
            isValid = false
        }
    }

    if (!isValid) {
        invalidUpdates.push(update)
    }

}


for (let update of invalidUpdates) {

    let isValid = false

    while (!isValid) {
        isValid = true
        for (let rule of rules) {
            let first = update.indexOf(rule[0])
            let second = update.indexOf(rule[1])
    
            if (first != -1 && second != -1 && first > second) {
                isValid = false;
                [update[first], update[second]] = [update[second], update[first]]
            }
        }
    }

    sum += update[Math.floor(update.length/2)]
    
}

console.log("Part 2 Answer:", sum)