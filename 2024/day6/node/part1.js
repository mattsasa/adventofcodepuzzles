const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

// console.log(input)


let guard = {}

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == '^') {
            guard.y = y
            guard.x = x
            guard.state = 'UP'
            grid[guard.y][guard.x] = 'X'
        }
    }
}

while (true) {

    switch (guard.state) {
        case 'UP':
            if (grid[guard.y - 1]?.[guard.x] == '#') {
                guard.state = 'RIGHT'
            } else {
                guard.y--
            }
            break
        case 'DOWN':
            if (grid[guard.y + 1]?.[guard.x] == '#') {
                guard.state = 'LEFT'
            } else {
                guard.y++
            }
            break
        case 'LEFT':
            if (grid[guard.y][guard.x - 1] == '#') {
                guard.state = 'UP'
            } else {
                guard.x--
            }
            break
        case 'RIGHT':
            if (grid[guard.y][guard.x + 1] == '#') {
                guard.state = 'DOWN'
            } else {
                guard.x++
            }
            break
        default:
            console.log("ERROR")
            return
    }

    if (guard.x < 0 || guard.x >= grid[0].length || guard.y < 0 || guard.y >= grid.length) {
        break
    }

    grid[guard.y][guard.x] = 'X'
}

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

console.log("Guard", guard)

// printGrid(grid)


let countX = 0

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == 'X') {
            countX++
        }
    }
}

console.log("Part 1 Answer:", countX)
