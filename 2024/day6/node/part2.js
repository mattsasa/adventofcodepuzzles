const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))


const guard = {}
const guardPositions = []

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] == '^') {
            guard.y = y
            guard.x = x
            guard.state = 'UP'
            grid[guard.y][guard.x] = 'X'
        }
    }
}

const doesTheGuardLoop = (grid, guard, blockx, blocky) => {

    const prevTurns = {
        'RIGHT': {},
        'LEFT': {},
        'UP': {},
        'DOWN': {}
    }

    grid[blocky][blockx] = '#'

    while (true) {
    
        switch (guard.state) {
            case 'UP':
                if (grid[guard.y - 1]?.[guard.x] == '#') {
                    guard.state = 'RIGHT'
                } else {
                    guard.y--
                }
                break
            case 'DOWN':
                if (grid[guard.y + 1]?.[guard.x] == '#') {
                    guard.state = 'LEFT'
                } else {
                    guard.y++
                }
                break
            case 'LEFT':
                if (grid[guard.y][guard.x - 1] == '#') {
                    guard.state = 'UP'
                } else {
                    guard.x--
                }
                break
            case 'RIGHT':
                if (grid[guard.y][guard.x + 1] == '#') {
                    guard.state = 'DOWN'
                } else {
                    guard.x++
                }
                break
            default:
                console.log("ERROR")
                return
        }
    
        if (prevTurns[guard.state][`${guard.y},${guard.x}`]) {
            return true
        } else {
            prevTurns[guard.state][`${guard.y},${guard.x}`] = 1
        }
    
        if (guard.x < 0 || guard.x >= grid[0].length || guard.y < 0 || guard.y >= grid.length) {

            /// Only ran the first time to populate the guard positions
            if (guardPositions.length == 0) {
                for (let y = 0; y < grid.length; y++) {
                    for (let x = 0; x < grid[0].length; x++) {
                        if (grid[y][x] == 'X') {
                            guardPositions.push({ x, y })
                        }
                    }
                }
            }

            return false
        }
    
        grid[guard.y][guard.x] = 'X'
    }
}

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}

/// Initial Call to populate guardPositions
doesTheGuardLoop(grid.map(row => row.slice()), { ...guard }, guard.x, guard.y)

let sum = 0
for (const position of guardPositions) {
    sum += doesTheGuardLoop(grid.map(row => row.slice()), { ...guard }, position.x, position.y)
}

console.log("Part 2 Answer:", sum)

