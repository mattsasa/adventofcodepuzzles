const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(": ")).map(x => { return { testValue: Number(x[0]), numbers: x[1].split(" ").map(Number) } })

const permuteOperands = (length) => {

    if (length == 1) {
        return [['+'], ['*'], ['||']]
    }
    const finalList = []
    const subset = permuteOperands(length - 1)
    subset.forEach(sub => {
        finalList.push(['+', ...sub])
        finalList.push(['*', ...sub])
        finalList.push(['||', ...sub])
    })

    return finalList
}

const cachePermutations = {}

for (let i = 1; i < 12; i++) {
    cachePermutations[i] = permuteOperands(i)
}

const processEquation = (equation) => {

    let options = cachePermutations[equation.numbers.length - 1]

    for (let operands of options) {
        
        let total = equation.numbers[0]
        for (let i = 1; i < equation.numbers.length; i++) {

            if (operands[i - 1] == '+') {
                total += equation.numbers[i]
            } else if (operands[i - 1] == '*') {
                total *= equation.numbers[i]
            } else if (operands[i - 1] == '||') {
                total = parseInt(total.toString() + equation.numbers[i].toString())
            }

        }

        if (total == equation.testValue) {
            return equation.testValue
        }


    }

    return 0
}

const sum = input.reduce((acc, val) => { return acc + processEquation(val) }, 0)
console.log(sum)