const fs = require('fs')
const grid = fs.readFileSync('../data/input.txt', 'utf8').split("\n").map(x => x.split(""))

const printGrid = (grid) => {
    grid.forEach(line => {
        line.forEach(char => {
            process.stdout.write(char)
        })
        console.log()
    })
    console.log()
}


const allAntennas = {}
const antiNodes = new Set()

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
        if (grid[y][x] != '.') {
            if (!allAntennas[grid[y][x]]) {
                allAntennas[grid[y][x]] = []
            }
            allAntennas[grid[y][x]].push({ x, y })
        }
    }
}


const makeAntiNodes = (antennas) => {   
    for (let i = 0; i < antennas.length; i++) {
        for (let j = i + 1; j < antennas.length; j++) {

            let newX = antennas[i].x
            let newY = antennas[i].y

            while (grid[newY] && grid[newY][newX]) {
                antiNodes.add(`${newX},${newY}`)
                newX -= (antennas[j].x - antennas[i].x)
                newY -= (antennas[j].y - antennas[i].y)
            }


            newX = antennas[j].x
            newY = antennas[j].y

            while (grid[newY] && grid[newY][newX]) {
                antiNodes.add(`${newX},${newY}`)
                newX += (antennas[j].x - antennas[i].x)
                newY += (antennas[j].y - antennas[i].y)
            }

        }
    }
}

for (const frequency in allAntennas) {
    makeAntiNodes(allAntennas[frequency])
}

// for (const node of antiNodes) {
//     const coords = node.split(",").map(Number)
//     grid[coords[1]][coords[0]] = '#'
// }
// printGrid(grid)


console.log("Part 2 Answer:", antiNodes.size)
