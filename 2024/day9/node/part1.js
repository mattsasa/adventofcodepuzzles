const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split("").map(Number)

let freeSpaceBlock = false

const dataFile = []
let currentId = 0

for (let i = 0; i < input.length; i++) {

    if (freeSpaceBlock) {
        for (let n = 0; n < input[i]; n++) {
            dataFile.push('.')
        }
    } else {
        for (let n = 0; n < input[i]; n++) {
            dataFile.push(currentId)
        }
        currentId++   
    }

    freeSpaceBlock = !freeSpaceBlock
}

// console.log(dataFile)


while (dataFile.includes('.')) {

    const lastElement = dataFile.pop()

    if (lastElement == '.') continue

    for (let i = 0; i < dataFile.length; i++) {
        if (dataFile[i] == '.') {
            dataFile[i] = lastElement
            break
        }
    }

}

// console.log(dataFile)


let sum = 0
for (let i = 0; i < dataFile.length; i++) {
    sum += i * dataFile[i]
}

console.log("Part 1 Answer:", sum)
