const fs = require('fs')
const input = fs.readFileSync('../data/input.txt', 'utf8').split("").map(Number)

let freeSpaceBlock = false

const dataDisk = []
let currentId = 0

for (let i = 0; i < input.length; i++) {
    if (freeSpaceBlock) {
        dataDisk[dataDisk.length - 1].followingFreeSpaceLength = input[i]
    } else {
        dataDisk.push({ id: currentId, blockLength: input[i] })
        currentId++   
    }
    freeSpaceBlock = !freeSpaceBlock
}

dataDisk[dataDisk.length - 1].followingFreeSpaceLength = 0

// console.log(dataDisk)

for (let i = dataDisk.length - 1; i >= 0; i--) {

    const targetIndex = dataDisk.findIndex(x => x.id == i)

    for (let n = 0; n < dataDisk.length; n++) {

        if (dataDisk[n].id == i) break

        if (dataDisk[targetIndex].blockLength <= dataDisk[n].followingFreeSpaceLength) {
            const movingBlock = dataDisk.splice(targetIndex, 1)[0]

            const beforeBlock = dataDisk[targetIndex - 1]
            beforeBlock.followingFreeSpaceLength += movingBlock.blockLength
            beforeBlock.followingFreeSpaceLength += movingBlock.followingFreeSpaceLength

            movingBlock.followingFreeSpaceLength = dataDisk[n].followingFreeSpaceLength - movingBlock.blockLength
            dataDisk[n].followingFreeSpaceLength = 0

            dataDisk.splice(n + 1, 0, movingBlock)
            break
        }
    }
    
}

// console.log(dataDisk)

const finalDisk = []
for (const block of dataDisk) {
    for (let i = 0; i < block.blockLength; i++) {
        finalDisk.push(block.id)
    }
    for (let i = 0; i < block.followingFreeSpaceLength; i++) {
        finalDisk.push('.')
    }
}

// console.log(finalDisk)


let sum = 0
for (let i = 0; i < finalDisk.length; i++) {
    if (finalDisk[i] != '.') sum += i * finalDisk[i]
}

console.log("Part 2 Answer:", sum)